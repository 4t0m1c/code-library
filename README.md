# Code Library

This project serves as a library for modular code examples.

+ AI
	 + Detection
	 + [Navmesh, Point'n'Click](#AI---Navmesh,-Point-'n-Click)
+ [Blackjack Framework](#Blackjack-Framework)
+ Coding Concepts
	 + Game Programming Patterns
	 	 + Command Pattern
	 + Generics
	 + Interfaces
	 + Job System
	 + Statics & Extension Methods
+ Collectables
+ [Dialogue System with Scriptable Objects](#Dialogue-System-with-Scriptable-Objects)
+ Editor Scripting
+ [Firebase - Global Leaderboard](#firebase---global-leaderboard)
+ [Fog of War](#fog-of-war)
+ Mesh Manipulation
+ Unity Input System
	 + Mobile
	 + XR
+ Mobile Notifications
+ [Node Network](#Node-Network)
+ Object Pooling
+ Pathfinding (BFS)
+ [Portals](#portals)
+ [Quest System](#quest-system)
+ Rouelette Table
+ [Runtime Terrain Painting](#Runtime-Terrain-Painting)
+ [Saving & Loading](#saving-&-loading-in-unity)
	 + JSON
	 + PlayerPrefs
+ Scriptable Objects
+ Shadergraph - Displacement
+ [Slot Machine Starter](#Slot-Machine-Starter)
+ UI Playground
+ [Vehicle Controller - Ball Physics](#Vehicle-Controller---Ball-Physics)
+ VR Teleport
+ WallWalking
+ Waypoints

## [AI - Navmesh, Point 'n Click](_PACKAGES/AINavmeshAgentsPointNClick_27032020.unitypackage)

Point 'n click mechanics using [Unity Input System](https://github.com/Unity-Technologies/InputSystem). [Navmesh Agents](https://docs.unity3d.com/ScriptReference/AI.NavMeshAgent.html) with waypoints and basic detection using a vision cone and [SphereCast](https://docs.unity3d.com/ScriptReference/Physics.SphereCast.html).

![AI Navmesh Example](Resources/AINavmeshPointNClick_Example.gif)

## [Blackjack Framework](_PACKAGES/BlackjackFramework_27032020.unitypackage)

A modular framework for Blackjack.

![Blackjack Example](Resources/Blackjack_Framework_Example.gif)

## [Dialogue System with Scriptable Objects](_PACKAGES/DialogueSystem_27032020.unitypackage)

Scriptable Object based dialogue system. Easy to use.

![Dialogue Example](Resources/DialogueSystem_Example.gif)

## [Firebase - Global Leaderboard](_PACKAGES/FirebaseLeaderboard_27032020.unitypackage)

Uses the Google Firebase SDK. Follow these videos to replicate:
	
+ [Add Firebase to your Unity project](https://firebase.google.com/docs/unity/setup)
+ [Get Started with Firebase Realtime Database for Unity](https://firebase.google.com/docs/database/unity/start)

WARNING: This example does not use any form of authentication or user validation to prevent spamming. This should be considered in a production environment.

![Firebase Example](Resources/Firebase_Leaderboard_Example.gif)

## [Fog of War](_PACKAGES/FogOfWar_23032020.unitypackage)

This Fog of War system stamps a texture of your choice to a renderTexture which is then projected onto the scene with a modified version of the Lux Essentials Decal shader.

The system is based off of [Unity Fog of War by Tunied](https://github.com/Tunied/Fog-Of-War). Many optimisations have been implemented, the biggest of which is the switch from multiple Graphics.Blit calls to Graphics.DrawTexture, saving tons of resources.

See the demo scene for usage.

![FoW Example](Resources/FoW_Example.gif)

## [Node Network](_PACKAGES/NodeNetwork_20022020.unitypackage)

This system is a network of connected nodes that are aware of the nodes they are connected to as well as the network they exist on. Networks can be merged or split. Nodes can search (BFS) the network for particular nodes; useful for games where nodes can manipulate behaviour on other nodes. 

Placement supports angle snapping and grid snapping.

![Firebase Example](Resources/NodeNetwork_Example.gif)

## [Portals](_PACKAGES/Portals_10062019.unitypackage)

Portal mechanic uses trigger to teleport a tagged object to a target portal after a short delay then adds some force. Also, nice portal effects and model.

![Portals Example](Resources/Portals_Example.gif)

## [Quest System](_PACKAGES/QuestSystem_28072021.unitypackage)

A simple fetch/return style questing system built with Scriptable Objects. Quest SO's are subdivided into non-linear tasks (eg: Collect 10 berries). Once all tasks are complete, the quest may be turned in at the appropriate Quest Giver. A reward may be given using the QuestComplete script and assigning a Quest SO. Unity Events are used throughout to ensure maximum flexibility. See the breakdown below for a better idea of which scripts do what:
+ `QuestObject.cs`
	+ ScriptableObject class, `Quest` class, `Task` class
+ `QuestGiver.cs`
	+ Placed on any object. Can give a quest to any object with `QuestInteract`.
+ `QuestInteract.cs`
	+ Tracks `QuestGiver`s and `QuestTask`s in range and facilitates interaction. This is the core character quest management script.
+ `QuestTask.cs`
	+ Progresses an assigned `QuestObject` and interfaces with `QuestInteract`.
+ `QuestComplete.cs`
	+ Listens to events on the assigned `QuestObject` and invokes a UnityEvent. Used for rewards.
+ `UIQuestStatus.cs`
	+ Displays quest information for key actions such as Quest Added, Task Complete, Quest Complete.

Look through the example scene for a better understanding.

![Quest System Example](Resources/QuestSystem_Example.gif)

## [Runtime Terrain Painting](_PACKAGES/RuntimeTerrainPainting_27032020.unitypackage)

Paint paths onto a terrain at runtime non-destructively. All modifications made at runtime can be reverted.

Two modes:
+ Point-to-point
+ Freehand

![Runtime Terrain Painting Example](Resources/RuntimeTerrainPainting.gif)

## [Saving & Loading in Unity](_PACKAGES/SavingLoadingExamples_29042019.unitypackage)

This example demonstrates the implementation of saving and loading using 2 methods - PlayerPrefs, JSON

### PlayerPrefs

- On Windows, PlayerPrefs are stored in the registry under HKCU\Software\[company name]\[product name] key.
- Data is accessed through a keyword (eg: “health”, “level”)
- Limitations
	 - Single variable of base types only (string, float, int)
		 - Could convert class to a JSON string ([JsonUtility.ToJson](https://docs.unity3d.com/ScriptReference/JsonUtility.ToJson.html)) and save/load that back into a variable.

### JSON

- Data is saved to a single file to a location of your choosing.
	- Should use built in unity paths to prevent errors across operating systems.
- Individual variables are accessed through a keyword (eg: “health”, “level”)
- Read on load and write on exit, or scene change.
- Can save and load an entire class with one call.
- Data can be encrypted to prevent users from modifying the data - cheating.

## [Slot Machine Starter](_PACKAGES/SlotMachine_27032020.unitypackage)

Modular starter for a slot machine.

![Slot Machine Example](Resources/SlotMachine_Example.gif)

## [Vehicle Controller - Ball Physics](_PACKAGES/Vehicle_Ball_23032020.unitypackage)

Utilises the ball physics method for a vehicle controller. Drift has been clamped for this particular example.

![Vehicle Example](Resources/VehicleControllerBallPhysics_Example.gif)
