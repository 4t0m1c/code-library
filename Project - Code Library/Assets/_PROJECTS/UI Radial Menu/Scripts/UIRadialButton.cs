﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIRadialButton : MonoBehaviour {

	public Image _bg;
	public Color defaultColor;
	public Color hoverColor;
	public bool selected = false;

	[Header("Actions")]
	public UnityEvent OnSubmit;

	public void Select(bool state){
		selected = state;
		_bg.color = selected ? hoverColor : defaultColor;
	}

	public void SubmitButton(){
		OnSubmit.Invoke();
	}
}
