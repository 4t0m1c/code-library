﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using UnityEngine.UI;

public class UIRadialLayout : MonoBehaviour {

	[System.Serializable]
	public struct RadialSegment {
		public UIRadialButton segment;
		public Vector2 angleRange;

		public RadialSegment (UIRadialButton _segment, Vector2 _angleRange) {
			segment = _segment;
			angleRange = _angleRange;
		}

	}

	[Header ("Debug")]
	[SerializeField] List<RadialSegment> segments = new List<RadialSegment> ();
	[SerializeField] UIRadialButton selectedSegment;

	[Header ("Layout")]
	[SerializeField] float innerDeselect = 60;
	[SerializeField] float spacing = 5;
	[SerializeField] bool fillCircle = false;
	[SerializeField] bool correctButtonOrientation = false;

	[Header ("Segment")]
	[SerializeField] GameObject segmentPrefab;
	[SerializeField] Transform psuedoMousePos;

	void Start () {
		CalculateRadialLayout ();
	}

	void Update () {
		CalculateActiveSegment ();
	}

	[Button]
	void AddSegment () {
#if UNITY_EDITOR
		if (!Application.isPlaying) {
			UnityEditor.PrefabUtility.InstantiatePrefab (segmentPrefab, transform);
		} else
#endif
		{
			Instantiate (segmentPrefab, transform);
		}

		CalculateRadialLayout ();
	}

	[Button]
	void RemoveSegment () {
#if UNITY_EDITOR
		if (!Application.isPlaying) {
			if (transform.childCount > 0)
				DestroyImmediate (transform.GetChild (transform.childCount - 1).gameObject);
		} else
#endif
		{
			if (transform.childCount > 0) Destroy (transform.GetChild (transform.childCount - 1).gameObject);
		}

		CalculateRadialLayout ();
	}

	[Button]
	void CalculateActiveSegment () {
		//Get angle from center to mousePosition
		//Based on angle, determine active segment

		Vector3 mouseDir = transform.position - (psuedoMousePos == null ? Input.mousePosition : psuedoMousePos.position);
		if (mouseDir.magnitude > innerDeselect) {
			float angle = Vector3.SignedAngle (mouseDir, Vector3.up, transform.forward);
			if (angle < 0) {
				angle = 360 + angle;
			}
			// Debug.Log ("mouseAngle: " + angle);

			bool foundMatch = false;
			foreach (RadialSegment seg in segments) {
				if (angle > seg.angleRange.x && angle < seg.angleRange.y) {
					foundMatch = true;
					selectedSegment?.Select (false);
					selectedSegment = seg.segment;
					selectedSegment.Select (true);
				}
			}
			if (!foundMatch) {
				selectedSegment?.Select (false);
				selectedSegment = null;
			}
		} else {
			selectedSegment?.Select (false);
		}

	}

	[Button]
	void CalculateRadialLayout () {
		float totalUsed = 0;
		float segmentOffset = 0;
		List<Transform> _children = new List<Transform> ();

		for (int i = 0; i < transform.childCount; i++) {
			if (transform.GetChild (i).TryGetComponent<LayoutElement> (out LayoutElement layoutElement)) {
				if (layoutElement.ignoreLayout) {
					continue;
				}
			}
			_children.Add (transform.GetChild (i));
		}

		// Debug.Log ("totalUsed: " + totalUsed);
		float equalSegmentSize = ((360 / _children.Count) - spacing) / 360;
		segments = new List<RadialSegment> ();

		for (int i = 0; i < _children.Count; i++) {
			Transform child = _children[i];
			Transform childBG = child.GetChild (0);
			if (fillCircle) {
				childBG.GetComponent<Image> ().fillAmount = equalSegmentSize;
			}
			float segmentSize = (360 * childBG.GetComponent<Image> ().fillAmount);
			if (i == 0) segmentOffset = segmentSize / 2;

			childBG.transform.localEulerAngles = new Vector3 (childBG.transform.localEulerAngles.x, childBG.transform.localEulerAngles.y, segmentSize / 2);
			totalUsed += segmentSize;
			child.localEulerAngles = new Vector3 (child.localEulerAngles.x, child.localEulerAngles.y, -((segmentSize + (i > 0 ? spacing : 0)) * i));
			segments.Add (new RadialSegment (child.GetComponent<UIRadialButton> (), new Vector2 (totalUsed + (spacing * i) - segmentSize, totalUsed + (spacing * (i + 1)))));

			// Debug.Log ("totalUsed: " + totalUsed);
		}
		// Debug.Log ("segmentOffset: " + segmentOffset);
		float rotationalCorrection = (totalUsed / 2) - segmentOffset + ((_children.Count - 1) * (spacing / 2));
		transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, rotationalCorrection);

		if (correctButtonOrientation) {
			for (int i = 0; i < transform.childCount; i++) {
				if (transform.GetChild (i).TryGetComponent<LayoutElement> (out LayoutElement layoutElement)) {
					if (layoutElement.ignoreLayout) {
						transform.GetChild (i).eulerAngles = Vector3.zero;
						continue;
					}
				}
				transform.GetChild (i).GetChild (1).eulerAngles = Vector3.zero;
			}
		}
	}

}