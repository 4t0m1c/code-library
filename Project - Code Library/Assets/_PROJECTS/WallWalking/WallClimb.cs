﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WallClimber {

    public class WallClimb : MonoBehaviour {

        Rigidbody rBody;
        [SerializeField] float moveSpeed = 5;
        bool climbingWall = false;
        [SerializeField] LayerMask wallMask;

        void Awake () {
            rBody = GetComponent<Rigidbody> ();
        }

        void Update () {
            float _h = Input.GetAxis ("Horizontal");
            float _v = Input.GetAxis ("Vertical");

            if (_h != 0 || _v != 0) {
                rBody.velocity = transform.TransformDirection (new Vector3 (_h * Time.deltaTime * moveSpeed, transform.InverseTransformDirection (rBody.velocity).y, _v * Time.deltaTime * moveSpeed)); //Move local space
            }

            if (Input.GetKeyDown (KeyCode.Space)) {
                ClimbWall ();
            }

            if (climbingWall) {
                rBody.AddRelativeForce (Vector3.down * 50); //Fake gravity
            }
        }

        void ClimbWall () {
            rBody.useGravity = false;
            climbingWall = true;

            //Cast a ray forward
            if (Physics.Raycast (transform.position + transform.up, transform.forward, out RaycastHit hit, 10, wallMask)) {
                //Get normal vector
                Vector3 normalVector = hit.normal;
                Debug.DrawRay (hit.point, normalVector, Color.red, 5);

                //Use normal vector as up
                transform.rotation = Quaternion.LookRotation (normalVector + new Vector3 (0, 90, 0), -transform.up);
            } else {
                climbingWall = false;
                // Vector3 newDir = Vector3.RotateTowards (transform.up, Vector3.up, 1000, 1);
                transform.rotation = Quaternion.LookRotation (-transform.up, Vector3.up);
                rBody.useGravity = true;
            }

        }

    }
}