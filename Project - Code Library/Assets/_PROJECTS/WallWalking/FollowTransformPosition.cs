﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WallClimber {
    public class FollowTransformPosition : MonoBehaviour {

        [SerializeField] Transform playerTransform;
        void Update () {
            if (playerTransform != null) {
                transform.position = playerTransform.position;
            }
        }
    }
}