﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace At0m1c.SlotMachine {

    public class UIDisplaySlots : MonoBehaviour {
        [SerializeField] List<Image> Reel1 = new List<Image> ();
        [SerializeField] List<Image> Reel2 = new List<Image> ();
        [SerializeField] List<Image> Reel3 = new List<Image> ();

        void OnEnable () {
            SlotMachine.OnSpin += SpinSlots;
        }

        void OnDisable () {
            SlotMachine.OnSpin -= SpinSlots;
        }

        void SpinSlots (List<Reel> spinResult) {
            //Reel 1
            Reel1[0].sprite = spinResult[0].symbols[0].symbolSprite;
            Reel1[0].color = spinResult[0].symbols[0].symbolColor;

            Reel1[1].sprite = spinResult[0].symbols[1].symbolSprite;
            Reel1[1].color = spinResult[0].symbols[1].symbolColor;

            Reel1[2].sprite = spinResult[0].symbols[2].symbolSprite;
            Reel1[2].color = spinResult[0].symbols[2].symbolColor;

            //Reel 2
            Reel2[0].sprite = spinResult[1].symbols[0].symbolSprite;
            Reel2[0].color = spinResult[1].symbols[0].symbolColor;

            Reel2[1].sprite = spinResult[1].symbols[1].symbolSprite;
            Reel2[1].color = spinResult[1].symbols[1].symbolColor;

            Reel2[2].sprite = spinResult[1].symbols[2].symbolSprite;
            Reel2[2].color = spinResult[1].symbols[2].symbolColor;

            //Reel 3
            Reel3[0].sprite = spinResult[2].symbols[0].symbolSprite;
            Reel3[0].color = spinResult[2].symbols[0].symbolColor;

            Reel3[1].sprite = spinResult[2].symbols[1].symbolSprite;
            Reel3[1].color = spinResult[2].symbols[1].symbolColor;
            
            Reel3[2].sprite = spinResult[2].symbols[2].symbolSprite;
            Reel3[2].color = spinResult[2].symbols[2].symbolColor;
        }
    }
}