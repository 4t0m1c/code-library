using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.SlotMachine {

    // BEGIN DATA TYPES

    [System.Serializable]
    public enum SymbolType {
        Cherry,
        Rocket,
        Bomb
    }

    [System.Serializable]
    public struct Symbol {
        public SymbolType symbolType;
        public int value;
        public Sprite symbolSprite;
        public Color symbolColor;
        [Range (0, 100)]
        public int rarity;
    }

    [System.Serializable]
    public struct Reel {
        public List<Symbol> symbols;
    }

    // END DATA TYPES

    public class SlotMachine : MonoBehaviour {

        public delegate void SlotEvents (List<Reel> spinResult);
        public static event SlotEvents OnSpin;

        [SerializeField] List<Symbol> possibleSymbols = new List<Symbol> ();
        [SerializeField] List<Reel> reels = new List<Reel> ();

        /* 
            Reel
            x       x       x       ==      false
            o       o       o
            x       x       x
        */

        public void Spin () {
            RandomiseReels ();
            CheckReels ();
        }

        void RandomiseReels () {
            for (int i = 0; i < reels.Count; i++) {
                for (int j = 0; j < reels[i].symbols.Count; j++) {
                    reels[i].symbols[j] = GetRandomSymbol ();
                }
            }

            if (OnSpin != null) OnSpin (reels);
        }

        /* 
            VALIDATE SPIN
        */

        [ContextMenu ("CheckReels")]
        public void CheckReels () {
            for (int i = 0; i < 3; i++) {
                if (CheckLine_Straight (i)) {
                    Debug.Log ($"Win Line {i}");
                }
            }
            if (CheckLine_Diagonal (2, 1, 0)) {
                Debug.Log ($"Win Diagonal 2,1,0");
            }
            if (CheckLine_Diagonal (0, 1, 2)) {
                Debug.Log ($"Win Diagonal 0,1,2");
            }
        }

        bool CheckLine_Straight (int row) {
            Symbol checkingSymbol = reels[0].symbols[row];
            bool allValid = true;
            foreach (var reel in reels) {
                if (reel.symbols[row].symbolType != checkingSymbol.symbolType) {
                    allValid = false;
                }
            }
            return allValid;
        }

        bool CheckLine_Diagonal (int reel1Index, int reel2Index, int reel3Index) {
            Symbol checkingSymbol = reels[0].symbols[reel1Index];
            bool allValid = true;

            if (reels[1].symbols[reel2Index].symbolType != checkingSymbol.symbolType) {
                allValid = false;
            }
            if (reels[2].symbols[reel3Index].symbolType != checkingSymbol.symbolType) {
                allValid = false;
            }

            return allValid;
        }

        Symbol GetRandomSymbol () {
            int randomNumber = Random.Range (0, 101);
            List<Symbol> inRarityRange = new List<Symbol> ();

            for (int i = 0; i < possibleSymbols.Count; i++) {
                if (randomNumber <= possibleSymbols[i].rarity) {
                    inRarityRange.Add (possibleSymbols[i]);
                }
            }

            return inRarityRange[Random.Range (0, inRarityRange.Count)];
        }

    }
}