﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Roulette {
    public class QuickSetRotate : MonoBehaviour {
        [SerializeField] float numberOfItems = 37;
        [SerializeField] GameObject objectToDuplicate;

        [ContextMenu ("SetItems")]
        void SetItems () {
            for (int i = 0; i < numberOfItems; i++) {
                GameObject newBlock = Instantiate (objectToDuplicate, objectToDuplicate.transform.parent);
                newBlock.transform.localEulerAngles = new Vector3 (0, 360f / numberOfItems * i, 0);
            }
        }

    }
}