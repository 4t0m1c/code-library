﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.Roulette {
    public class RotateTable : MonoBehaviour {
        [SerializeField] float rotateSpeed = 10;
        [SerializeField] AnimationCurve wheelRotate;

        UnityAction callbackAction;

        void OnEnable () {
            RouletteManager.OnRoundBegin += x => {
                callbackAction = x;
                BeginRound ();
            };
        }

        void BeginRound () {
            StartCoroutine (UpdateRotation ());
        }

        IEnumerator UpdateRotation () {
            float timeStarted = Time.timeSinceLevelLoad;
            float timeSinceStart = Time.timeSinceLevelLoad - timeStarted;
            while (timeSinceStart < wheelRotate[wheelRotate.length - 1].time) {
                timeSinceStart = Time.timeSinceLevelLoad - timeStarted;
                transform.Rotate (Vector3.up * rotateSpeed * Time.deltaTime * wheelRotate.Evaluate (timeSinceStart), Space.Self);
                yield return new WaitForFixedUpdate ();
            }

            callbackAction.Invoke ();
        }
    }
}