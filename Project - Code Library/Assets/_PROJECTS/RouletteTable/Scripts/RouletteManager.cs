﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace At0m1c.Roulette {

    public enum RouletteColors {
        Red = 0,
        Black = 1,
        Green = 2,
        None = -1
    }

    public class RouletteManager : MonoBehaviour {
        public delegate void GameEvents (UnityAction roundComplete);
        public static GameEvents OnRoundBegin;
        [SerializeField] Button spinButton;
        [SerializeField] Button spinRedButton;
        [SerializeField] Button spinBlackButton;
        [SerializeField] Button spinGreenButton;
        [SerializeField] RectTransform selectedBG;
        [SerializeField] Text scoreStreakDisplay;

        int callbackCount = 0;
        RouletteColors bet;
        RouletteColors colorLanded;
        int streak = 0;

        void Awake () {
            Application.targetFrameRate = 60;
            spinButton.onClick.AddListener (BeginRound);
            spinRedButton.onClick.AddListener (BeginRoundRed);
            spinBlackButton.onClick.AddListener (BeginRoundBlack);
            spinGreenButton.onClick.AddListener (BeginRoundGreen);
        }

        void OnEnable () {
            ColorEvent.OnColorTriggered += SetLastColorTriggered;
        }

        void OnDisable () {
            ColorEvent.OnColorTriggered -= SetLastColorTriggered;
        }

        void SetLastColorTriggered (RouletteColors col) {
            colorLanded = col;
        }

        void BeginRound () {
            spinButton.gameObject.SetActive (false);
            callbackCount = 0;
            if (OnRoundBegin != null) OnRoundBegin (RoundComplete);
        }

        void BeginRoundRed () {
            bet = RouletteColors.Red;
            selectedBG.gameObject.SetActive (true);
            selectedBG.SetParent (spinRedButton.transform, false);
        }

        void BeginRoundBlack () {
            bet = RouletteColors.Black;
            selectedBG.gameObject.SetActive (true);
            selectedBG.SetParent (spinBlackButton.transform, false);
        }

        void BeginRoundGreen () {
            bet = RouletteColors.Green;
            selectedBG.gameObject.SetActive (true);
            selectedBG.SetParent (spinGreenButton.transform, false);
        }

        public void RoundComplete () {
            callbackCount++;
            if (callbackCount == 2) {
                spinButton.gameObject.SetActive (true);
                CheckBet ();
                bet = RouletteColors.None;
                selectedBG.gameObject.SetActive (false);
            }
        }

        void CheckBet () {
            if (colorLanded == bet) {
                streak++;
                scoreStreakDisplay.text = streak.ToString ();
            } else {
                streak = 0;
                scoreStreakDisplay.text = streak.ToString ();
            }
        }

    }
}