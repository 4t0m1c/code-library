﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Roulette {
    public class ColorEvent : MonoBehaviour {
        public delegate void ColorEvents (RouletteColors col);
        public static event ColorEvents OnColorTriggered;
        [SerializeField] RouletteColors color;

        void OnTriggerEnter (Collider other) {
            if (other.CompareTag ("Ball")) {
                OnColorTriggered?.Invoke(color);
            }
        }

    }
}