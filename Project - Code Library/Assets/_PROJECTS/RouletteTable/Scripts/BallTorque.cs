﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.Roulette {
    public class BallTorque : MonoBehaviour {
        [SerializeField] Vector3 ballForce = new Vector3 (0, 0, 30);
        Rigidbody rb;
        MeshRenderer mr;
        Vector3 startPos;
        Quaternion startRot;

        UnityAction callbackAction;

        void OnEnable () {
            RouletteManager.OnRoundBegin += x => {
                callbackAction = x;
                AddTheForce ();
            };
        }

        void Awake () {
            rb = GetComponent<Rigidbody> ();
            mr = transform.GetChild (0).GetComponent<MeshRenderer> ();
            startPos = transform.position;
            startRot = transform.rotation;
        }

        void AddTheForce () {
            StartCoroutine (WaitForStandStill ());
        }

        IEnumerator WaitForStandStill () {
            rb.isKinematic = true;
            mr.enabled = false;
            rb.position = startPos;
            rb.rotation = startRot;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            yield return new WaitForSeconds (1f);

            rb.isKinematic = false;
            rb.AddRelativeForce (ballForce, ForceMode.Impulse);

            yield return new WaitForSeconds (0.1f);
            mr.enabled = true;

            do { yield return null; } while (rb.velocity.sqrMagnitude > 0.001f);

            callbackAction.Invoke ();
        }
    }
}