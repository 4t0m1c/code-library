﻿using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {
	[CustomPropertyDrawer (typeof (InventoryItemObject))]
	public class InventoryItemObjectDrawer : PropertyDrawer {
		SerializedObject itemObject = null;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.BeginProperty (position, label, property);

			if (property.objectReferenceValue == null) {
				property.objectReferenceValue = EditorGUILayout.ObjectField (property.objectReferenceValue, typeof (InventoryItemObject), false);
				return;
			} else {
				itemObject = new SerializedObject (property.objectReferenceValue);
				itemObject.Update ();

				EditorGUILayout.BeginHorizontal (EditorStyles.helpBox);
				EditorGUILayout.LabelField (itemObject.targetObject.name);
				if (GUILayout.Button ("Clear", EditorStyles.miniButton)) {
					property.objectReferenceValue = null;
					return;
				}
				EditorGUILayout.EndHorizontal ();

				property = itemObject.FindProperty ("inventoryItem");
				EditorGUILayout.PropertyField (property, GUIContent.none, GUILayout.Height (0));

				itemObject.ApplyModifiedProperties ();
			}

		}
	}
}