﻿
using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {
    [CustomPropertyDrawer (typeof (InventoryItem))]
    public class InventoryItemDrawer : PropertyDrawer {

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
            EditorGUI.BeginProperty (position, label, property);

            EditorGUILayout.BeginHorizontal (EditorStyles.helpBox);
            {
                SerializedProperty _itemSprite = property.FindPropertyRelative ("itemSprite");
                _itemSprite.objectReferenceValue = EditorGUILayout.ObjectField (_itemSprite.objectReferenceValue, typeof (Sprite), false, new GUILayoutOption[] {
                    GUILayout.Width (60), GUILayout.Height (60)
                });

                EditorGUILayout.BeginVertical ();
                {
                    SerializedProperty _name = property.FindPropertyRelative ("itemName");
                    EditorGUILayout.PropertyField (_name);

                    EditorGUILayout.BeginHorizontal ();

                    EditorGUILayout.BeginVertical ();
                    {
                        SerializedProperty _itemCost = property.FindPropertyRelative ("itemCost");
                        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
                        GUILayout.Label ("Value", EditorStyles.miniLabel);
                        EditorGUILayout.PropertyField (_itemCost, GUIContent.none);
                    }
                    EditorGUILayout.EndVertical ();

                    EditorGUILayout.BeginVertical ();
                    {
                        SerializedProperty _itemWeight = property.FindPropertyRelative ("itemWeight");
                        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
                        GUILayout.Label ("Weight", EditorStyles.miniLabel);
                        EditorGUILayout.PropertyField (_itemWeight, GUIContent.none);
                    }
                    EditorGUILayout.EndVertical ();

                    EditorGUILayout.BeginVertical ();
                    {
                        SerializedProperty _itemHeal = property.FindPropertyRelative ("itemHeal");
                        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
                        GUILayout.Label ("Heal", EditorStyles.miniLabel);
                        EditorGUILayout.PropertyField (_itemHeal, GUIContent.none);
                    }
                    EditorGUILayout.EndVertical ();

                    EditorGUILayout.BeginVertical ();
                    {
                        SerializedProperty _itemMana = property.FindPropertyRelative ("itemMana");
                        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
                        GUILayout.Label ("Mana", EditorStyles.miniLabel);
                        EditorGUILayout.PropertyField (_itemMana, GUIContent.none);
                    }
                    EditorGUILayout.EndVertical ();

                    EditorGUILayout.BeginVertical ();
                    {
                        SerializedProperty _itemManaTime = property.FindPropertyRelative ("itemManaTime");
                        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
                        GUILayout.Label ("Mana Time", EditorStyles.miniLabel);
                        EditorGUILayout.PropertyField (_itemManaTime, GUIContent.none);
                    }
                    EditorGUILayout.EndVertical ();

                    EditorGUILayout.BeginVertical ();
                    {
                        SerializedProperty _itemRarity = property.FindPropertyRelative ("itemRarity");
                        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
                        GUILayout.Label ("Rarity", EditorStyles.miniLabel);
                        EditorGUILayout.PropertyField (_itemRarity, GUIContent.none);
                    }
                    EditorGUILayout.EndVertical ();

                    EditorGUILayout.EndHorizontal ();
                }
                EditorGUILayout.EndVertical ();
            }
            EditorGUILayout.EndHorizontal ();

            EditorGUI.EndProperty ();
        }
    }
}