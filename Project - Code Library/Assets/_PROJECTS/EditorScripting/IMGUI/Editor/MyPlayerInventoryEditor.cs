﻿// This example shows a custom inspector for an
// object "MyPlayer", which has a variable speed.
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {

	[CustomEditor (typeof (MyPlayerInventory))]
	public class MyPlayerInventoryEditor : Editor {
		public override void OnInspectorGUI () {
			serializedObject.Update ();

			MyPlayerInventory targetPlayerInventory = (MyPlayerInventory) target;

			GUIStyle myLabelStyle = new GUIStyle (GUI.skin.label);
			myLabelStyle = EditorStyles.helpBox;
			myLabelStyle.wordWrap = true;
			myLabelStyle.richText = true;
			myLabelStyle.fontSize = 12;

			EditorGUILayout.Space ();

			EditorGUILayout.LabelField ("This is the player inventory. <b>Pretty cool huh?</b> \n\nYou could add some <color=yellow>nice descriptions</color> to your scripts this way, especially if they're assets that other developers need to use.", myLabelStyle);
			EditorGUILayout.Space ();

			EditorGUILayout.PropertyField (serializedObject.FindProperty ("inventory"), GUIContent.none, GUILayout.Height(0));

			serializedObject.ApplyModifiedProperties ();

		}
	}
}