﻿using System.Collections;
using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {
    // Custom Editor using SerializedProperties.
    // Automatic handling of multi-object editing, undo, and Prefab overrides.
    [CustomEditor (typeof (MyPlayer))]
    [CanEditMultipleObjects]
    public class MyPlayerEditor : Editor {
        SerializedProperty damageProp;
        SerializedProperty armorProp;
        SerializedProperty itemProp;

        void OnEnable () {
            // Setup the SerializedProperties.
            damageProp = serializedObject.FindProperty ("damage");
            armorProp = serializedObject.FindProperty ("armor");
            itemProp = serializedObject.FindProperty ("equippedItem");
        }

        public override void OnInspectorGUI () {
            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
            serializedObject.Update ();

            // Show the custom GUI controls.
            EditorGUILayout.IntSlider (damageProp, 0, 100, new GUIContent ("Damage"));

            // Only show the damage progress bar if all the objects have the same damage value:
            if (!damageProp.hasMultipleDifferentValues)
                ProgressBar (damageProp.intValue / 100.0f, "Damage");

            EditorGUILayout.IntSlider (armorProp, 0, 100, new GUIContent ("Armor"));

            // Only show the armor progress bar if all the objects have the same armor value:
            if (!armorProp.hasMultipleDifferentValues)
                ProgressBar (armorProp.intValue / 100.0f, "Armor");

            EditorGUILayout.LabelField("Equipped Item");
            EditorGUILayout.PropertyField (itemProp, GUIContent.none, GUILayout.Height (0));

            // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
            serializedObject.ApplyModifiedProperties ();
        }

        // Custom GUILayout progress bar.
        void ProgressBar (float value, string label) {
            // Get a rect for the progress bar using the same margins as a textfield:
            Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
            EditorGUI.ProgressBar (rect, value, label);
            EditorGUILayout.Space ();
        }
    }
}