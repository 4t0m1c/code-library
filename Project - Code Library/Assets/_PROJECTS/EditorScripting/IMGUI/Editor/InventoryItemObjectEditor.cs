﻿using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {
    [CustomEditor (typeof (InventoryItemObject))]
    public class InventoryItemObjectEditor : Editor {
        public override void OnInspectorGUI () {
            base.OnInspectorGUI ();
        }
    }
}