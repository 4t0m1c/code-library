﻿using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {
    [CustomEditor (typeof (InventoryObject))]
    public class InventoryObjectEditor : Editor {
        public override void OnInspectorGUI () {
            base.OnInspectorGUI ();
        }
    }
}