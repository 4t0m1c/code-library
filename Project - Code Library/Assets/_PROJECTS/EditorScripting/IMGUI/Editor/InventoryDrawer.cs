﻿using UnityEditor;
using UnityEngine;

namespace At0m1c.EditorScripting {
	[CustomPropertyDrawer (typeof (Inventory))]
	public class InventoryDrawer : PropertyDrawer {
		/* 
			List InventoryItemObjects
			Add/Remove InventoryItemObjects
		*/

		SerializedObject itemObject = null;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {

			SerializedProperty inventoryItems = property.FindPropertyRelative ("inventoryItems");

			EditorGUILayout.BeginHorizontal ();
			if (inventoryItems.arraySize > 0) {
				if (GUILayout.Button ("-", EditorStyles.miniButton)) {
					inventoryItems.DeleteArrayElementAtIndex (inventoryItems.arraySize - 1);
				}
			}

			if (GUILayout.Button ("+", EditorStyles.miniButton)) {
				inventoryItems.InsertArrayElementAtIndex (inventoryItems.arraySize);
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.Space ();

			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			for (int i = 0; i < inventoryItems.arraySize; i++) {

				SerializedProperty item = inventoryItems.GetArrayElementAtIndex (i);

				EditorGUILayout.PropertyField (item, GUIContent.none, GUILayout.Height (0));

				if (GUILayout.Button ("Remove", EditorStyles.miniButton)) {
					inventoryItems.DeleteArrayElementAtIndex (i);
					i--;
				}

				EditorGUILayout.Space ();
			}
		}
	}
}