using UnityEngine;

namespace At0m1c.EditorScripting {
	[System.Serializable, SerializeField]
	public struct InventoryItem {
		public string itemName;
		public Sprite itemSprite;
		public int itemCost;
		public float itemWeight;
		public float itemHeal;
		public float itemMana;
		public float itemManaTime;
		public ItemRarity itemRarity;
	}

	public enum ItemRarity {
		Common,
		Uncommon,
		Rare,
		Mythical,
		Legendary
	}
}