﻿using System.Collections;
using UnityEngine;

// This is not an editor script.
namespace At0m1c.EditorScripting {
    public class MyPlayer : MonoBehaviour {
        public int armor = 75;
        public int damage = 25;
        public InventoryItemObject equippedItem;

        void Update () {
            // Update logic here...
        }
    }
}