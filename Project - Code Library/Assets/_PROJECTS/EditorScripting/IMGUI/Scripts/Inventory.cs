using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.EditorScripting {
	[System.Serializable, SerializeField]
	public struct Inventory {
		public List<InventoryItemObject> inventoryItems;
	}
}