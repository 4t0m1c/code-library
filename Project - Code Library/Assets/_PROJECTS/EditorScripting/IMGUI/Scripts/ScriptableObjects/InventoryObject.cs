﻿using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.EditorScripting {
    [CreateAssetMenu (fileName = "Inventory", menuName = "Inventory/InventoryObject", order = 0)]
    public class InventoryObject : ScriptableObject {
        [SerializeField] public Inventory inventory;
    }
}