﻿using UnityEngine;

namespace At0m1c.EditorScripting {
    [System.Serializable, SerializeField, CreateAssetMenu (fileName = "InventoryItem", menuName = "Inventory/InventoryItemObject", order = 0)]
    public class InventoryItemObject : ScriptableObject {
        [SerializeField] public InventoryItem inventoryItem;
    }
}