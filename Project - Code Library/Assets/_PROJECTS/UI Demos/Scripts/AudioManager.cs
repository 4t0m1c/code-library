﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.UIPlayground {
    public class AudioManager : MonoBehaviour {

        [Header ("Audio Sources")]
        [SerializeField] AudioSource sourceAmbience;
        [SerializeField] AudioSource sourceMusic;
        [SerializeField] AudioSource sourceDialogue;
        [SerializeField] AudioSource sourceGameplay;

        public void PlayAudio_Ambience (AudioClip clip) {
            sourceAmbience.clip = clip;
            sourceAmbience.loop = true;
            sourceAmbience.Play ();
        }

        public void PlayAudio_Music (AudioClip clip) {
            sourceMusic.clip = clip;
            sourceMusic.loop = true;
            sourceMusic.Play ();
        }

        public void PlayAudio_Dialogue (AudioClip clip) {
            sourceDialogue.PlayOneShot (clip);
        }

        public void PlayAudio_Gameplay (AudioClip clip) {
            sourceGameplay.PlayOneShot (clip);
        }

    }

}