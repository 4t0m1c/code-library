﻿using System.Collections;
using System.Collections.Generic;
using At0m1c.UIPlayground;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour {
    Enemy enemy;

    [SerializeField] Image image;

    public void SetEnemy (Enemy _enemy) {
        enemy = _enemy;
        enemy.OnHealthUpdated += HealthUpdated;
        enemy.OnHealthDepleted += HealthDepleted;
    }

    void OnDisable() {
        enemy.OnHealthUpdated -= HealthUpdated;
        enemy.OnHealthDepleted -= HealthDepleted;
    }

    void Update() {
        ((RectTransform)transform).anchoredPosition = Camera.main.WorldToScreenPoint(enemy.transform.position);
    }

    void HealthUpdated (float health) {
        image.fillAmount = health/100;
    }

    void HealthDepleted (float health) {
        Destroy(gameObject);
    }

}