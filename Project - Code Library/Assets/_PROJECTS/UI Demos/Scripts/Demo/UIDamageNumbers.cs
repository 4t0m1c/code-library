﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.UIPlayground {

    public class UIDamageNumbers : MonoBehaviour {

        [SerializeField] Enemy enemy;
        [SerializeField] GameObject healthTextPrefab;
        [SerializeField] float healthValueThreshsold = 5;
        [SerializeField] float moveUpSpeed = 10;
        float lastHealth;

        List<GameObject> numbers = new List<GameObject> ();

        public void SetEnemy (Enemy _enemy) {
            enemy = _enemy;
            enemy.OnHealthUpdated += HealthUpdated;
            enemy.OnHealthDepleted += HealthDepleted;
            lastHealth = enemy.health;
        }

        void OnDisable () {
            enemy.OnHealthUpdated -= HealthUpdated;
            enemy.OnHealthDepleted -= HealthDepleted;
        }

        void HealthUpdated (float health) {
            float healthDiff = lastHealth - health;
            if (healthDiff >= healthValueThreshsold) {
                lastHealth = health;
                StartCoroutine (DisplayDamage (healthDiff));
            }
        }

        void HealthDepleted (float value) {
            StartCoroutine (WaitThenDestroy ());
        }

        IEnumerator WaitThenDestroy () {
            while (numbers.Count > 0) {
                yield return null;
            }
            Destroy (gameObject);
        }

        IEnumerator DisplayDamage (float value) {
            GameObject newHealthText = Instantiate (healthTextPrefab, transform.parent);
            numbers.Add (newHealthText);
            ((RectTransform) newHealthText.transform).anchoredPosition = Camera.main.WorldToScreenPoint (enemy.transform.position);
            newHealthText.GetComponent<Text> ().text = "-" + value.ToString ("n0");
            CanvasGroup cg = newHealthText.GetComponent<CanvasGroup> ();

            float displayTime = 1f;
            float currentDisplayTime = 1f;
            while (currentDisplayTime > 0) {
                currentDisplayTime -= Time.deltaTime;

                ((RectTransform) newHealthText.transform).anchoredPosition += Vector2.up * moveUpSpeed * Time.deltaTime;
                cg.alpha = currentDisplayTime / displayTime;

                yield return null;
            }

            numbers.Remove (newHealthText);
            Destroy (newHealthText);
        }

    }
}