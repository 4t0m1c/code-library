﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.UIPlayground {

    public class MoveEnemy : MonoBehaviour {

        [SerializeField] Vector3 startPosition;
        [SerializeField] Vector3 endPosition;
        [SerializeField] float moveSpeed = 5;

        Enemy enemy;

        void Awake () {
            enemy = GetComponent<Enemy> ();
            transform.position = startPosition;
        }

        void Update () {
            if (!enemy.dead) {
                transform.position = Vector3.MoveTowards (transform.position, endPosition, Time.deltaTime * moveSpeed);
            }
        }

    }

}