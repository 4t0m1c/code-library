﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.UIPlayground {
    public class SawBlade : MonoBehaviour {

        [SerializeField] float sawSpeed = 5;
        [SerializeField] float damage = 5;
        List<Enemy> enemiesInRange = new List<Enemy> ();

        void Update () {
            transform.Rotate (Vector3.forward * Time.deltaTime * sawSpeed);

            foreach (Enemy enemy in enemiesInRange) {
                if (enemy.isActiveAndEnabled)
                    enemy.DoDamage (damage * Time.deltaTime);
            }

        }

        void OnTriggerEnter (Collider other) {
            if (other.attachedRigidbody != null) {
                Enemy enemy = other.attachedRigidbody.GetComponent<Enemy> ();
                if (enemy != null) enemiesInRange.Add (enemy);
            }
        }

        void OnTriggerExit (Collider other) {
            if (other.attachedRigidbody != null) {
                Enemy enemy = other.attachedRigidbody.GetComponent<Enemy> ();
                if (enemy != null) enemiesInRange.Remove (enemy);
            }
        }
    }
}