﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.UIPlayground {
    public class SpawnEnemy : MonoBehaviour {

        [SerializeField] GameObject enemyPrefab;
        [SerializeField] float spawnInterval;

        float currentInterval;

        void Update () {
            if (currentInterval <= 0) {
                GameObject enemy = Instantiate (enemyPrefab, transform);
                currentInterval = spawnInterval;
            } else {
                currentInterval -= Time.deltaTime;
            }
        }

    }
}