﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.UIPlayground {
    public class Enemy : MonoBehaviour {

        // Subscribe to these events to update UI

        public delegate void EnemyHealthEvents (float health);
        public event EnemyHealthEvents OnHealthUpdated;
        public event EnemyHealthEvents OnHealthDepleted;

        public float health = 100;
        public bool dead = false;

        [SerializeField] GameObject enemyUIDamageNumPrefab;
        [SerializeField] GameObject enemyHealthPrefab;
        [SerializeField] Transform[] bits;

        void Start () {
            GameObject ui = Instantiate (enemyUIDamageNumPrefab, UIManager.instance.transform);
            ui.GetComponent<UIDamageNumbers> ().SetEnemy (this);

            GameObject uiHealth = Instantiate (enemyHealthPrefab, UIManager.instance.transform);
            uiHealth.GetComponent<HealthScript> ().SetEnemy (this);
        }

        public void DoDamage (float damageValue) {
            if (dead) return;

            health -= damageValue;
            if (OnHealthUpdated != null) OnHealthUpdated (health);

            if (health <= 0) {
                if (OnHealthDepleted != null) OnHealthDepleted (health);
                dead = true;
                Die ();
            }
        }

        void Die () {
            foreach (var bit in bits) {
                bit.SetParent (null);
                bit.gameObject.SetActive (true);
                bit.GetComponent<Rigidbody> ().AddRelativeTorque (Vector3.right * 5, ForceMode.Impulse);
            }
            gameObject.SetActive (false);
            // Destroy(gameObject);
        }

    }
}