﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISetResolution : MonoBehaviour {

    [SerializeField] Dropdown resolutionDropDown;
    [SerializeField] Dropdown fullscreenModeDropDown;

    [SerializeField] List<string> resolutions = new List<string> {
        "1024x768",
        "1280x800",
        "1280x1024",
        "1280x720",
        "1360x768",
        "1366x768",
        "1440x900",
        "1600x900",
        "1680x1050",
        "1920x1080",
        "1920x1200",
        "2560x1440",
        "2560x1080",
        "3440x1440",
        "3840x2160"
    };

    [System.Serializable]
    class ResolutionSettings {
        public string currentResolution;
        public FullScreenMode fullScreenMode = FullScreenMode.FullScreenWindow;

        public ResolutionSettings () { }
    }

    [SerializeField] ResolutionSettings resolutionSettings;

    void Awake () {
        LoadSettings ();
        LoadDropDowns ();
    }

    void LoadSettings () {
        resolutionSettings = JsonUtility.FromJson<ResolutionSettings> (PlayerPrefs.GetString ("settings_resolution"));

        if (resolutionSettings == null || resolutionSettings.currentResolution.Length == 0) {
            Debug.Log ($"No resolution settings found. Creating new settings.");
            resolutionSettings = new ResolutionSettings ();
            SetResolution (Screen.width + "x" + Screen.height);

            SaveSettings ();
        }
    }

    void LoadDropDowns () {
        resolutionDropDown.AddOptions (resolutions);
        resolutionDropDown.AddOptions (new List<string> () { "Custom (Native)" });

        bool found = false;
        for (var i = 0; i < resolutions.Count; i++) {
            if (resolutionSettings.currentResolution == resolutions[i]) {
                found = true;
                resolutionDropDown.SetValueWithoutNotify (i);
                break;
            }
        }
        if (!found) {
            resolutionDropDown.SetValueWithoutNotify (resolutionDropDown.options.Count - 1);
        }

        fullscreenModeDropDown.AddOptions (new List<string> () {
            "Exclusive Fullscreen",
            "Fullscreen Window",
            "Maximized Window",
            "Windowed"
        });

        if (resolutionSettings.fullScreenMode == FullScreenMode.ExclusiveFullScreen) {
            fullscreenModeDropDown.SetValueWithoutNotify (0);
        } else if (resolutionSettings.fullScreenMode == FullScreenMode.FullScreenWindow) {
            fullscreenModeDropDown.SetValueWithoutNotify (1);
        } else if (resolutionSettings.fullScreenMode == FullScreenMode.MaximizedWindow) {
            fullscreenModeDropDown.SetValueWithoutNotify (2);
        } else if (resolutionSettings.fullScreenMode == FullScreenMode.Windowed) {
            fullscreenModeDropDown.SetValueWithoutNotify (3);
        }
    }

    void SaveSettings () {
        PlayerPrefs.SetString ("settings_resolution", JsonUtility.ToJson (resolutionSettings));
        PlayerPrefs.Save ();
    }

    void SetResolution (string resolution) {
        string[] values = resolution.Split ('x');
        if (values.Length == 2) {
            Screen.SetResolution (int.Parse (values[0]), int.Parse (values[1]), resolutionSettings.fullScreenMode);
        } else {
            Screen.SetResolution (Screen.width, Screen.height, resolutionSettings.fullScreenMode);
        }

        resolutionSettings.currentResolution = resolution;

        SaveSettings ();
    }

    public void SetResolution (int resolutionOption) {
        SetResolution (resolutionDropDown.options[resolutionOption].text);
    }

    public void SetFullscreen (int fullscreenModeOption) {
        resolutionSettings.fullScreenMode = (FullScreenMode) fullscreenModeOption;
        SetResolution (resolutionSettings.currentResolution);
    }

}