﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.MobileNotifications {

    public class UIDateTime : MonoBehaviour {

        enum TimeType {
            Hours,
            Minutes,
            Seconds
        }

        [SerializeField] TimeType timeType;
        [SerializeField] Text timeText;
        [SerializeField] ScheduleNotifications scheduleNotifications;

        public void IncreaseTime (bool increase) {
            switch (timeType) {
                case TimeType.Hours:
                    Debug.Log("Adding Hours");
                    timeText.text = scheduleNotifications.AddTimeHours (increase).ToString ();
                    break;
                case TimeType.Minutes:
                    Debug.Log("Adding Minutes");
                    timeText.text = scheduleNotifications.AddTimeMin (increase).ToString ();
                    break;
                case TimeType.Seconds:
                    Debug.Log("Adding Seconds");
                    timeText.text = scheduleNotifications.AddTimeSec (increase).ToString ();
                    break;
            }
        }

    }
}