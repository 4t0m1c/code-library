﻿using System;
using System.Collections;
using System.Collections.Generic;
using NotificationSamples;
using UnityEngine;

namespace At0m1c.MobileNotifications {

    public class ScheduleNotifications : MonoBehaviour {

        [SerializeField] GameNotificationsManager gameNotificationsManager;
        [SerializeField] TimeSpan scheduledDateTime;
        [SerializeField] string notificationTitle;
        [SerializeField] string notificationDescription;

        void Start () {
            GameNotificationChannel channel = new GameNotificationChannel ("channel_notifications", "Default Channel", "General Notifications");
            Debug.Log (channel.Id + " created");
            gameNotificationsManager.Initialize (channel);
            scheduledDateTime = new TimeSpan ();
        }

        void ScheduleNotification () {
            IGameNotification notification = gameNotificationsManager.CreateNotification ();
            if (notification == null) return;

            notification.Title = notificationTitle;
            notification.Body = notificationDescription;
            notification.DeliveryTime = DateTime.Now + scheduledDateTime;

            gameNotificationsManager.ScheduleNotification (notification);
        }

        //UI
        public void UpdateNotificationTitle (string text) {
            notificationTitle = text;
        }

        public void UpdateNotificationDescription (string text) {
            notificationDescription = text;
        }

        public int AddTimeHours (bool increase) {
            if (increase) {
                scheduledDateTime += new TimeSpan (0, 1, 0, 0);
                Debug.Log ("Added Hours: " + scheduledDateTime);
            } else {
                if (scheduledDateTime.Hours > 0) {
                    scheduledDateTime -= new TimeSpan (0, 1, 0, 0);
                }
                Debug.Log ("Removing Hours: " + scheduledDateTime);
            }
            return scheduledDateTime.Hours;
        }

        public int AddTimeMin (bool increase) {
            if (increase) {
                scheduledDateTime += new TimeSpan (0, 0, 1, 0);
                Debug.Log ("Added Minutes: " + scheduledDateTime);
            } else {
                if (scheduledDateTime.Minutes > 0) {
                    scheduledDateTime -= new TimeSpan (0, 0, 1, 0);
                }
                Debug.Log ("Removing Minutes: " + scheduledDateTime);
            }
            return scheduledDateTime.Minutes;
        }

        public int AddTimeSec (bool increase) {
            if (increase) {
                scheduledDateTime += new TimeSpan (0, 0, 0, 1);
                Debug.Log ("Added Seconds: " + scheduledDateTime);
            } else {
                if (scheduledDateTime.Seconds > 0) {
                    scheduledDateTime -= new TimeSpan (0, 0, 0, 1);
                }
                Debug.Log ("Removing Seconds: " + scheduledDateTime);
            }
            return scheduledDateTime.Seconds;
        }

        public void ScheduleNotificationButton () {
            ScheduleNotification ();
        }

    }
}