﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.RuntimeTerrainPainting {
    public class PaintInput : MonoBehaviour {

        TerrainPaintActions terrainPaintActions;
        public static Vector2 MousePosition;

        public delegate void PaintEvents ();
        public static event PaintEvents OnPaintBegin;
        public static event PaintEvents OnPaintEnd;

        void Awake () {
            terrainPaintActions = new TerrainPaintActions ();
            terrainPaintActions.Enable ();
        }

        void Start () {
            terrainPaintActions.Paint.MousePosition.performed += x => { MousePosition = x.ReadValue<Vector2> (); };
            terrainPaintActions.Paint.Paint.performed += x => { if (OnPaintBegin != null) OnPaintBegin (); };
            terrainPaintActions.Paint.Cancel.performed += x => { if (OnPaintEnd != null) OnPaintEnd (); };
        }

    }
}