// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/RuntimeTerrainPainting/Scripts/Input/TerrainPaintActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @TerrainPaintActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @TerrainPaintActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""TerrainPaintActions"",
    ""maps"": [
        {
            ""name"": ""Paint"",
            ""id"": ""9ea8070f-ff60-4c5f-87df-597774a386f2"",
            ""actions"": [
                {
                    ""name"": ""Paint"",
                    ""type"": ""Button"",
                    ""id"": ""d5f5d7d2-b9a9-4048-9b64-8b99b77176cd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""54c6b652-ad6f-41e3-881c-e2c3a49b9325"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""33a06fb0-baa7-4a86-a347-b8b9194d9008"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7f249d5b-1418-4a12-8b2c-21e433a23f54"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Paint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""57cc645b-0351-4471-a1ce-093896ec4821"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0f2983d8-8c0f-4475-bc84-d8bf718db7d2"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Paint
        m_Paint = asset.FindActionMap("Paint", throwIfNotFound: true);
        m_Paint_Paint = m_Paint.FindAction("Paint", throwIfNotFound: true);
        m_Paint_MousePosition = m_Paint.FindAction("MousePosition", throwIfNotFound: true);
        m_Paint_Cancel = m_Paint.FindAction("Cancel", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Paint
    private readonly InputActionMap m_Paint;
    private IPaintActions m_PaintActionsCallbackInterface;
    private readonly InputAction m_Paint_Paint;
    private readonly InputAction m_Paint_MousePosition;
    private readonly InputAction m_Paint_Cancel;
    public struct PaintActions
    {
        private @TerrainPaintActions m_Wrapper;
        public PaintActions(@TerrainPaintActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Paint => m_Wrapper.m_Paint_Paint;
        public InputAction @MousePosition => m_Wrapper.m_Paint_MousePosition;
        public InputAction @Cancel => m_Wrapper.m_Paint_Cancel;
        public InputActionMap Get() { return m_Wrapper.m_Paint; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PaintActions set) { return set.Get(); }
        public void SetCallbacks(IPaintActions instance)
        {
            if (m_Wrapper.m_PaintActionsCallbackInterface != null)
            {
                @Paint.started -= m_Wrapper.m_PaintActionsCallbackInterface.OnPaint;
                @Paint.performed -= m_Wrapper.m_PaintActionsCallbackInterface.OnPaint;
                @Paint.canceled -= m_Wrapper.m_PaintActionsCallbackInterface.OnPaint;
                @MousePosition.started -= m_Wrapper.m_PaintActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_PaintActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_PaintActionsCallbackInterface.OnMousePosition;
                @Cancel.started -= m_Wrapper.m_PaintActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_PaintActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_PaintActionsCallbackInterface.OnCancel;
            }
            m_Wrapper.m_PaintActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Paint.started += instance.OnPaint;
                @Paint.performed += instance.OnPaint;
                @Paint.canceled += instance.OnPaint;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
            }
        }
    }
    public PaintActions @Paint => new PaintActions(this);
    public interface IPaintActions
    {
        void OnPaint(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
    }
}
