﻿using System.Collections;
using UnityEngine;
using UnityEngine.Experimental.TerrainAPI;

namespace At0m1c.RuntimeTerrainPainting {
    public class RuntimeTerrainPainting : MonoBehaviour {

        enum PaintType {
            PointToPoint,
            FreeForm
        }

        Terrain activeTerrain;

        [Header ("Painting Settings")]
        PaintType paintType;
        [SerializeField] Texture brushTexture;
        [SerializeField] float brushSize = 5f;
        [SerializeField] TerrainLayer terrainLayer;
        [SerializeField] LayerMask terrainLayerMask;
        [SerializeField] float paintResolution = 1f;
        float paintResolutionSqr;

        [Header ("Path Accessories")]
        [SerializeField] bool spawnSides = false;
        [SerializeField] GameObject pathBarrierPrefab;

        [Header ("Path Placement")]
        [SerializeField] Transform paintStartGizmo;
        [SerializeField] Transform paintEndGizmo;
        [SerializeField] LineRenderer paintLine;

        Camera camera;
        bool painting = false;
        bool placeNextPoint = false;
        TerrainData origData;
        TerrainData cloneData;

        void Awake () {
            activeTerrain = Terrain.activeTerrain;
            camera = Camera.main;
            paintResolutionSqr = paintResolution * paintResolution;

            //Clone existing terrainData to avoid permanent changes
            origData = activeTerrain.terrainData;
            cloneData = new TerrainData ();
            cloneData = Instantiate (origData);
            activeTerrain.terrainData = cloneData;
        }

        void OnEnable () {
            PaintInput.OnPaintBegin += PaintBegin;
            PaintInput.OnPaintEnd += PaintEnd;
        }

        void OnDisable () {
            PaintInput.OnPaintBegin -= PaintBegin;
            PaintInput.OnPaintEnd -= PaintEnd;
        }

        void PaintBegin () {
            if (painting) {
                //Place next point
                placeNextPoint = true;
            } else {
                //Start up paint coroutine
                Debug.Log ("Begin Painting");

                Ray ray = camera.ScreenPointToRay (PaintInput.MousePosition);
                if (Physics.Raycast (ray, out RaycastHit hit, 5000f, terrainLayerMask)) {
                    painting = true;
                    if (paintType == PaintType.FreeForm) {
                        StartCoroutine (FreeformPaint (hit.point));
                    } else {
                        StartCoroutine (PointToPointPaint (hit.point));

                        paintStartGizmo.gameObject.SetActive (true);
                        paintEndGizmo.gameObject.SetActive (true);
                        paintLine.gameObject.SetActive (true);
                    }
                }
            }
        }

        void PaintEnd () {
            painting = false;

            paintStartGizmo.gameObject.SetActive (false);
            paintEndGizmo.gameObject.SetActive (false);
            paintLine.gameObject.SetActive (false);

            Debug.Log ("End Painting");
        }

        //Point to point painting
        IEnumerator PointToPointPaint (Vector3 startPoint) {
            paintStartGizmo.position = startPoint;

            startPoint -= activeTerrain.transform.position;
            Vector3 lastSuccessfulPaintPoint = startPoint;
            Vector2 terrainUVCoord = new Vector2 (startPoint.x, startPoint.z) / activeTerrain.terrainData.size.x;
            bool lastHitValid = false;

            yield return null;

            while (painting) {
                Ray ray = camera.ScreenPointToRay (PaintInput.MousePosition);
                if (Physics.Raycast (ray, out RaycastHit hit, 5000f, terrainLayerMask)) {
                    paintEndGizmo.position = hit.point;
                    paintLine.positionCount = 2;
                    paintLine.SetPositions (new Vector3[] { paintStartGizmo.position, paintEndGizmo.position });
                    lastHitValid = true;
                } else {
                    lastHitValid = false;
                }
                if (placeNextPoint && lastHitValid) {
                    placeNextPoint = false;

                    //Place first point
                    // Debug.Log ($"Painting at: {terrainUVCoord}");
                    PaintTexture (activeTerrain, terrainUVCoord);

                    //Place each point up to end point following resolution setting
                    hit.point -= activeTerrain.transform.position;
                    Vector3 _directionChange = hit.point - lastSuccessfulPaintPoint;
                    Vector2 directionChange = new Vector2 (_directionChange.x, _directionChange.z);
                    // Debug.Log ($"Direction Change: {directionChange} | Mag: {directionChange.magnitude}");
                    if (directionChange.sqrMagnitude >= paintResolutionSqr) {

                        float iterations = Mathf.Floor (directionChange.magnitude / paintResolution);
                        for (int i = 0; i < iterations; i++) {
                            terrainUVCoord += ((directionChange.normalized * paintResolution) / activeTerrain.terrainData.size.x);
                            PaintTexture (activeTerrain, terrainUVCoord);

                            if (spawnSides && pathBarrierPrefab != null) {
                                GameObject newBarrierLeft = Instantiate (pathBarrierPrefab, activeTerrain.transform);
                                newBarrierLeft.transform.localPosition = (new Vector3 (terrainUVCoord.x, hit.point.y, terrainUVCoord.y) * activeTerrain.terrainData.size.x);
                                newBarrierLeft.transform.forward = _directionChange;
                                newBarrierLeft.transform.Translate ((newBarrierLeft.transform.right * -((brushSize - 5) / 2)) / 2, Space.World);

                                GameObject newBarrierRight = Instantiate (pathBarrierPrefab, activeTerrain.transform);
                                newBarrierRight.transform.localPosition = (new Vector3 (terrainUVCoord.x, hit.point.y, terrainUVCoord.y) * activeTerrain.terrainData.size.x);
                                newBarrierRight.transform.forward = _directionChange;
                                newBarrierRight.transform.Translate ((newBarrierRight.transform.right * ((brushSize - 5) / 2)) / 2, Space.World);
                            }
                        }

                        terrainUVCoord = new Vector2 (hit.point.x, hit.point.z) / activeTerrain.terrainData.size.x;

                        lastSuccessfulPaintPoint = hit.point;
                        paintStartGizmo.position = paintEndGizmo.position;
                    }
                }
                yield return null;
            }
        }

        //Free form painting
        IEnumerator FreeformPaint (Vector3 startPoint) {
            startPoint -= activeTerrain.transform.position;
            Vector3 lastSuccessfulPaintPoint = startPoint;
            Vector2 terrainUVCoord = new Vector2 (startPoint.x, startPoint.z) / activeTerrain.terrainData.size.x;
            // Debug.Log ($"Painting at: {terrainUVCoord}");
            PaintTexture (activeTerrain, terrainUVCoord);

            yield return null;

            while (painting) {
                Ray ray = camera.ScreenPointToRay (PaintInput.MousePosition);
                if (Physics.Raycast (ray, out RaycastHit hit, 5000f, terrainLayerMask)) {
                    hit.point -= activeTerrain.transform.position;
                    Vector3 _directionChange = hit.point - lastSuccessfulPaintPoint;
                    Vector2 directionChange = new Vector2 (_directionChange.x, _directionChange.z);
                    // Debug.Log ($"Direction Change: {directionChange} | Mag: {directionChange.magnitude}");
                    if (directionChange.sqrMagnitude >= paintResolutionSqr) {

                        float iterations = Mathf.Floor (directionChange.magnitude / paintResolution);
                        for (int i = 0; i < iterations; i++) {
                            terrainUVCoord += ((directionChange.normalized * paintResolution) / activeTerrain.terrainData.size.x);
                            PaintTexture (activeTerrain, terrainUVCoord);

                            if (spawnSides && pathBarrierPrefab != null) {
                                GameObject newBarrierLeft = Instantiate (pathBarrierPrefab, activeTerrain.transform);
                                newBarrierLeft.transform.localPosition = (new Vector3 (terrainUVCoord.x, hit.point.y, terrainUVCoord.y) * activeTerrain.terrainData.size.x);
                                newBarrierLeft.transform.forward = _directionChange;
                                newBarrierLeft.transform.Translate ((newBarrierLeft.transform.right * -((brushSize - 5) / 2)) / 2, Space.World);

                                GameObject newBarrierRight = Instantiate (pathBarrierPrefab, activeTerrain.transform);
                                newBarrierRight.transform.localPosition = (new Vector3 (terrainUVCoord.x, hit.point.y, terrainUVCoord.y) * activeTerrain.terrainData.size.x);
                                newBarrierRight.transform.forward = _directionChange;
                                newBarrierRight.transform.Translate ((newBarrierRight.transform.right * ((brushSize - 5) / 2)) / 2, Space.World);
                            }
                        }

                        terrainUVCoord = new Vector2 (hit.point.x, hit.point.z) / activeTerrain.terrainData.size.x;

                        lastSuccessfulPaintPoint = hit.point;
                    }
                }
                yield return null;
            }
        }

        //Extracted from https://github.com/Roland09/PathPaintTool
        private bool PaintTexture (Terrain terrain, Vector2 currUV) {
            float brushSize = this.brushSize;

            BrushTransform brushXform = TerrainPaintUtility.CalculateBrushTransform (terrain, currUV, brushSize, 0f);
            PaintContext paintContext = TerrainPaintUtility.BeginPaintTexture (terrain, brushXform.GetBrushXYBounds (), terrainLayer);

            if (paintContext == null)
                return false;

            Material mat = TerrainPaintUtility.GetBuiltinPaintMaterial ();

            float targetAlpha = 1.0f;
            float brushStrength = 100f / 100f;

            Vector4 brushParams = new Vector4 (brushStrength, targetAlpha, 0.0f, 0.0f);
            mat.SetTexture ("_BrushTex", brushTexture);
            mat.SetVector ("_BrushParams", brushParams);

            TerrainPaintUtility.SetupTerrainToolMaterialProperties (paintContext, brushXform, mat);

            Graphics.Blit (paintContext.sourceRenderTexture, paintContext.destinationRenderTexture, mat, (int) TerrainPaintUtility.BuiltinPaintMaterialPasses.PaintTexture);

            TerrainPaintUtility.EndPaintTexture (paintContext, "Terrain Paint - Texture");

            return true;
        }
    }
}