﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.ObjectPooling {
    public class Spawner : MonoBehaviour {

        [SerializeField] ObjectPoolSpawnableObject spawnPool;

        List<SpawnableObject> spawned = new List<SpawnableObject> ();

        void Start () {
            StartCoroutine (Spawn ());
            StartCoroutine (Kill ());
        }

        IEnumerator Spawn () {
            while (true) {
                SpawnableObject opObject = spawnPool.GetFromObjectPool ();
                spawned.Add (opObject);
                opObject.transform.position = new Vector3 (Random.Range (-5f, 5f), 0, Random.Range (-5f, 5f));

                yield return new WaitForSeconds (Random.Range (1f, 3f));
            }
        }

        IEnumerator Kill () {
            while (true) {
                if (spawned.Count > 0) {
                    SpawnableObject randomObject = spawned[Random.Range (0, spawned.Count)];

                    spawned.Remove (randomObject);
                    spawnPool.PutIntoObjectPool (randomObject);

                    yield return new WaitForSeconds (Random.Range (1.5f, 4f));
                } else {
                    yield return null;
                }
            }
        }

    }
}