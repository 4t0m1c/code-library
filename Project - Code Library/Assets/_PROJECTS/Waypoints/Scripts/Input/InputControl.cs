// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/Waypoints/Scripts/Input/InputControl.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace At0m1c.TurretDemo
{
    public class @InputControl : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @InputControl()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControl"",
    ""maps"": [
        {
            ""name"": ""Control"",
            ""id"": ""212551ae-a882-4d8e-b830-e7bcc07c2c5a"",
            ""actions"": [
                {
                    ""name"": ""AimTarget"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5ab9f389-5d15-455a-b905-246b2f52907b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""f2592740-aa7c-4176-bd0e-9dd5cf799368"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Screenshot"",
                    ""type"": ""Button"",
                    ""id"": ""6b2b31ac-a1d0-4380-9c3c-9141cf0ab86c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""260f0535-89fb-46ec-9873-11aa0bcdd13f"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimTarget"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de07a1da-43be-4898-9c49-d6ee3ffb6fea"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f0d1c679-11c4-40cc-b7e3-221b8a4fa155"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e1006cc4-bc3a-465c-8c31-907d46ca706b"",
                    ""path"": ""<Keyboard>/f12"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Screenshot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Control
            m_Control = asset.FindActionMap("Control", throwIfNotFound: true);
            m_Control_AimTarget = m_Control.FindAction("AimTarget", throwIfNotFound: true);
            m_Control_Fire = m_Control.FindAction("Fire", throwIfNotFound: true);
            m_Control_Screenshot = m_Control.FindAction("Screenshot", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Control
        private readonly InputActionMap m_Control;
        private IControlActions m_ControlActionsCallbackInterface;
        private readonly InputAction m_Control_AimTarget;
        private readonly InputAction m_Control_Fire;
        private readonly InputAction m_Control_Screenshot;
        public struct ControlActions
        {
            private @InputControl m_Wrapper;
            public ControlActions(@InputControl wrapper) { m_Wrapper = wrapper; }
            public InputAction @AimTarget => m_Wrapper.m_Control_AimTarget;
            public InputAction @Fire => m_Wrapper.m_Control_Fire;
            public InputAction @Screenshot => m_Wrapper.m_Control_Screenshot;
            public InputActionMap Get() { return m_Wrapper.m_Control; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(ControlActions set) { return set.Get(); }
            public void SetCallbacks(IControlActions instance)
            {
                if (m_Wrapper.m_ControlActionsCallbackInterface != null)
                {
                    @AimTarget.started -= m_Wrapper.m_ControlActionsCallbackInterface.OnAimTarget;
                    @AimTarget.performed -= m_Wrapper.m_ControlActionsCallbackInterface.OnAimTarget;
                    @AimTarget.canceled -= m_Wrapper.m_ControlActionsCallbackInterface.OnAimTarget;
                    @Fire.started -= m_Wrapper.m_ControlActionsCallbackInterface.OnFire;
                    @Fire.performed -= m_Wrapper.m_ControlActionsCallbackInterface.OnFire;
                    @Fire.canceled -= m_Wrapper.m_ControlActionsCallbackInterface.OnFire;
                    @Screenshot.started -= m_Wrapper.m_ControlActionsCallbackInterface.OnScreenshot;
                    @Screenshot.performed -= m_Wrapper.m_ControlActionsCallbackInterface.OnScreenshot;
                    @Screenshot.canceled -= m_Wrapper.m_ControlActionsCallbackInterface.OnScreenshot;
                }
                m_Wrapper.m_ControlActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @AimTarget.started += instance.OnAimTarget;
                    @AimTarget.performed += instance.OnAimTarget;
                    @AimTarget.canceled += instance.OnAimTarget;
                    @Fire.started += instance.OnFire;
                    @Fire.performed += instance.OnFire;
                    @Fire.canceled += instance.OnFire;
                    @Screenshot.started += instance.OnScreenshot;
                    @Screenshot.performed += instance.OnScreenshot;
                    @Screenshot.canceled += instance.OnScreenshot;
                }
            }
        }
        public ControlActions @Control => new ControlActions(this);
        public interface IControlActions
        {
            void OnAimTarget(InputAction.CallbackContext context);
            void OnFire(InputAction.CallbackContext context);
            void OnScreenshot(InputAction.CallbackContext context);
        }
    }
}
