﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.TurretDemo {
    public class TurretBehaviour : MonoBehaviour {

        /*
            Use the aimTarget property from InputManager to aim the turret.
            Use the OnFire event from InputManager to fire a projectile at the target.
        */

        [SerializeField] GameObject projectilePrefab;
        [SerializeField] Transform projectileSpawn;

        void OnEnable () {
            InputManager.OnFire += Fire;
        }

        void OnDisable () {
            InputManager.OnFire -= Fire;
        }

        void Fire () {
            GameObject newProjectile = Instantiate (projectilePrefab, projectileSpawn.position, projectileSpawn.rotation);
        }

        void FixedUpdate () {
            transform.LookAt (InputManager.lastRaycastHit.point);
            transform.localEulerAngles = new Vector3 (0, transform.localEulerAngles.y, 0);
        }

    }
}