﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.TurretDemo {
    public class EnemyBehaviour : MonoBehaviour {

        [SerializeField] float moveSpeed = 2;
        [Header ("Debug")]
        [SerializeField] List<Transform> waypoints = new List<Transform> ();
        [SerializeField] Transform currentTarget;

        public void SetDestinations (List<Transform> destinations) {
            waypoints = destinations;
            if (waypoints.Count > 0)
                currentTarget = waypoints[0];
        }

        void Update () {
            if (currentTarget != null) {
                transform.position = Vector3.MoveTowards (transform.position, currentTarget.position, Time.deltaTime * moveSpeed);
                transform.LookAt (currentTarget);
                transform.localEulerAngles = new Vector3 (0, transform.localEulerAngles.y, 0);

                if ((transform.position - currentTarget.position).sqrMagnitude <= 0.01f) {
                    int currentIndex = waypoints.IndexOf (currentTarget);
                    if (currentIndex < waypoints.Count - 1) {
                        currentIndex++;
                    } else {
                        currentIndex = 0;
                    }
                    currentTarget = waypoints[currentIndex];
                }
            }
        }

        public void Die () {
            Destroy (gameObject);
        }
    }
}