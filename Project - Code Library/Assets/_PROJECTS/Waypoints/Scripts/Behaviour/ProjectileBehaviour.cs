﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.TurretDemo {
    public class ProjectileBehaviour : MonoBehaviour {

        [SerializeField] float timeOut = 5;
        [SerializeField] float explodeRadius = 2;
        [SerializeField] float projectileSpeed = 5;
        [SerializeField] LayerMask enemyMask;

        void Update () {
            timeOut -= Time.deltaTime;
            if (timeOut <= 0) Explode ();

            transform.Translate (Vector3.forward * projectileSpeed * Time.deltaTime);
        }

        void Explode () {
            Collider[] inRange = Physics.OverlapSphere (transform.position, explodeRadius, enemyMask, QueryTriggerInteraction.Ignore);
            foreach (Collider col in inRange) {
                if (col.attachedRigidbody != null) {
                    col.attachedRigidbody.GetComponent<EnemyBehaviour> ().Die ();
                }
            }

            Destroy (gameObject);
        }

        void OnTriggerEnter (Collider other) {
            if (other.attachedRigidbody != null) {
                if (((1 << other.attachedRigidbody.gameObject.layer) | enemyMask) == enemyMask) {
                    Debug.Log ("Hit enemy!");
                    Explode ();
                }
            }
        }

    }
}