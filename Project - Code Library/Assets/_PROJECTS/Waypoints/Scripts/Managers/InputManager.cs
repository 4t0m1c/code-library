﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.TurretDemo {
    public class InputManager : MonoBehaviour {
        InputControl inputControl;

        public delegate void InputEvents ();
        public static event InputEvents OnFire;

        public static Vector2 mousePosition; //Mouse position
        public static RaycastHit lastRaycastHit;

        new Camera camera;

        [SerializeField] LayerMask hitMask;

        void Awake () {
            camera = Camera.main;
            inputControl = new InputControl ();
            inputControl.Enable ();

            inputControl.Control.AimTarget.performed += x => mousePosition = x.ReadValue<Vector2> ();

            inputControl.Control.Fire.performed += x => {
                if (OnFire != null) OnFire ();
            };

            inputControl.Control.Screenshot.performed += x => {
                ScreenCapture.CaptureScreenshot ("Screenshot.png", 6);
            };
        }

        void FixedUpdate () {
            Ray ray = camera.ScreenPointToRay (mousePosition);
            if (Physics.Raycast (ray, out RaycastHit hit, 500, hitMask, QueryTriggerInteraction.Ignore)) {
                lastRaycastHit = hit;
            }
        }

    }
}