﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.TurretDemo {
    public class SpawnManager : MonoBehaviour {

        [Header ("Enemy")]
        [SerializeField] GameObject enemyPrefab;
        [SerializeField] List<Transform> waypoints = new List<Transform> ();
        [SerializeField] float spawnInterval = 1;
        float currentInterval = 0;

        [Header ("Turret")]
        [SerializeField] GameObject turretPrefab;
        [SerializeField] List<Transform> spawnPositions = new List<Transform> ();

        void Start () {
            for (int i = 0; i < spawnPositions.Count; i++) {
                GameObject newTurret = Instantiate (turretPrefab, spawnPositions[i].position, Quaternion.identity);
            }
        }

        void Update () {
            if (waypoints.Count > 1) {
                if (currentInterval > 0) {
                    currentInterval -= Time.deltaTime;
                } else {
                    GameObject newEnemy = Instantiate (enemyPrefab, waypoints[0].position, Quaternion.identity);
                    newEnemy.GetComponent<EnemyBehaviour> ().SetDestinations (waypoints);
                    currentInterval = spawnInterval;
                }
            } else {
                Debug.Log ("<color=red>Not Enough Waypoints</color>");
            }
        }

        void OnDrawGizmos () {
            Gizmos.color = Color.yellow;
            if (waypoints.Count > 1) {
                Gizmos.DrawWireSphere (waypoints[0].position, 1);
                for (int i = 1; i < waypoints.Count; i++) {
                    Gizmos.DrawLine (waypoints[i - 1].position, waypoints[i].position);
                }
                Gizmos.DrawLine (waypoints[waypoints.Count - 1].position, waypoints[0].position);
            }

            Gizmos.color = Color.cyan;
            if (spawnPositions.Count > 0) {
                for (int i = 0; i < spawnPositions.Count; i++) {
                    Gizmos.DrawWireSphere (spawnPositions[i].position, 1);
                }
            }
        }
    }
}