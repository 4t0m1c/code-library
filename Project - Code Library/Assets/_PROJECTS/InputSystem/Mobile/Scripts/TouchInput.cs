﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TouchInput : MonoBehaviour {

    [SerializeField] Transform transformToMatchPosition;

    //Input
    AndroidDemoBuild input_AndroidDemoBuild;

    void Awake () {
        input_AndroidDemoBuild = new AndroidDemoBuild ();
        input_AndroidDemoBuild.Enable ();
    }

    void OnEnable () {
        input_AndroidDemoBuild.Player.TouchPosition.performed += UpdateTouchPosition;
    }

    void OnDisable () {
        input_AndroidDemoBuild.Player.TouchPosition.performed -= UpdateTouchPosition;
    }

    public void UpdateTouchPosition (InputAction.CallbackContext ctx) {
        Vector2 touchPos = ctx.ReadValue<Vector2> ();
        transformToMatchPosition.position = Camera.main.ScreenToWorldPoint (new Vector3 (touchPos.x, touchPos.y, 10f));
    }

}