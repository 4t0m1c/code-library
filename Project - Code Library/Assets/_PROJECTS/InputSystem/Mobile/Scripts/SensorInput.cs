﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace InputSystemMobileDemo {

    /* 
        Build profile password is the same as the filename: "testbuild"
    */

    public class SensorInput : MonoBehaviour {

        [Header ("References")]
        [SerializeField] Text outputTextField;

        [Header ("Outputs")]
        [SerializeField] Vector3 accelerometer;
        [SerializeField] Vector3 gyro;
        [SerializeField] Vector3 gravity;
        [SerializeField] Quaternion attitude;
        [SerializeField] float stepCounter;
        [SerializeField] float lightLevel;

        enum Metrics {
            Gyro,
            Accelerometer,
            Gravity,
            Attitude,
            StepCounter,
            LightLevel
        }
        Metrics selectedMetric;
        string selectedMetricText;

        void OnEnable () {
            InputSystem.EnableDevice (UnityEngine.InputSystem.Gyroscope.current);
            InputSystem.EnableDevice (UnityEngine.InputSystem.Accelerometer.current);
            InputSystem.EnableDevice (UnityEngine.InputSystem.GravitySensor.current);
            InputSystem.EnableDevice (UnityEngine.InputSystem.AttitudeSensor.current);
            InputSystem.EnableDevice (UnityEngine.InputSystem.StepCounter.current);
            InputSystem.EnableDevice (UnityEngine.InputSystem.LightSensor.current);
        }

        void OnDisable () {
            InputSystem.DisableDevice (UnityEngine.InputSystem.Gyroscope.current);
            InputSystem.DisableDevice (UnityEngine.InputSystem.Accelerometer.current);
            InputSystem.DisableDevice (UnityEngine.InputSystem.GravitySensor.current);
            InputSystem.DisableDevice (UnityEngine.InputSystem.AttitudeSensor.current);
            InputSystem.DisableDevice (UnityEngine.InputSystem.StepCounter.current);
            InputSystem.DisableDevice (UnityEngine.InputSystem.LightSensor.current);
        }

        void Update () {
            MetricAccelerometer ();
            MetricGyro ();
            MetricGravity ();
            MetricAttitude ();
            MetricStepCounter ();
            MetricLightLevel ();

            if (outputTextField != null)
                outputTextField.text = selectedMetricText;
        }

        public void MetricAccelerometer () {
            if (UnityEngine.InputSystem.Accelerometer.current != null)
                accelerometer = UnityEngine.InputSystem.Accelerometer.current.acceleration.ReadValue ();
            if (selectedMetric == Metrics.Accelerometer) selectedMetricText = $"accelerometer: {accelerometer}";
        }

        public void MetricGyro () {
            if (UnityEngine.InputSystem.Gyroscope.current != null)
                gyro = UnityEngine.InputSystem.Gyroscope.current.angularVelocity.ReadValue ();
            Debug.Log ($"gyro: {gyro}");
        }

        public void MetricGravity () {
            if (UnityEngine.InputSystem.GravitySensor.current != null)
                gravity = UnityEngine.InputSystem.GravitySensor.current.gravity.ReadValue ();
            if (selectedMetric == Metrics.Gravity) selectedMetricText = $"gravity: {gravity}";
        }

        public void MetricAttitude () {
            if (UnityEngine.InputSystem.AttitudeSensor.current != null)
                attitude = UnityEngine.InputSystem.AttitudeSensor.current.attitude.ReadValue ();
            if (selectedMetric == Metrics.Attitude) selectedMetricText = $"attitude: {attitude}";
        }

        public void MetricStepCounter () {
            if (UnityEngine.InputSystem.StepCounter.current != null)
                stepCounter = UnityEngine.InputSystem.StepCounter.current.stepCounter.ReadValue ();
            if (selectedMetric == Metrics.StepCounter) selectedMetricText = $"stepCounter: {stepCounter}";
        }

        public void MetricLightLevel () {
            if (UnityEngine.InputSystem.LightSensor.current != null)
                lightLevel = UnityEngine.InputSystem.LightSensor.current.lightLevel.ReadValue ();
            if (selectedMetric == Metrics.LightLevel) selectedMetricText = $"lightLevel: {lightLevel}";
        }

        /* 
            UI stuff
        */

        public void SetSelectedMetric (int metric) {
            selectedMetric = (Metrics) metric;
        }

    }

}