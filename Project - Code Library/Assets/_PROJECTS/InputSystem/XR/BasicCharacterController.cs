﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace At0m1c.InputSystemXR {
    public class BasicCharacterController : MonoBehaviour {

        XRControls xrControls;

        [SerializeField] Vector2 moveVector;
        [SerializeField] float speed;
        [SerializeField] bool faceDirectionOfTravel = false;

        void Awake () {
            xrControls = new XRControls ();
            xrControls.Enable ();
        }

        void Start () {
            //We need to subscribe to the actions
            //By having Generate C# enabled, all actions and maps are generated as C# classes. 
            //We can then select what event of the action we'd like to subscribe to (started, performed, cancelled)
            //We use a lambda expression " x => " to represent the input context as "x" and simply read a single value from it instead of the whole class
            //Make sure that the action properties and the ReadValue match
            xrControls.Player.Move.performed += x => moveVector = x.ReadValue<Vector2> ();
        }

        void Update () {
            DoMove ();
        }

        void DoMove () {
            transform.Translate (new Vector3 (moveVector.x, 0, moveVector.y) * Time.deltaTime * speed, Space.World);
            if (faceDirectionOfTravel) {
                transform.forward = Vector3.Lerp (transform.forward, new Vector3 (moveVector.x, 0, moveVector.y), Time.deltaTime * 20);
            }
        }

    }
}