﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BasicCharacterController : MonoBehaviour {

    enum ControlType {
        Translate,
        Rigidbody
    }

    [SerializeField] ControlType controlType;
    [SerializeField] float moveSpeed = 10;

    BasicControls basicControls;
    Vector2 moveVector;
    Rigidbody rigidbody;

    void Awake () {
        rigidbody = GetComponent<Rigidbody> ();

        basicControls = new BasicControls ();
        basicControls.Enable ();
    }

    void Start () {
        //We need to subscribe to the actions
        //By having Generate C# enabled, all actions and maps are generated as C# classes. 
        //We can then select what event of the action we'd like to subscribe to (started, performed, cancelled)
        //We use a lambda expression " x => " to represent the input context as "x" and simply read a single value from it instead of the whole class
        //Make sure that the action properties and the ReadValue match
        basicControls.Player.Move.performed += x => moveVector = x.ReadValue<Vector2> ();
    }

    void FixedUpdate () {
        DoMove ();
    }

    void DoMove () {
        if (controlType == ControlType.Translate) {
            transform.Translate (new Vector3 (moveVector.x, 0, moveVector.y) * Time.fixedDeltaTime * moveSpeed, Space.Self);
        }
        if (controlType == ControlType.Rigidbody && rigidbody != null) {
            rigidbody.velocity = (new Vector3 (moveVector.x, rigidbody.velocity.y, moveVector.y) * Time.fixedDeltaTime * moveSpeed * 40);
        }
    }

}