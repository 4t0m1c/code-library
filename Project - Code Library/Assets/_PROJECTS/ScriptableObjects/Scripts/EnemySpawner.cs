﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.ScriptableObjects {
    public class EnemySpawner : MonoBehaviour {

        [SerializeField] List<EnemyData> enemies = new List<EnemyData> ();
        [SerializeField] bool dontRepeatSpawn = true;
        EnemyData lastSpawn;

        float counter = 1f;

        void Update () {
            if (counter > 0) {
                //Counts down by Time.deltaTime (realtime) | 60 fps = 1 sec / 60
                counter -= Time.deltaTime;
            } else {
                counter = 1f; //Reset the counter
                SpawnRandomEnemy (); //Spawn the enemy
            }
        }

        void SpawnRandomEnemy () {
            EnemyData randomEnemy = enemies[Random.Range (0, enemies.Count)];

            if (dontRepeatSpawn) {
                if (lastSpawn == randomEnemy){
                    SpawnRandomEnemy(); //Calls the function again to try get another random enemy that isn't the same
                    return; //Stops this function
                }
            }

            Vector3 randomPosition = new Vector3 (Random.Range (-5f, 5f), 0, Random.Range (-5f, 5f));
            Instantiate (randomEnemy.enemyPrefab, randomPosition, Quaternion.identity);
            
            lastSpawn = randomEnemy;
        }

    }
}