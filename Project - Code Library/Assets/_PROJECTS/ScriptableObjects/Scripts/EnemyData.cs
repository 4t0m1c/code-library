﻿using UnityEngine;

namespace At0m1c.ScriptableObjects {

    public enum Race {
        Orc,
        Elf,
        Gnome,
        Fairy
    }

    [System.Serializable]
    public struct AttackParams {
        public float attackRate;
        public float attackDamage;
    }

    [CreateAssetMenu (fileName = "EnemyData", menuName = "EnemyData", order = 0)]
    public class EnemyData : ScriptableObject {

        public GameObject enemyPrefab;
        public new string name;
        public Race race;
        public AttackParams attackParams;
        public float armour;

        public float CalcArmour () {
            return armour;
        }

    }
}