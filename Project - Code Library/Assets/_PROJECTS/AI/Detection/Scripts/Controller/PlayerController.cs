﻿using System.Collections;
using System.Collections.Generic;
using At0m1c.HideAndSeek;
using UnityEngine;

namespace At0m1c.Controller {

    public class PlayerController : MonoBehaviour {

        Rigidbody rigidbody;

        [Header ("Aim")]
        public LayerMask hitMask;

        [Header ("Movement")]
        [SerializeField] float moveSpeed;
        [SerializeField] float lookSpeed;
        [SerializeField] float jumpForce;
        [SerializeField] float jumpTimeout;
        float jumpTimeoutCurrent;
        [SerializeField] LayerMask groundMask;
        bool grounded = true;
        bool sprinting = false;
        bool active = true;

        [Header ("References")]
        public Transform playerHeadTransform;
        [SerializeField] Transform playerShoulderTransform;


        //Input
        PlayerInput playerInput;
        Vector2 moveVector;
        Vector2 lookVector;

        /*
            Player Controller
            Uses the Input System
            Enables/Disables Cursor
        */

        void Awake () {
            playerInput = new PlayerInput ();
            playerInput.Enable ();

            rigidbody = GetComponent<Rigidbody> ();

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        void OnEnable () {
            GameManager.OnGameOver += GameOver;
        }

        void OnDisable () {
            GameManager.OnGameOver -= GameOver;
        }

        void GameOver () {
            playerInput.Disable ();
            lookVector = Vector2.zero;
            moveVector = Vector2.zero;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void Start () {
            playerInput.Player.Jump.performed += x => Jump ();
            playerInput.Player.Sprint.started += x => Sprint (true);
            playerInput.Player.Sprint.canceled += x => Sprint (false);
            playerInput.Player.Move.performed += x => moveVector = x.ReadValue<Vector2> ();
            playerInput.Player.Look.performed += x => lookVector = x.ReadValue<Vector2> ();
        }

        void Update () {
            if (!active) return;

            GroundCheck ();
            Move (moveVector);
            Look (lookVector);

        }

        void FixedUpdate () {
            if (!active) return;

            Aim ();
        }

        /*
            ===== ACTIONS =====
        */

        public void Sprint (bool enabled) {
            sprinting = enabled;
        }

        public void Move (Vector2 moveVector) {
            if (!grounded) moveVector /= 1.5f;

            Vector3 localVelocity = transform.TransformDirection (new Vector3 (moveVector.x, 0, moveVector.y) * moveSpeed * (sprinting ? 1.5f : 1f));
            rigidbody.velocity = new Vector3 (localVelocity.x, rigidbody.velocity.y, localVelocity.z);
        }

        public void Look (Vector2 lookVector) {
            transform.Rotate (Vector3.up * lookSpeed * lookVector.x); //Horizontal look
            playerHeadTransform.Rotate (-Vector3.right * lookSpeed * lookVector.y); //Vertical look
            playerHeadTransform.localEulerAngles = new Vector3 (playerHeadTransform.localEulerAngles.x.ClampAngle (-89f, 90f), 0, 0);
        }

        public void Jump () {
            if (grounded && jumpTimeoutCurrent <= 0) {
                jumpTimeoutCurrent = jumpTimeout;
                rigidbody.AddForce (Vector3.up * jumpForce, ForceMode.Impulse);
                grounded = false;
            }
        }

        /*
            ===== CHECKS =====
        */

        void Aim () {
            if (!Physics.Raycast (playerHeadTransform.position, playerHeadTransform.forward, out RaycastHit hit, 50, hitMask, QueryTriggerInteraction.Ignore)) {
                hit.point = playerHeadTransform.position + playerHeadTransform.forward * 5;
            }
            Quaternion targetRotation = Quaternion.LookRotation (hit.point - playerShoulderTransform.position, -Vector3.up);
            playerShoulderTransform.rotation = Quaternion.Lerp (playerShoulderTransform.rotation, targetRotation, Time.deltaTime * 100);
        }

        void GroundCheck () {
            if (jumpTimeoutCurrent > 0) {
                jumpTimeoutCurrent -= Time.deltaTime;
            }

            if (Physics.SphereCast (transform.position + Vector3.up, 0.5f, Vector3.down, out RaycastHit hit, 1f, groundMask, QueryTriggerInteraction.Ignore)) {
                grounded = true;
                rigidbody.AddForce (Vector3.down * (jumpForce / 2));
            } else {
                grounded = false;
                if (rigidbody.velocity.y < 0) {
                    rigidbody.AddForce (Vector3.down * jumpForce);
                }
            }
        }

    }
}