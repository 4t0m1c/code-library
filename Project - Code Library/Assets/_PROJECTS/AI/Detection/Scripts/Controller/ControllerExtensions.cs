using UnityEngine;

namespace At0m1c.Controller {
    public static class ControllerExtensions {
        public static float ClampAngle (this float angle, float min, float max) {
            angle = Mathf.Repeat (angle, 360);
            min = Mathf.Repeat (min, 360);
            max = Mathf.Repeat (max, 360);
            bool inverse = false;
            var tmin = min;
            var tangle = angle;
            if (min > 180) {
                inverse = !inverse;
                tmin -= 180;
            }
            if (angle > 180) {
                inverse = !inverse;
                tangle -= 180;
            }
            var result = !inverse ? tangle > tmin : tangle < tmin;
            if (!result)
                angle = min;

            inverse = false;
            tangle = angle;
            var tmax = max;
            if (angle > 180) {
                inverse = !inverse;
                tangle -= 180;
            }
            if (max > 180) {
                inverse = !inverse;
                tmax -= 180;
            }

            result = !inverse ? tangle<tmax : tangle> tmax;
            if (!result)
                angle = max;
            return angle;
        }

        public static void IgnoreCollisions (this Collider source, Collider[] others) {
            foreach (var item in others) {
                Physics.IgnoreCollision (source, item);
            }
        }

        public static bool IsValid (this Vector3 v) {
            return !float.IsNaN (v.x) && !float.IsNaN (v.y) && !float.IsNaN (v.z) && !float.IsInfinity (v.x) && !float.IsInfinity (v.y) && !float.IsInfinity (v.z);
        }
    }
}