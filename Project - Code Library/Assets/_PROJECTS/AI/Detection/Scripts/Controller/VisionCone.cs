﻿using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Controller {

    public class VisionCone : MonoBehaviour {

        //For use with the AI
        //Detects if the player is within the vision cone of the enemy
        //Include a raycast in your enemy logic to detect if there is direct line of sight

        public delegate void VisionConeEvents (GameObject player);
        public VisionConeEvents OnPlayerEnteredVision;
        public VisionConeEvents OnPlayerExitedVision;

        List<GameObject> playersInVision = new List<GameObject> ();

        void OnTriggerEnter (Collider other) {
            if (other.attachedRigidbody != null && other.attachedRigidbody.gameObject.layer == 14) { //Player Layer
                if (!playersInVision.Contains (other.attachedRigidbody.gameObject)) {
                    playersInVision.Add (other.attachedRigidbody.gameObject);
                    if (OnPlayerEnteredVision != null) OnPlayerEnteredVision (other.attachedRigidbody.gameObject);
                    Debug.Log("Player entered vision cone");
                }
            }
        }

        void OnTriggerExit (Collider other) {
            if (other.attachedRigidbody != null && other.attachedRigidbody.gameObject.layer == 14) { //Player Layer
                if (playersInVision.Contains (other.attachedRigidbody.gameObject)) {
                    playersInVision.Remove (other.attachedRigidbody.gameObject);
                    if (OnPlayerExitedVision != null) OnPlayerExitedVision (other.attachedRigidbody.gameObject);
                    Debug.Log("Player exited vision cone");
                }
            }
        }

    }
}