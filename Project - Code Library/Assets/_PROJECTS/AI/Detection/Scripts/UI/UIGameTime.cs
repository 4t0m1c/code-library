﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.HideAndSeek {
    public class UIGameTime : MonoBehaviour {

        //Displays amount of time survived

        Text text;
        float currentTime = 0;
        bool gameOver = false;

        void Awake () {
            text = GetComponent<Text> ();
        }

        void OnEnable () {
            GameManager.OnGameOver += GameOver;
        }

        void OnDisable () {
            GameManager.OnGameOver -= GameOver;
        }

        void GameOver () {
            gameOver = true;
        }

        void Update () {
            if (!gameOver) {
                currentTime += Time.deltaTime;

                float minutes = Mathf.Floor (currentTime / 60);
                float seconds = Mathf.RoundToInt (currentTime % 60);

                text.text = minutes.ToString ("00") + ":" + seconds.ToString ("00");
            }
        }
    }
}