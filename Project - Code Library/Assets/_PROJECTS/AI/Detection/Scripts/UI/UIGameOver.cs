﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.HideAndSeek {
    public class UIGameOver : MonoBehaviour {

        //Shows game over screen with try again button

        [SerializeField] Animator GameOverAnimator;

        void OnEnable () {
            GameManager.OnGameOver += GameOver;
        }

        void OnDisable () {
            GameManager.OnGameOver -= GameOver;
        }

        void GameOver () {
            GameOverAnimator.SetTrigger("FadeIn");
        }

    }
}