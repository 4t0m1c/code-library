﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.HideAndSeek {
    public class PlayerSense : MonoBehaviour {

        //End the game if we collide with the enemy

        void OnCollisionEnter (Collision other) {
            if (other.collider.attachedRigidbody != null) {
                if (other.collider.attachedRigidbody.gameObject.layer == 13) { //Hit the enemy layer
                    GameManager.instance.GameOver ();
                }
            }
        }
    }
}