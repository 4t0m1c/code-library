﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.HideAndSeek {
    public class Level_AddStep : MonoBehaviour {

        //This script is just used to make it easier to make steps with cubes

        [SerializeField] int stepCount = 10;
        [SerializeField] GameObject stepPrefab;

        [ContextMenu ("Add Step")]
        public void AddSteps () {
            for (int i = 0; i < stepCount; i++) {
                GameObject newStep = Instantiate (stepPrefab, transform);
                newStep.transform.localPosition += new Vector3 (0, 0, i * 0.5f);
                newStep.transform.localScale += new Vector3 (0, i * 0.5f, 0);
            }
        }

    }
}