﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace At0m1c.HideAndSeek {
    public class GameManager : MonoBehaviour {

        //Adjusts TimeScale to pause the game
        //Event to notify when game is over

        public static GameManager instance;

        public delegate void GameEvents ();
        public static GameEvents OnGameOver;

        void Awake () {
            instance = this;
            Time.timeScale = 1;
        }

        public void GameOver () {
            Time.timeScale = 0;
            if (OnGameOver != null) OnGameOver ();
        }

        public void TryAgain () {
            Debug.Log("Try again");
            SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
        }

    }
}