﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace At0m1c.AI {
    public class EnemyBehaviour : MonoBehaviour {

        [SerializeField] LayerMask viewMask;

        List<Transform> waypoints = new List<Transform> ();
        Transform lastTarget;
        Transform currentTarget;
        NavMeshAgent agent;
        PlayerBehaviour targetPlayer;

        bool chasingPlayer = false;

        void Awake () {
            agent = GetComponent<NavMeshAgent> ();

            agent.updatePosition = false;
            agent.updateRotation = false;
        }

        void Update () {
            UpdateNavAgent ();
        }

        public void SetDestinations (List<Transform> destinations) {
            waypoints = destinations;
            if (waypoints.Count > 0) {
                currentTarget = FindNextWayPoint ();
            }
            StartCoroutine (GoToWaypoints ());
        }

        IEnumerator GoToWaypoints () {
            while (true) {
                if (chasingPlayer) {
                    bool hitPlayer = false;
                    if (Physics.SphereCast (transform.position + Vector3.up + (Vector3.forward * 0.5f), 0.5f, (targetPlayer.transform.position - transform.position), out RaycastHit hit, 10f, viewMask, QueryTriggerInteraction.Ignore)) {
                        // Debug.Log (hit.collider.name, hit.collider.gameObject);
                        if (hit.collider.attachedRigidbody != null && hit.collider.attachedRigidbody.transform == targetPlayer.transform) {
                            Debug.Log ("Direct Line of Sight: " + hit.collider.name);
                            Debug.DrawLine (transform.position + Vector3.up + (Vector3.forward * 0.5f), hit.point);
                            currentTarget = targetPlayer.transform;
                            agent.SetDestination (currentTarget.position);
                            hitPlayer = true;
                        }
                    }

                    if (!hitPlayer && targetPlayer.transform == currentTarget) {
                        currentTarget = FindNextWayPoint ();
                    }
                }

                agent.SetDestination (currentTarget.position);
                while (!agent.hasPath && !chasingPlayer) {
                    yield return null;
                }
                while (agent.remainingDistance > agent.radius && !chasingPlayer) {
                    yield return null;
                }

                if (!chasingPlayer) {
                    NextTarget ();
                }

                yield return null;
            }
        }

        void NextTarget () {
            int currentIndex = waypoints.IndexOf (currentTarget);
            if (currentIndex < waypoints.Count - 1) {
                currentIndex++;
            } else {
                currentIndex = 0;
            }
            currentTarget = waypoints[currentIndex];
        }

        void UpdateNavAgent () {
            if (agent.hasPath && !agent.isStopped) {
                NavMeshHit hit;
                float maxAgentTravelDistance = Time.deltaTime * agent.speed;

                if (agent.SamplePathPosition (NavMesh.AllAreas, maxAgentTravelDistance, out hit) ||
                    agent.remainingDistance <= agent.stoppingDistance) { //Agent stopped
                } else { //Else, move the actor and manually update the agent pos and rotation
                    agent.velocity = hit.position - transform.position;
                    transform.position = Vector3.Lerp (transform.position, hit.position, Time.deltaTime * agent.speed * 50);
                    agent.nextPosition = transform.position;
                }

                if (agent.hasPath && agent.path.corners.Length > 1) {
                    Vector3 lookTarget = agent.path.corners[1] + Vector3.up + (agent.path.corners[1] - agent.path.corners[0]).normalized;
                    lookTarget.y = transform.position.y;

                    //Body face direction
                    Quaternion targetRotation = Quaternion.LookRotation (lookTarget - transform.position, Vector3.up);
                    transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, Time.deltaTime * agent.angularSpeed);
                    transform.localEulerAngles = new Vector3 (0, transform.localEulerAngles.y, 0);
                }

            }
        }

        public void FoundPlayer (PlayerBehaviour player) {
            targetPlayer = player;
            chasingPlayer = true;
        }

        public void LostPlayer () {
            if (currentTarget == targetPlayer.transform) {
                currentTarget = FindNextWayPoint ();
            }
            chasingPlayer = false;
        }

        Transform FindNextWayPoint () {
            float closestDist = 999f;
            Transform closestTransform = null;

            foreach (var waypoint in waypoints) {
                float _dist = (transform.position - waypoint.position).magnitude;
                if (_dist < closestDist) {
                    closestTransform = waypoint;
                    closestDist = _dist;
                }
            }

            return closestTransform;
        }
    }
}