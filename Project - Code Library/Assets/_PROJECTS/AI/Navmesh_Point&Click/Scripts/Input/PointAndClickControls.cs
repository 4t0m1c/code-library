// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/AI/Navmesh_Point&Click/Scripts/Input/PointAndClickControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PointAndClickControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PointAndClickControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PointAndClickControls"",
    ""maps"": [
        {
            ""name"": ""Game"",
            ""id"": ""e8486e87-0cdd-4835-8798-317a2db5b4f5"",
            ""actions"": [
                {
                    ""name"": ""PointClick"",
                    ""type"": ""Button"",
                    ""id"": ""c1f6397e-8289-47b9-8200-061389481beb"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""Button"",
                    ""id"": ""aa49bd14-b324-4eb3-b7e9-dead29808fca"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b4447b13-d21d-4109-bf59-3023e54a2853"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PointClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7dbd7d7b-3e0b-483d-aa14-ccb10e01e5ff"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Game
        m_Game = asset.FindActionMap("Game", throwIfNotFound: true);
        m_Game_PointClick = m_Game.FindAction("PointClick", throwIfNotFound: true);
        m_Game_MousePosition = m_Game.FindAction("MousePosition", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Game
    private readonly InputActionMap m_Game;
    private IGameActions m_GameActionsCallbackInterface;
    private readonly InputAction m_Game_PointClick;
    private readonly InputAction m_Game_MousePosition;
    public struct GameActions
    {
        private @PointAndClickControls m_Wrapper;
        public GameActions(@PointAndClickControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @PointClick => m_Wrapper.m_Game_PointClick;
        public InputAction @MousePosition => m_Wrapper.m_Game_MousePosition;
        public InputActionMap Get() { return m_Wrapper.m_Game; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameActions set) { return set.Get(); }
        public void SetCallbacks(IGameActions instance)
        {
            if (m_Wrapper.m_GameActionsCallbackInterface != null)
            {
                @PointClick.started -= m_Wrapper.m_GameActionsCallbackInterface.OnPointClick;
                @PointClick.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnPointClick;
                @PointClick.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnPointClick;
                @MousePosition.started -= m_Wrapper.m_GameActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnMousePosition;
            }
            m_Wrapper.m_GameActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PointClick.started += instance.OnPointClick;
                @PointClick.performed += instance.OnPointClick;
                @PointClick.canceled += instance.OnPointClick;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
            }
        }
    }
    public GameActions @Game => new GameActions(this);
    public interface IGameActions
    {
        void OnPointClick(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
    }
}
