﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.AI {
    public class Waypoints : MonoBehaviour {
        List<Transform> wayPoints = new List<Transform> ();

        void Awake () {
            foreach (Transform child in transform) {
                wayPoints.Add (child);
            }
        }

        void Start () {
            EnemyBehaviour[] enemies = GameObject.FindObjectsOfType<EnemyBehaviour> ();

            foreach (var enemy in enemies) {
                enemy.SetDestinations (wayPoints);
            }
        }

    }
}