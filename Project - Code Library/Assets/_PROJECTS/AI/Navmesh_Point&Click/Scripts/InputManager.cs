﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace At0m1c.AI {
    public class InputManager : MonoBehaviour {

        public delegate void InputEvents ();
        public static event InputEvents OnPointAndClick;

        public static Vector2 _mousePosition;

        PointAndClickControls pointAndClickControls;

        void Awake () {
            pointAndClickControls = new PointAndClickControls ();
            pointAndClickControls.Enable ();
        }

        void Start () {
            pointAndClickControls.Game.PointClick.performed += PointAndClick;
            pointAndClickControls.Game.MousePosition.performed += MousePosition;
        }

        void PointAndClick (InputAction.CallbackContext ctx) {
            if (OnPointAndClick != null) OnPointAndClick ();
        }

        void MousePosition (InputAction.CallbackContext ctx) {
            _mousePosition = ctx.ReadValue<Vector2> ();
        }

    }
}