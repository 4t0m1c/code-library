﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace At0m1c.AI {
    public class EndTrigger : MonoBehaviour {

        void OnTriggerEnter (Collider other) {
            if (other.attachedRigidbody != null) {
                if (other.attachedRigidbody.CompareTag ("Player")) {
                    StartCoroutine (LoadYourAsyncScene ());
                }
            }
        }

        IEnumerator LoadYourAsyncScene () {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync (SceneManager.GetActiveScene ().name);

            while (!asyncLoad.isDone) {
                yield return null;
            }
        }

    }
}