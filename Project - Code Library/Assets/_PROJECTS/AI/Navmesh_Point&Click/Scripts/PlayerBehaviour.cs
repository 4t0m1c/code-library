﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace At0m1c.AI {
    public class PlayerBehaviour : MonoBehaviour {

        [SerializeField] LayerMask hitMask;
        [SerializeField] int layerBlock;
        NavMeshAgent agent;

        void Awake () {
            agent = GetComponent<NavMeshAgent> ();

            agent.updatePosition = false;
            agent.updateRotation = false;
        }

        void OnEnable () {
            InputManager.OnPointAndClick += PointAndClick;
        }

        void OnDisable () {
            InputManager.OnPointAndClick -= PointAndClick;
        }

        void Update () {
            UpdateNavAgent ();
        }

        void UpdateNavAgent () {
            if (agent.hasPath && !agent.isStopped) {
                NavMeshHit hit;
                float maxAgentTravelDistance = Time.deltaTime * agent.speed;

                if (agent.SamplePathPosition (NavMesh.AllAreas, maxAgentTravelDistance, out hit) ||
                    agent.remainingDistance <= agent.stoppingDistance) { //Agent stopped
                } else { //Else, move the actor and manually update the agent pos and rotation
                    agent.velocity = hit.position - transform.position;
                    transform.position = Vector3.Lerp (transform.position, hit.position, Time.deltaTime * agent.speed * 50);
                    agent.nextPosition = transform.position;
                }

                if (agent.hasPath && agent.path.corners.Length > 1) {
                    Vector3 lookTarget = agent.path.corners[1] + Vector3.up + (agent.path.corners[1] - agent.path.corners[0]).normalized;
                    lookTarget.y = transform.position.y;

                    //Body face direction
                    Quaternion targetRotation = Quaternion.LookRotation (lookTarget - transform.position, Vector3.up);
                    transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, Time.deltaTime * agent.angularSpeed);
                    transform.localEulerAngles = new Vector3 (0, transform.localEulerAngles.y, 0);
                }

            }
        }

        void PointAndClick () {
            Ray ray = Camera.main.ScreenPointToRay (InputManager._mousePosition);
            if (Physics.Raycast (ray, out RaycastHit hit, 500, hitMask, QueryTriggerInteraction.Ignore)) {
                if (hit.collider.gameObject.layer != layerBlock) {
                    agent.SetDestination (hit.point);
                }
            }
        }

    }
}