﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.AI {
    public class VisionTrigger : MonoBehaviour {

        [SerializeField] EnemyBehaviour enemyBehaviour;

        void OnTriggerEnter (Collider other) {
            if (other.attachedRigidbody != null) {
                if (other.attachedRigidbody.CompareTag ("Player")) {
                    enemyBehaviour.FoundPlayer (other.attachedRigidbody.GetComponent<PlayerBehaviour> ());
                }
            }
        }
        void OnTriggerExit (Collider other) {
            if (other.attachedRigidbody != null) {
                if (other.attachedRigidbody.CompareTag ("Player")) {
                    enemyBehaviour.LostPlayer ();
                }
            }
        }

    }
}