﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace At0m1c.Utilities {
    public class Screenshot : MonoBehaviour {

        string documentsPath;

        Texture2D tex;
        byte[] pngBytes;

        void Awake () {
            documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.MyDocuments);
            documentsPath += "/" + Application.productName + "/";

            if (!Directory.Exists (documentsPath)) {
                Directory.CreateDirectory (documentsPath);
            }
        }

        void Update () {
            if (Input.GetKeyDown (KeyCode.F7)) TakeScreenshot ();
        }

        void TakeScreenshot () {
            StartCoroutine (TakingScreenShot ());
        }

        IEnumerator TakingScreenShot () {
            yield return new WaitForEndOfFrame ();

            DoTheThing ();

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            Time.timeScale = 0;
        }

        void DoTheThing () {
            // // Create a texture the size of the screen, RGB24 format
            int width = Screen.width;
            int height = Screen.height;
            var tex = new Texture2D (width, height, TextureFormat.RGB24, false);

            // // Read screen contents into the texture
            tex.ReadPixels (new Rect (0, 0, width, height), 0, 0);
            tex.Apply ();

            pngBytes = tex.EncodeToJPG ();
            SaveScreenshot ();
        }

        void SaveScreenshot () {
            string dateTime = System.DateTime.Now.ToString ("yyyy''MM''dd'_'HH''mm''ss");
            string appName = Application.productName;
            appName = appName.Replace (" ", string.Empty);
            string fileName = $"{appName}_{Application.version}_{dateTime}.jpg";
            File.WriteAllBytes (documentsPath + fileName, pngBytes);

            OpenFolder (documentsPath);

            StartCoroutine (CloseOnFocus ());
            // Close ();
        }

        void OpenFolder (string path) {
            if (Directory.Exists (path)) {
                System.Diagnostics.Process.Start (path);
            }
        }

        public void Close () {
            Destroy (tex);

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            Time.timeScale = 1;
        }

        IEnumerator CloseOnFocus () {
            while (Application.isFocused) {
                // Debug.Log("in focus");
                yield return null;
            }
            while (!Application.isFocused) {
                // Debug.Log("out of focus");
                yield return null;
            }
            Close ();
        }

    }
}