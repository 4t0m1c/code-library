﻿using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.Utilities {
    [RequireComponent (typeof (Text))]
    public class UIVersionText : MonoBehaviour {

        void Start () {
            GetComponent<Text> ().text = "v. " + Application.version;
        }

    }
}