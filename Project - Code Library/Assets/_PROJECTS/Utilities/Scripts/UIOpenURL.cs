﻿using UnityEngine;

namespace At0m1c.Utilities {
    public class UIOpenURL : MonoBehaviour {
        [SerializeField] string url;
        [SerializeField] Texture2D hoverCursor;

        public void OpenURL () {
            Application.OpenURL (url);
        }

        public void MouseEnter () {
            Cursor.SetCursor (hoverCursor, new Vector2 (5, 5), CursorMode.Auto);
        }

        public void MouseExit () {
            Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
        }
    }
}