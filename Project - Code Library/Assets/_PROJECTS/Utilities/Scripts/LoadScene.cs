﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace At0m1c.Utilities {

    public class LoadScene : MonoBehaviour {

        [SerializeField] int sceneIndex;

        public void LoadTheScene () {
            SceneManager.LoadScene (sceneIndex);
        }
    }
}