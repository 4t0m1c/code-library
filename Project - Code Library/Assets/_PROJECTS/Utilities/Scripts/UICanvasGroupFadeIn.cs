﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Utilities {
    public class UICanvasGroupFadeIn : MonoBehaviour {

        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] float fadeTime = 1;
        bool dead = false;

        void Awake () {
            canvasGroup.alpha = 1;
        }

        void Update () {
            if (dead) return;

            fadeTime -= Time.deltaTime;
            canvasGroup.alpha = fadeTime;

            if (fadeTime <= 0) {
                Destroy (gameObject);
                dead = true;
            }
        }
    }
}