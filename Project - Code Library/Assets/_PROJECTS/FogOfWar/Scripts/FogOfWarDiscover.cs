﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FogOfWarDiscover : MonoBehaviour {
    [SerializeField] public UnityEvent discoveryHide;
    [SerializeField] public UnityEvent discoveryFind;

    public delegate void DiscoverEvent ();
    public event DiscoverEvent OnDiscovered;

    [SerializeField] bool discovered = false;
    [SerializeField] bool visibleInFog = false;
    bool active = false;

    int eyesOnMe = 0;

    void Awake () {
        if (!discovered) discoveryHide.Invoke ();
    }

    void Update () {
        // if (discovered && !visibleInFog) {
        //     Hide ();
        // }
        // if (visibleInFog && !active) {
        //     active = CEFowFacade.IsWorldPosInView (transform.position);
        //     if (active) Discover ();
        //     else Hide ();
        // }
    }

    public void Hide () {
        eyesOnMe--;
        if (eyesOnMe > 0) return;

        if (!visibleInFog) {
            if (eyesOnMe == 0) {
                discoveryHide.Invoke ();
                discovered = false;
            }
        }

    }

    public void Discover () {
        eyesOnMe++;

        if (discovered || eyesOnMe > 1) return;

        //Become visible
        if (discoveryFind != null) {
            discoveryFind.Invoke ();
        }

        discovered = true;
        if (OnDiscovered != null) OnDiscovered ();
    }

}