using UnityEngine;

namespace Code.FogOfWar.Script {
    public class CEFowProperty {
        public float FowInvisibleAreaAlpha = 0.8f;

        public int FowMapWidth;
        public int FowMapHeight;
        public float WorldMapWidth;
        public float WorldMapHeight;
        public Transform WorldTransform;

        public RenderTexture persistentRT;
        public RenderTexture activeRT;
        public Texture2D discoverStampTexture;
        public LayerMask discoverMask;

    }
}