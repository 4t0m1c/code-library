using System.Collections.Generic;
using Code.FogOfWar.Script;
using UnityEngine;

namespace Code.FogOfWar.Core {

    public struct PaintSpot {
        public Vector2Int paintPoint;
        public float paintSize;

        public PaintSpot (Vector2Int paintPoint, float paintSize) {
            this.paintPoint = paintPoint;
            this.paintSize = paintSize;
        }
    }

    public class CEFowMapPainter {
        public static CEFowMapPainter instance;

        List<PaintSpot> activePoints = new List<PaintSpot> ();
        List<PaintSpot> changedPoints = new List<PaintSpot> ();

        private bool mIsNeedRepaint;
        private bool mIsStaticViewDataChange;

        private CEFowProperty mProperty;
        private Texture2D blackMap;

        public void Initialize (CEFowProperty _property) {
            mProperty = _property;
            instance = this;

            CreateClearTexture (CEFowFacade.instance.fowPersistentTex);
            CreateClearTexture (CEFowFacade.instance.fowActiveTex);

        }

        public void MarkActiveExploreDataChange (PaintSpot _activePoint) {
            activePoints.Add (_activePoint);
        }

        public void MarkExploreDataChange (List<PaintSpot> _changedPoints) {
            if (_changedPoints.Count == 0) return;

            changedPoints.AddRange (_changedPoints);

            mIsNeedRepaint = true;
        }

        public void MarkStaticViewDataChange () {
            mIsStaticViewDataChange = true;
            mIsNeedRepaint = true;
        }

        public void LateUpdate () {
            if (FogOfWarManager.instance.fogType == FogOfWarManager.FogType.Active || FogOfWarManager.instance.fogType == FogOfWarManager.FogType.Both) {
                CreateClearTexture (CEFowFacade.instance.fowActiveTex);
                StampTexture (CEFowFacade.instance.fowActiveTex, activePoints);
            }
            activePoints.Clear ();

            if (!mIsNeedRepaint) return;
            mIsNeedRepaint = false;

            if (FogOfWarManager.instance.fogType == FogOfWarManager.FogType.Persistent || FogOfWarManager.instance.fogType == FogOfWarManager.FogType.Both) {
                StampTexture (CEFowFacade.instance.fowPersistentTex, changedPoints);
            }

            changedPoints.Clear ();

            // if (OnFogOfWarUpdated != null) OnFogOfWarUpdated ();
        }

        void StampTexture (RenderTexture targetTexture, List<PaintSpot> points) {
            RenderTexture.active = targetTexture; // activate rendertexture for drawtexture;
            GL.PushMatrix (); // save matrixes
            GL.LoadPixelMatrix (0, targetTexture.width, targetTexture.height, 0); // setup matrix for correct size

            // Debug.Log ($"changedPoints: {changedPoints.Count} on {targetTexture.name}");
            foreach (var pointChanged in points) {

                float relativeSize = ((pointChanged.paintSize / (float) mProperty.FowMapWidth) * (float) targetTexture.width) * 2;
                float relativeX = (((float) pointChanged.paintPoint.x / (float) mProperty.FowMapWidth) * (float) targetTexture.width) - (relativeSize / 2);
                float relativeY = (float) targetTexture.height - (((float) pointChanged.paintPoint.y / (float) mProperty.FowMapHeight) * (float) targetTexture.height) - (relativeSize / 2);

                // Debug.Log ($"RelativeX: {relativeX} | RelativeY: {relativeY}");

                Graphics.DrawTexture (new Rect (relativeX, relativeY, relativeSize, relativeSize), mProperty.discoverStampTexture);
            }

            // draw brushtexture
            GL.PopMatrix ();
            RenderTexture.active = null; // turn off rendertexture
        }

        void CreateClearTexture (RenderTexture targetTexture) {
            blackMap = new Texture2D (1, 1);
            blackMap.SetPixel (0, 0, Color.black);
            blackMap.Apply ();
            Graphics.Blit (blackMap, targetTexture);
        }

    }
}