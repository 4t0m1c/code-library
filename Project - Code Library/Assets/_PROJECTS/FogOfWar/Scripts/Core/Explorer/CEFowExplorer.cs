using System.Collections.Generic;
using UnityEngine;

namespace Code.FogOfWar.Core {
    public class CEFowExplorer {
        public List<PaintSpot> changedPoints = new List<PaintSpot> ();

        public void Initialize (int mViewRange, Vector3 _worldPos) {
            CEFowFacade.instance.dynamicExplorerList.Add (this);

            RunLogic (CEFowFacade.GetFowPos (_worldPos), mViewRange);
        }

        public void Dispose () {
            CEFowFacade.instance.dynamicExplorerList.Remove (this);
        }

        public void Update (Vector3 _nowWorldPos, int range) {
            RunLogic (CEFowFacade.GetFowPos (_nowWorldPos), range);
        }

        public void UpdateActive (Vector3 _nowWorldPos, int range) {
            CEFowFacade.instance.painter.MarkActiveExploreDataChange (new PaintSpot (CEFowFacade.GetFowPos (_nowWorldPos), range));
        }

        void RunLogic (Vector2Int pos, int mViewRange) {
            changedPoints.Clear ();

            if (CEFowFacade.SetMap (CEFowFacade.instance.data.exploreMapData, pos.x, pos.y)) {
                changedPoints.Add (new PaintSpot (new Vector2Int (pos.x, pos.y), mViewRange));
            }

            CEFowFacade.instance.painter.MarkExploreDataChange (changedPoints);
        }
    }
}