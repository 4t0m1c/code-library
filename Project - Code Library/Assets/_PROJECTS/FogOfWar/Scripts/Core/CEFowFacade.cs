﻿using System.Collections.Generic;
using Code.FogOfWar.Core;
using Code.FogOfWar.Script;
using Unity.Collections;
using UnityEngine;

public class CEFowFacade {
    private static CEFowFacade mInstance;

    public static CEFowFacade instance => mInstance ?? (mInstance = new CEFowFacade ());

    public CEFowMapData data;
    public CEFowMapPainter painter;
    public Texture2D rawFowTexture;
    public RenderTexture fowPersistentTex => mProperty.persistentRT;
    public RenderTexture fowActiveTex => mProperty.activeRT;

    public List<CEFowExplorer> dynamicExplorerList = new List<CEFowExplorer> ();
    public List<CEFowStaticExplorer> staticExplorerList = new List<CEFowStaticExplorer> ();

    public int fowMapWidth => mProperty.FowMapWidth;
    public int fowMapHeight => mProperty.FowMapHeight;
    public float worldMapWidth => mProperty.WorldMapWidth;
    public float worldMapHeight => mProperty.WorldMapHeight;
    public Transform worldTransform => mProperty.WorldTransform;

    private CEFowProperty mProperty;

    public void InitAsNew (CEFowProperty _fowProperty) {
        mProperty = _fowProperty;

        var size = fowMapWidth * fowMapHeight;
        data = new CEFowMapData { exploreMapData = new bool[size] };

        rawFowTexture = new Texture2D (fowMapWidth, fowMapHeight, TextureFormat.RGB24, false);
        rawFowTexture.wrapMode = TextureWrapMode.Clamp;

        painter = new CEFowMapPainter ();
        painter.Initialize (mProperty);

        // fowTexture = new RenderTexture (fowMapWidth, fowMapHeight, 0);

        // DoInitShaderProperty ();
    }

    public void LateUpdate () {
        painter.LateUpdate ();
    }

    //====================
    //== Utils
    //====================

    public static bool IsWorldPosInView (Vector3 _worldPos) {
        var fowPos = GetFowPos (_worldPos, true);
        bool valid = GetMap (instance.data.exploreMapData, fowPos.x, fowPos.y);
        // if (valid) Debug.Log (_worldPos + " | x: " + fowPos.x + " | y: " + fowPos.y);
        return valid;
    }

    public static Vector2Int GetFowPos (Vector3 _worldPos, bool skipRelative = false) {
        if (!skipRelative) _worldPos = GetRelativePos (_worldPos);
        var x = Mathf.FloorToInt ((_worldPos.x / instance.worldMapWidth) * instance.fowMapWidth);
        var y = Mathf.FloorToInt ((_worldPos.z / instance.worldMapHeight) * instance.fowMapHeight);
        // Debug.Log (_worldPos + " | x: " + x + " | y: " + y);
        return new Vector2Int (x, y);
    }

    public static Vector3 GetRelativePos (Vector3 position) {
        Vector3 _pos = instance.worldTransform.InverseTransformPoint (position);
        _pos *= instance.worldTransform.localScale.x;
        _pos.x += instance.worldTransform.localScale.x / 2;
        _pos.z += instance.worldTransform.localScale.z / 2;
        // Debug.Log("World pos: " + position + " | Relative pos: " + _pos);
        return _pos;
    }

    public static bool SetMap (bool[] _map, int _x, int _y) {
        // Debug.Log ("SetMap: " + _map.Length + " | x: " + _x + " | y: " + _y);
        if (_map == null || _map.Length == 0) return false;
        var index = _x + (_y * instance.fowMapWidth);
        bool orig = false;
        if (index <= _map.Length && index >= 0) {
            orig = _map[index];
            // Debug.Log ("index: " + index);
            if (index < 0 || index >= _map.Length) return false;
            _map[index] = true;
        } else {
            orig = false;
        }
        return orig == false;
    }

    public static bool GetMap (bool[] _map, int _x, int _y) {
        if (_map == null || _map.Length == 0) return false;
        var index = _x + (_y * instance.fowMapWidth);
        if (index < 0 || index >= _map.Length) return false;
        return _map[index];
    }

}