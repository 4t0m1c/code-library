﻿using System.Collections;
using Code.FogOfWar.Core;
using UnityEditor;
using UnityEngine;

public class FogOfWarExplore : MonoBehaviour {
    public float viewRadius;
    int range;
    public float ViewRadius {
        get { return viewRadius; }
        set {
            viewRadius = value;
            range = Mathf.FloorToInt (viewRadius / CEFowFacade.instance.worldMapWidth * CEFowFacade.instance.fowMapWidth);
            if (discoverTrigger != null)
                discoverTrigger.radius = viewRadius;
        }
    }

    [SerializeField] float repaintMinDist = 0.5f;

    int lastRange;

    Vector3 lastPos = new Vector3 (0, -2f, 0);

    public static CEFowExplorer mExplorer;

    Transform mTrans;
    SphereCollider discoverTrigger;

    bool initialised = false;

    void Awake () {
        discoverTrigger = gameObject.AddComponent<SphereCollider> ();
        discoverTrigger.isTrigger = true;
        discoverTrigger.radius = viewRadius * 2;
    }

    void OnEnable () {
        CheckNull ();
    }

    void OnDisable () {
        mExplorer.Dispose ();
    }

    void CheckNull () {
        if (mExplorer == null) {
            mExplorer = new CEFowExplorer ();
        }

        if (!initialised)
            ViewRadius = viewRadius;
        Initialize ();
    }

    public void Initialize () {
        initialised = true;
        mTrans = transform;

        mExplorer.Initialize (range, transform.position);

        StartCoroutine (UpdatePosition ());
    }

    IEnumerator UpdatePosition () {
        while (true) {
            float sqrDist = (transform.position - lastPos).sqrMagnitude;
            if (sqrDist >= repaintMinDist || lastRange != range) {
                mExplorer?.Update (transform.position, range);
                lastPos = transform.position;
                lastRange = range;
            }

            if (FogOfWarManager.instance.fogType == FogOfWarManager.FogType.Active || FogOfWarManager.instance.fogType == FogOfWarManager.FogType.Both) {
                mExplorer?.UpdateActive (transform.position, range);
            }
            yield return null;
        }
    }

    void OnTriggerEnter (Collider other) {
        if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent<Rigidbody> (out Rigidbody rigidbody)) {
            // Debug.Log ("Has Rigidbody: " + (1 << other.attachedRigidbody.gameObject.layer) + " | " + (FogOfWarSettings.instance.discoverMask.value));
            if (((1 << other.attachedRigidbody.gameObject.layer) == FogOfWarManager.instance.discoverMask.value) && !other.isTrigger) {
                // Debug.Log ("Found Discoverable: " + other.attachedRigidbody.gameObject, other.attachedRigidbody.gameObject);
                FogOfWarDiscover discover = other.attachedRigidbody.GetComponent<FogOfWarDiscover> ();
                discover?.Discover ();
            }
        }
    }

    void OnTriggerExit (Collider other) {
        if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent<Rigidbody> (out Rigidbody rigidbody)) {
            // Debug.Log ("Has Rigidbody: " + (1 << other.attachedRigidbody.gameObject.layer) + " | " + (FogOfWarSettings.instance.discoverMask.value));
            if (((1 << other.attachedRigidbody.gameObject.layer) == FogOfWarManager.instance.discoverMask.value) && !other.isTrigger) {
                // Debug.Log ("Found Discoverable: " + other.attachedRigidbody.gameObject, other.attachedRigidbody.gameObject);
                FogOfWarDiscover discover = other.attachedRigidbody.GetComponent<FogOfWarDiscover> ();
                discover?.Hide ();
            }
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected () {
        Handles.DrawWireArc (transform.position, Vector3.up, Vector3.forward, 360, ViewRadius);
    }
#endif
}