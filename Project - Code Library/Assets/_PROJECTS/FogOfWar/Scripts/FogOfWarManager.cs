﻿using Code.FogOfWar.Script;
using UnityEngine;

public class FogOfWarManager : MonoBehaviour {
    public static FogOfWarManager instance;

    public enum FogType {
        Persistent,
        Active,
        Both
    }

    public FogType fogType;
    public float worldSize = 50f;
    [Range (0.0f, 1f)]
    public float FowInvisibleAreaAlpha = 0.9f;
    public int FowTextureSize = 512;
    public GameObject persistentDecal;
    public RenderTexture persistentRT;
    public GameObject activeDecal;
    public RenderTexture activeRT;
    public Texture2D discoverStampTexture;
    public LayerMask discoverMask;

    CEFowProperty mProperty;

    void Awake () {
        mProperty = new CEFowProperty {
            FowMapWidth = FowTextureSize,
            FowMapHeight = FowTextureSize,
            WorldMapWidth = worldSize,
            WorldMapHeight = worldSize,
            FowInvisibleAreaAlpha = FowInvisibleAreaAlpha,
            persistentRT = this.persistentRT,
            activeRT = this.activeRT,
            WorldTransform = transform,
            discoverStampTexture = this.discoverStampTexture,
        };

        transform.localScale = new Vector3 (worldSize, worldSize, worldSize);

        CEFowFacade.instance.InitAsNew (mProperty);
        instance = this;

        if (fogType == FogType.Active) {
            persistentDecal.SetActive(false);
        }
        if (fogType == FogType.Persistent) {
            activeDecal.SetActive(false);
        }
    }

    void LateUpdate () {
        CEFowFacade.instance.LateUpdate ();
    }

}