﻿using UnityEditor;
using UnityEngine;

namespace At0m1c.FoW.LuxURPEssentials {
	[CustomEditor (typeof (Decal))]
	public class DecalEditor : Editor {
		public override void OnInspectorGUI () {
			Decal script = (Decal) target;
			if (GUILayout.Button ("Align")) {
				script.AlignDecal ();
			}
		}
	}
}