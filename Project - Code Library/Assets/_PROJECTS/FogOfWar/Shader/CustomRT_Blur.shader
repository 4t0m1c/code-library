﻿Shader "CustomRenderTexture/Blur"
{

    Properties
    {
        _Tex ("InputTex", 2D) = "white" {}
        _BlurSize("Blur Size", Range(0,0.1)) = 0
        [KeywordEnum(Low, Medium, High)] _Samples ("Sample amount", Float) = 0
        [Toggle(GAUSS)] _Gauss ("Gaussian Blur", float) = 0
        [PowerSlider(3)]_StandardDeviation("Standard Deviation (Gauss only)", Range(0.00, 0.3)) = 0.02
    }

    CGINCLUDE

    #include "UnityCG.cginc"
    #include "UnityCustomRenderTexture.cginc"

    sampler2D _Tex;
    float _BlurSize;
    float _StandardDeviation;

    #define PI 3.14159265359
    #define E 2.71828182846

    #pragma multi_compile _SAMPLES_LOW _SAMPLES_MEDIUM _SAMPLES_HIGH
    #pragma shader_feature GAUSS

    #if _SAMPLES_LOW
        #define SAMPLES 10
    #elif _SAMPLES_MEDIUM
        #define SAMPLES 30
    #else
        #define SAMPLES 100
    #endif

    fixed4 fragVertical(v2f_customrendertexture i) : SV_Target
    {
        #if GAUSS
            //failsafe so we can use turn off the blur by setting the deviation to 0
            if(_StandardDeviation == 0)
            return tex2D(_Tex, i.localTexcoord);
        #endif
        //init color variable
        float4 col = 0;
        #if GAUSS
            float sum = 0;
        #else
            float sum = SAMPLES;
        #endif
        
        //calculate aspect ratio
        float invAspect = _ScreenParams.y / _ScreenParams.x;

        //iterate over blur samples
        for(float index = 0; index < SAMPLES; index++){
            //get the offset of the sample
            float offset = (index/(SAMPLES-1) - 0.5) * _BlurSize;
            //get localTexcoord coordinate of sample
            float2 localTexcoord = i.localTexcoord + float2(0, offset);
            #if !GAUSS
                //simply add the color if we don't have a gaussian blur (box)
                col += tex2D(_Tex, localTexcoord);
            #else
                //calculate the result of the gaussian function
                float stDevSquared = _StandardDeviation*_StandardDeviation;
                float gauss = (1 / sqrt(2*PI*stDevSquared)) * pow(E, -((offset*offset)/(2*stDevSquared)));
                //add result to sum
                sum += gauss;
                //multiply color with influence from gaussian function and add it to sum color
                col += tex2D(_Tex, localTexcoord) * gauss;
            #endif
        }

        //divide the sum of values by the amount of samples
        col = col / sum;
        return col;
    }

    fixed4 fragHorizontal(v2f_customrendertexture i) : SV_Target
    {
        #if GAUSS
            //failsafe so we can use turn off the blur by setting the deviation to 0
            if(_StandardDeviation == 0)
            return tex2D(_Tex, i.localTexcoord);
        #endif
        //init color variable
        float4 col = 0;
        #if GAUSS
            float sum = 0;
        #else
            float sum = SAMPLES;
        #endif
        
        //calculate aspect ratio
        float invAspect = _ScreenParams.y / _ScreenParams.x;

        //iterate over blur samples
        for(float index = 0; index < SAMPLES; index++){
            //get the offset of the sample
            float offset = (index/(SAMPLES-1) - 0.5) * _BlurSize * invAspect;
            //get localTexcoord coordinate of sample
            float2 localTexcoord = i.localTexcoord + float2(offset, 0);
            #if !GAUSS
                //simply add the color if we don't have a gaussian blur (box)
                col += tex2D(_Tex, localTexcoord);
            #else
                //calculate the result of the gaussian function
                float stDevSquared = _StandardDeviation*_StandardDeviation;
                float gauss = (1 / sqrt(2*PI*stDevSquared)) * pow(E, -((offset*offset)/(2*stDevSquared)));
                //add result to sum
                sum += gauss;
                //multiply color with influence from gaussian function and add it to sum color
                col += tex2D(_Tex, localTexcoord) * gauss;
            #endif
        }

        //divide the sum of values by the amount of samples
        col = col / sum;
        return col;
    }


    ENDCG
    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            Name "HorizontalBlur"
            CGPROGRAM
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment fragHorizontal
            ENDCG
        }

        Pass
        {
            Name "VerticalBlur"
            CGPROGRAM
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment fragVertical
            ENDCG
        }

        Pass
        {
            Name "Blur"
            CGPROGRAM
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment fragCombine
            
            fixed4 fragCombine(v2f_customrendertexture i) : SV_Target
            {
                return fragHorizontal(i)*fragVertical(i);
            }

            ENDCG
        }
    }
}