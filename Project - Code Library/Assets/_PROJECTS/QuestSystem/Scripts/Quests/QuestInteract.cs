using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.QuestSystem {
    public class QuestInteract : MonoBehaviour {

        [SerializeField] UnityEvent OnEnterRange = new UnityEvent ();
        [SerializeField] UnityEvent OnExitRange = new UnityEvent ();

        [Header ("Debug")]
        [SerializeField] List<QuestObject> activeQuests = new List<QuestObject> ();
        [SerializeField] List<QuestObject> completedQuests = new List<QuestObject> ();

        List<QuestTask> tasksInRange = new List<QuestTask> ();
        List<QuestGiver> questGiversInRange = new List<QuestGiver> ();

        void OnEnable () {
            BasicCharacterController.OnInteract.AddListener (Interact);
        }

        void OnDisable () {
            BasicCharacterController.OnInteract.RemoveListener (Interact);
        }

        void OnTriggerEnter (Collider other) {
            //Giver
            if (other.attachedRigidbody != null && other.TryGetComponent<QuestGiver> (out QuestGiver questGiver)) {
                if (!questGiversInRange.Contains (questGiver) && questGiver.CanInteract ()) {
                    questGiversInRange.Add (questGiver);
                    questGiver.SetInRange (true);
                }
            }

            //Task
            if (other.attachedRigidbody != null && other.TryGetComponent<QuestTask> (out QuestTask questTask)) {
                if (!tasksInRange.Contains (questTask)) {
                    tasksInRange.Add (questTask);
                    questTask.SetInRange (true);
                }
            }
            CheckRange ();
        }

        void OnTriggerExit (Collider other) {
            //Giver
            if (other.attachedRigidbody != null && other.TryGetComponent<QuestGiver> (out QuestGiver questGiver)) {
                if (questGiversInRange.Contains (questGiver)) {
                    questGiversInRange.Remove (questGiver);
                    questGiver.SetInRange (false);
                }
            }

            //Task
            if (other.attachedRigidbody != null && other.TryGetComponent<QuestTask> (out QuestTask questTask)) {
                if (tasksInRange.Contains (questTask)) {
                    tasksInRange.Remove (questTask);
                    questTask.SetInRange (false);
                }
            }
            CheckRange ();
        }

        void CheckRange () {
            if (tasksInRange.Count == 1 || questGiversInRange.Count == 1) {
                OnEnterRange.Invoke ();
            } else if (tasksInRange.Count == 0 && questGiversInRange.Count == 0) {
                OnExitRange.Invoke ();
            }
        }

        void Interact () {
            Debug.Log ($"Player interacting");
            if (tasksInRange.Count > 0) {
                QuestTask questTask = GetNearestTask ();
                if (questTask.QueryQuest (activeQuests, out QuestObject questObject, out Task task)) {
                    //Success
                    if (!questObject.quest.tasksComplete) {
                        Debug.Log ($"Task completed.");
                        UIQuestStatus.instance.QuestTaskComplete (questObject, task);
                    } else {
                        Debug.Log ($"All tasks complete.");
                        UIQuestStatus.instance.QuestAllTasksComplete (questObject, task);
                    }

                    tasksInRange.Remove (questTask);
                    CheckRange ();
                } else {
                    //Failed
                    Debug.Log ($"You do not have this quest.");
                }
            } else if (questGiversInRange.Count > 0) {
                QuestGiver giver = GetNearestQuestGiver ();
                QuestObject questObject = giver.GetQuest ();
                if (!activeQuests.Contains (questObject) && !completedQuests.Contains (questObject)) {
                    Debug.Log ($"Getting quest [{questObject.quest.questName}] from {giver.gameObject}");
                    activeQuests.Add (questObject);
                    UIQuestStatus.instance.QuestAdded (questObject);

                    questGiversInRange.Remove (giver);
                    giver.SetInRange (false);
                } else if (activeQuests.Contains (questObject) && questObject.quest.tasksComplete) {
                    giver.ReturnQuest ();
                    Debug.Log ($"Quest completed.");
                    UIQuestStatus.instance.QuestComplete (questObject);
                    activeQuests.Remove (questObject);
                    completedQuests.Add (questObject);
                }

            } else {
                Debug.Log ($"Nothing in range");
            }
        }

        QuestTask GetNearestTask () {
            //Clear null refs
            if (tasksInRange.Count > 0) {
                for (int i = tasksInRange.Count - 1; i >= 0; i--) {
                    if (tasksInRange[i] == null) tasksInRange.RemoveAt (i);
                }
            }

            float nearestDistance = 99999;
            QuestTask nearestTask = null;
            foreach (var task in tasksInRange) {
                float dist = (task.transform.position - transform.position).sqrMagnitude;
                if (dist < nearestDistance) {
                    nearestDistance = dist;
                    nearestTask = task;
                }
            }

            return nearestTask;
        }

        QuestGiver GetNearestQuestGiver () {
            //Clear null refs
            if (questGiversInRange.Count > 0) {
                for (int i = questGiversInRange.Count - 1; i >= 0; i--) {
                    if (questGiversInRange[i] == null) questGiversInRange.RemoveAt (i);
                }
            }

            float nearestDistance = 99999;
            QuestGiver nearestQuestGiver = null;
            foreach (var questGiver in questGiversInRange) {
                float dist = (questGiver.transform.position - transform.position).sqrMagnitude;
                if (dist < nearestDistance) {
                    nearestDistance = dist;
                    nearestQuestGiver = questGiver;
                }
            }

            return nearestQuestGiver;
        }

    }
}