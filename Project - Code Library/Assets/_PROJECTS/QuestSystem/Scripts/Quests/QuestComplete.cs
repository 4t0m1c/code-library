using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.QuestSystem {
    public class QuestComplete : MonoBehaviour {

        [SerializeField] UnityEvent OnQuestCompleted = new UnityEvent ();
        [SerializeField] QuestObject questObject;

        bool activated = false;

        void OnEnable () {
            QuestObject.OnQuestCompleted.AddListener (QuestCompleted);
        }

        void OnDisable () {
            QuestObject.OnQuestCompleted.RemoveListener (QuestCompleted);
        }

        void QuestCompleted (QuestObject quest) {
            if (quest == questObject && !activated) {
                activated = true;
                OnQuestCompleted.Invoke ();
            }
        }

    }
}