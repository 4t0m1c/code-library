using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.QuestSystem {
    public class QuestGiver : MonoBehaviour {

        [SerializeField] UnityEvent OnEnterRange = new UnityEvent ();
        [SerializeField] UnityEvent OnExitRange = new UnityEvent ();

        [SerializeField] QuestObject questObject;
        bool questGiven = false;
        bool questReturned = false;

        void Awake() {
            if (questObject != null){
                questObject.Reset();
            }    
        }

        public bool CanInteract () {
            if (questGiven && !questReturned && questObject.quest.tasksComplete) return true;
            if (!questGiven) return true;
            return false;
        }

        public QuestObject GetQuest () {
            questGiven = true;
            return questObject;
        }

        public bool ReturnQuest () {
            if (questGiven && questObject.quest.tasksComplete) {
                questReturned = true;
                questObject.CompleteQuest ();
                return true;
            } else {
                return false;
            }
        }

        public void SetInRange (bool inRange) {
            if (inRange) {
                OnEnterRange.Invoke ();
            } else {
                OnExitRange.Invoke ();
            }
        }

    }
}