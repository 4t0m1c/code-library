using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.QuestSystem {
    public class QuestTask : MonoBehaviour {
        [SerializeField] QuestObject quest;
        [SerializeField] UnityEvent OnEnterRange = new UnityEvent ();
        [SerializeField] UnityEvent OnExitRange = new UnityEvent ();

        public bool QueryQuest (List<QuestObject> quests, out QuestObject questObject, out Task task) {
            questObject = null;
            task = null;

            if (quests.Contains (quest)) {
                questObject = quest;
                task = CompleteTask ();
                return true;
            }
            return false;
        }

        Task CompleteTask () {
            gameObject.SetActive (false);
            return quest.CompleteTask ();
        }

        public void SetInRange (bool inRange) {
            if (inRange) {
                OnEnterRange.Invoke ();
            } else {
                OnExitRange.Invoke ();
            }
        }
    }
}