using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.QuestSystem {
    [CreateAssetMenu (fileName = "Quest", menuName = "Quests/New Quest", order = 1)]
    public class QuestObject : ScriptableObject {
        [HideInInspector] public static UnityEvent<QuestObject, int> OnQuestTaskCompleted = new UnityEvent<QuestObject, int> ();
        [HideInInspector] public static UnityEvent<QuestObject> OnQuestCompleted = new UnityEvent<QuestObject> ();
        [HideInInspector] public static UnityEvent<QuestObject> OnQuestAdded = new UnityEvent<QuestObject> ();

        public Quest quest;

        public void Reset () {
            quest.complete = false;
            quest.tasksComplete = false;
            foreach (var task in quest.questTasks) {
                task.complete = false;
            }
        }

        public void QuestCollected () {
            OnQuestAdded.Invoke (this);
        }

        public Task CompleteTask () {
            for (var i = 0; i < quest.questTasks.Length; i++) {
                if (quest.questTasks[i].complete) {
                    continue;
                } else {
                    OnQuestTaskCompleted.Invoke (this, i);

                    if (i == quest.questTasks.Length - 1) {
                        quest.tasksComplete = true;
                    }

                    quest.questTasks[i].complete = true;
                    return quest.questTasks[i];
                }
            }
            return null;
        }

        public void CompleteQuest () {
            if (quest.tasksComplete) {
                quest.complete = true;
                OnQuestCompleted.Invoke (this);
            }
        }
    }

    [System.Serializable]
    public class Quest {
        public string questName;
        public string questDescription;
        public bool tasksComplete;
        public bool complete;
        public Task[] questTasks;

    }

    //Tasks
    [System.Serializable]
    public class Task {
        public string questTaskName;
        public string questTaskDescription;

        public bool complete;
    }
}