using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace At0m1c.QuestSystem {
    public class UIQuestStatus : MonoBehaviour {
        public static UIQuestStatus instance;

        [SerializeField] Text messageText;
        [SerializeField] Text questTitleText;
        [SerializeField] Text questDescriptionText;
        [SerializeField] Text questTaskText;

        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] float fadeSpeed = 1;
        [SerializeField] float displayWaitTime = 3;

        [System.Serializable]
        struct QuestStatusUpdate {
            public string updateMessage;
            public QuestObject questObject;
            public Task questTask;

            public QuestStatusUpdate (string updateMessage, QuestObject questObject, Task questTask) {
                this.updateMessage = updateMessage;
                this.questObject = questObject;
                this.questTask = questTask;
            }
        }

        Queue<QuestStatusUpdate> displayQuestStatus = new Queue<QuestStatusUpdate> ();

        bool displaying = false;

        void Awake () {
            instance = this;
        }

        public void QuestAdded (QuestObject questObject) {
            displayQuestStatus.Enqueue (new QuestStatusUpdate ("NEW QUEST", questObject, questObject.quest.questTasks[0]));
        }

        public void QuestComplete (QuestObject questObject) {
            displayQuestStatus.Enqueue (new QuestStatusUpdate ("QUEST COMPLETE", questObject, null));
        }

        public void QuestTaskComplete (QuestObject questObject, Task questTask) {
            displayQuestStatus.Enqueue (new QuestStatusUpdate ("TASK COMPLETE", questObject, questTask));
        }

        public void QuestAllTasksComplete (QuestObject questObject, Task questTask) {
            displayQuestStatus.Enqueue (new QuestStatusUpdate ("TURN IN THE QUEST", questObject, questTask));
        }

        void Update () {
            if (!displaying) {
                if (displayQuestStatus.Count > 0) {
                    StartCoroutine (ShowQuestStatus (displayQuestStatus.Dequeue ()));
                }
            }
        }

        IEnumerator ShowQuestStatus (QuestStatusUpdate questStatusUpdate) {
            displaying = true;

            messageText.text = questStatusUpdate.updateMessage;

            questTitleText.text = questStatusUpdate.questObject.quest.questName;
            if (questStatusUpdate.questTask != null) {
                questTaskText.gameObject.SetActive (true);
                questTitleText.gameObject.SetActive (false);
                questTaskText.text = questStatusUpdate.questTask.questTaskName;
                questDescriptionText.text = questStatusUpdate.questTask.questTaskDescription;
            } else {
                questTaskText.gameObject.SetActive (false);
                questTitleText.gameObject.SetActive (true);
                questDescriptionText.text = questStatusUpdate.questObject.quest.questDescription;
            }

            canvasGroup.alpha = 0;
            while (canvasGroup.alpha < 1) {
                canvasGroup.alpha += Time.deltaTime * fadeSpeed;
                yield return null;
            }
            float wait = displayWaitTime;
            while (wait > 0) {
                wait -= Time.deltaTime;
                yield return null;
            }
            while (canvasGroup.alpha > 0) {
                canvasGroup.alpha -= Time.deltaTime * fadeSpeed;
                yield return null;
            }
            canvasGroup.alpha = 0;

            displaying = false;
        }
    }
}