﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFollowTransform : MonoBehaviour {

	public Transform followTransform;
	[HideInInspector] public Vector3 followPosition;
	[SerializeField] RectTransform rectToMove;
	[SerializeField] RectTransform sizeConstraintRect;
	[SerializeField] Camera cam;

	Canvas canvas;

	public void Setup (Transform followTransform, RectTransform sizeConstraintRect, Camera cam) {
		this.followTransform = followTransform;
		this.sizeConstraintRect = sizeConstraintRect;
		this.cam = cam;
	}

	void Awake () {
		if (cam == null) {
			cam = Camera.main;
		}

		if (rectToMove == null) {
			rectToMove = GetComponent<RectTransform> ();
		}

		canvas = gameObject.GetComponentInParent<Canvas> ();
	}

	void LateUpdate () {
			Vector2 viewportPosition = cam.WorldToViewportPoint (followTransform == null ? followPosition : followTransform.position);
			Vector2 relativeSize = Vector2.one;
			if (sizeConstraintRect != null) {
				relativeSize = new Vector2 (sizeConstraintRect.sizeDelta.x, sizeConstraintRect.sizeDelta.y);
				rectToMove.anchoredPosition = new Vector2 ((viewportPosition.x * relativeSize.x) - (relativeSize.x * 0.5f), (viewportPosition.y * relativeSize.y) - (relativeSize.y * 0.5f));
			} else {
				relativeSize = new Vector2 (Screen.width, cam.rect.height * Screen.height);
				rectToMove.anchoredPosition = new Vector2 (((viewportPosition.x * Screen.width) - (Screen.width * 0.5f)), ((viewportPosition.y * (cam.rect.height * Screen.height)) - ((cam.rect.height * Screen.height) * 0.5f)));
			}
	}
}