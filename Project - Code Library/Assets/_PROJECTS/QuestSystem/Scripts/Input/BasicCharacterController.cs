﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace At0m1c.QuestSystem {
    public class BasicCharacterController : MonoBehaviour {

        BasicControls controls;

        public static UnityEvent OnInteract = new UnityEvent ();

        [SerializeField] Vector2 moveVector;
        [SerializeField] float speed;
        [SerializeField] bool faceDirectionOfTravel = false;

        void Awake () {
            controls = new BasicControls ();
            controls.Enable ();
        }

        void Start () {
            controls.Player.Move.performed += x => moveVector = x.ReadValue<Vector2> ();
            controls.Player.Interact.performed += x => {
                Debug.Log($"Interacting");
                OnInteract.Invoke ();
            };
        }

        void Update () {
            DoMove ();
        }

        void DoMove () {
            transform.Translate (new Vector3 (moveVector.x, 0, moveVector.y) * Time.deltaTime * speed, Space.World);
            if (faceDirectionOfTravel) {
                transform.forward = Vector3.Lerp (transform.forward, new Vector3 (moveVector.x, 0, moveVector.y), Time.deltaTime * 20);
            }
        }

    }
}