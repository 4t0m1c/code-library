using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

    Transform cameraTransform;

    [SerializeField] bool lockX, lockY, lockZ;
    Vector3 originalEulerAngles;

    void Awake() {
        cameraTransform = Camera.main.transform;    
        originalEulerAngles = transform.localEulerAngles;
    }

    void LateUpdate () {
        transform.LookAt (cameraTransform, Vector3.up);
        transform.localEulerAngles = new Vector3(
            lockX ? originalEulerAngles.x : transform.localEulerAngles.x,
            lockY ? originalEulerAngles.y : transform.localEulerAngles.y,
            lockZ ? originalEulerAngles.z : transform.localEulerAngles.z
        );
    }
}