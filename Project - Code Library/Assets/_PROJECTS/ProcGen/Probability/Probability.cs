﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProbabilityValue {
    public EnemyDifficulty enemyDifficulty;
    public float probability;
    public float probabilityIncrement = 1f;

    [Header ("Debug")]
    public float probabilityLikelinessOffset; //Likeliness
    public int rollCount;
}

public enum EnemyDifficulty {
    Easy,
    Medium,
    Hard
}

public class Probability : MonoBehaviour {

    [SerializeField] List<ProbabilityValue> probabilityValues = new List<ProbabilityValue> ();

    [Header ("Debug")]
    [SerializeField] float totalProbability = 100;

    [ContextMenu ("Roll 500 Test")]
    public void RollTest500 () {
        probabilityValues.ForEach (x => x.rollCount = 0); //Reset roll counts
        probabilityValues.ForEach (x => x.probabilityLikelinessOffset = 0); //Reset probabilityLikelinessOffset

        for (int i = 0; i < 500; i++) {
            RollNumber ();
        }

        for (int j = 0; j < probabilityValues.Count; j++) {
            Debug.Log ($"{probabilityValues[j].enemyDifficulty} rolled {(((float)probabilityValues[j].rollCount/500f)*100f)}%");
        }
    }

    [ContextMenu ("Roll Random")]
    public void RollNumber () {

        totalProbability = 0;
        probabilityValues.ForEach (x => totalProbability += x.probability + x.probabilityLikelinessOffset); //Add all probabilities together

        float randomNum = Random.Range (0, totalProbability);
        bool foundCandidate = false;
        ProbabilityValue selectedProbabilityValue = null;
        float runningTotal = 0;

        for (int i = 0; i < probabilityValues.Count; i++) {
            bool offsetValue = true;
            if (!foundCandidate) {
                //Searching for candidate
                float probabilityNumber = (probabilityValues[i].probability + probabilityValues[i].probabilityLikelinessOffset);
                runningTotal += probabilityNumber;

                if (randomNum <= runningTotal) {
                    selectedProbabilityValue = probabilityValues[i];
                    foundCandidate = true;
                    offsetValue = false;

                    probabilityValues[i].probabilityLikelinessOffset = 0; //Reset offset value
                    probabilityValues[i].rollCount++; //FOR DEBUG PURPOSES

                    Debug.Log ($"Rolled {randomNum} / {runningTotal} and found {selectedProbabilityValue.enemyDifficulty} enemy difficulty");
                }
            }

            if (offsetValue) {
                //Adjusting offset for others
                probabilityValues[i].probabilityLikelinessOffset += probabilityValues[i].probabilityIncrement;
            }
        }

    }

}