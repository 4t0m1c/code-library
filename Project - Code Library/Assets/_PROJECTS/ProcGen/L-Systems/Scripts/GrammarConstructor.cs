﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public struct GrammarAction {
    public List<char> characters;
    public List<GrammarActions> actions;

    public GrammarAction (List<char> characters, List<GrammarActions> actions) {
        this.characters = characters;
        this.actions = actions;
    }
}

[System.Serializable]
public struct GrammarRuleset {
    public string name;
    public string axiom;
    public int rotationAngle;
    public List<GrammarList> grammars;
    public List<GrammarAction> grammarActions;

    public GrammarRuleset (string name, string axiom, int rotationAngle, List<GrammarList> grammars, List<GrammarAction> grammarActions) {
        this.name = name;
        this.axiom = axiom;
        this.rotationAngle = rotationAngle;
        this.grammars = grammars;
        this.grammarActions = grammarActions;
    }
}

[System.Serializable]
public enum GrammarActions {
    Translate,
    RotateCW,
    RotateCCW,
    Save,
    Restore,
    None
}

[System.Serializable]
public class Grammar {
    public char variable;
    public string rule;
    public float probability;
    [HideInInspector] public float offsetProbability;

    public Grammar (char variable, string rule, float probability) {
        this.variable = variable;
        this.rule = rule;
        this.probability = probability;
    }
}

[System.Serializable]
public class GrammarList {
    public List<Grammar> grammarList = new List<Grammar> ();

    public GrammarList (List<Grammar> grammarList) {
        this.grammarList = grammarList;
    }
}

public class GrammarConstructor : MonoBehaviour {

    public GrammarRuleset grammarRuleset;

    [ContextMenu ("Create Ruleset Asset")]
    public void GenerateNewGrammar () {
        CreateGrammar (grammarRuleset);
    }

    public GrammarObject CreateGrammar (GrammarRuleset grammarRuleset) {
#if UNITY_EDITOR

        GrammarObject grammar = AssetDatabase.LoadAssetAtPath<GrammarObject> ("Assets/_PROJECTS/ProcGen/L-Systems/Data/" + grammarRuleset.name + ".asset");

        if (grammar == null) {
            grammar = ScriptableObject.CreateInstance<GrammarObject> ();
            grammar.grammarRuleset = grammarRuleset;

            AssetDatabase.CreateAsset (grammar, "Assets/_PROJECTS/ProcGen/L-Systems/Data/" + grammarRuleset.name + ".asset");
            AssetDatabase.SaveAssets ();
        }

        return grammar;
#endif
        return null;
    }

}