﻿using System.Collections;
using UnityEngine;

public class GenerateLSystem : MonoBehaviour {

    [Header ("L-System")]
    [SerializeField] GrammarObject grammarObject;
    LSystemProcessor LSystemProcessor;

    [Header ("Settings")]
    [SerializeField] int iterations;
    [SerializeField] float lineLength;

    [Range (0.5f, 5f)]
    [SerializeField] float waitInterval = 0.5f;

    [Range (0f, 5f)]
    [SerializeField] float drawWaitInterval = 0.5f;

    [Range (1, 1000)]
    [SerializeField] int stepsPerFrame = 1;
    [SerializeField] bool debugOutput = true;
    [SerializeField] bool playIterations = true;

    [Header ("References")]
    [SerializeField] GameObject lineRendererPrefab;
    [SerializeField] Transform lineRendererParent;
    [SerializeField] Transform drawPoint;
    new Camera camera;

    [Header ("Diagnostics")]
    [TextArea]
    [SerializeField] string sequence;
    [SerializeField] int currentIteration;

    bool interpreting = false;
    bool ended = false;
    bool abort = false;

    Coroutine rulesRoutine;
    Coroutine interpretRoutine;

    void Start () {
        camera = Camera.main;

        LSystemProcessor = new LSystemProcessor (drawPoint, lineLength, lineRendererPrefab, lineRendererParent);
    }

    public void StartGenerate () {
        if (interpreting) return;
        if (grammarObject == null) return;

        StopAllCoroutines ();
        rulesRoutine = StartCoroutine (ApplyRules ());
    }

    public void StopGenerate () {
        abort = true;
        LSystemProcessor?.Clear ();
    }

    void OnValidate () {
        if (Application.isPlaying) {
            abort = true;
            LSystemProcessor?.Clear ();
        }
    }

    IEnumerator ApplyRules () {
        LSystemProcessor?.Clear ();

        //Reset flags
        ended = false;
        abort = false;

        sequence = grammarObject.grammarRuleset.axiom;

        for (int i = 0; i < iterations; i++) {
            currentIteration = i;
            sequence = NextSequence (sequence);

            if ((playIterations && i < iterations) ||
                (!playIterations && i >= iterations - 1)) {

                if (interpretRoutine != null) StopCoroutine (interpretRoutine);
                interpretRoutine = StartCoroutine (InterpretSequence (sequence, i >= iterations - 1));

                if (debugOutput) Debug.Log ($"Player iteration {i}");

                while (interpreting) {
                    yield return null;
                }

                if (playIterations) yield return new WaitForSeconds (2);
            }
        }

        yield return null;
    }

    string NextSequence (string _sequence) {
        string nextSequence = string.Empty;

        char[] characters = _sequence.ToCharArray ();
        for (int i = 0; i < characters.Length; i++) {
            bool found = false;
            for (int j = 0; j < grammarObject.grammarRuleset.grammars.Count; j++) {
                Grammar selectedGrammar = null;
                if (grammarObject.grammarRuleset.grammars[j].grammarList.Count > 1) {
                    float totalProbability = 0;
                    for (int x = 0; x < grammarObject.grammarRuleset.grammars[j].grammarList.Count; x++) {
                        grammarObject.grammarRuleset.grammars[j].grammarList[x].offsetProbability = totalProbability + grammarObject.grammarRuleset.grammars[j].grammarList[x].probability;
                        totalProbability += grammarObject.grammarRuleset.grammars[j].grammarList[x].probability;
                    }
                    float randomProbability = Random.Range (0, totalProbability);
                    for (int x = 0; x < grammarObject.grammarRuleset.grammars[j].grammarList.Count; x++) {
                        if (randomProbability <= grammarObject.grammarRuleset.grammars[j].grammarList[x].offsetProbability) {
                            selectedGrammar = grammarObject.grammarRuleset.grammars[j].grammarList[x];
                            break;
                        }
                    }
                } else {
                    selectedGrammar = grammarObject.grammarRuleset.grammars[j].grammarList[0];
                }

                if (characters[i] == selectedGrammar.variable) {
                    nextSequence += selectedGrammar.rule;
                    found = true;
                    break;
                }
            }

            if (!found) {
                nextSequence += characters[i];
            }
        }

        return nextSequence;
    }

    IEnumerator InterpretSequence (string _sequence, bool last) {
        interpreting = true;

        LSystemProcessor.Reset ();
        char[] characters = _sequence.ToCharArray ();

        int _stepCount = 0;

        for (int j = 0; j < characters.Length; j++) {
            bool wait = false;
            _stepCount++;

            if (abort || LSystemProcessor == null) break;

            wait = LSystemProcessor.Execute (characters[j], grammarObject.grammarRuleset, debugOutput);

            LSystemProcessor.currentRenderer.positionCount++;
            LSystemProcessor.currentRenderer.SetPosition (LSystemProcessor.currentRenderer.positionCount - 1, drawPoint.localPosition);

            if (drawPoint.localPosition.y > camera.orthographicSize * 2) {
                camera.orthographicSize = ((int) drawPoint.localPosition.y / 2) + 1;
                transform.position = new Vector3 (0, -camera.orthographicSize, 0);
            }

            if (wait && _stepCount % (stepsPerFrame * (currentIteration + 1)) == 0) yield return new WaitForSeconds (drawWaitInterval / (currentIteration + 1));
        }

        yield return new WaitForSeconds (waitInterval);

        interpreting = false;

        if (!last && !abort) {
            LSystemProcessor.Clear ();
        }

    }

}