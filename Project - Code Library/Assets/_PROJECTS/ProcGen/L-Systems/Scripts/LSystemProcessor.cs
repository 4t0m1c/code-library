﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PosRot {
    public Vector3 position;
    public Quaternion rotation;

    public PosRot (Vector3 position, Quaternion rotation) {
        this.position = position;
        this.rotation = rotation;
    }
}

public class LSystemProcessor {

    internal Transform drawPoint;
    internal float lineLength;
    internal List<LineRenderer> lines = new List<LineRenderer> ();
    internal GameObject lineRendererPrefab;
    internal Transform lineRendererParent;
    public LineRenderer currentRenderer;
    internal Stack<PosRot> posRotMemory = new Stack<PosRot> ();

    public LSystemProcessor (Transform _drawPoint, float _lineLength, GameObject _lineRendererPrefab, Transform _lineRendererParent) {
        drawPoint = _drawPoint;
        lineLength = _lineLength;
        lineRendererPrefab = _lineRendererPrefab;
        lineRendererParent = _lineRendererParent;

        if (Application.isPlaying) {
            Reset ();
        }
    }

    public void Reset () {
        drawPoint.localPosition = Vector3.zero;
        drawPoint.localEulerAngles = Vector3.zero;

        lines.Clear ();
        lines.Add (GameObject.Instantiate (lineRendererPrefab, drawPoint.position, drawPoint.rotation, lineRendererParent).GetComponent<LineRenderer> ());
        currentRenderer = lines[0];
        posRotMemory = new Stack<PosRot> ();
    }

    public void Clear () {
        if (Application.isPlaying) {
            for (int x = lines.Count - 1; x >= 0; x--) {
                GameObject.Destroy (lines[x].gameObject);
            }
        }
        lines.Clear ();
    }

    public bool Execute (char _char, GrammarRuleset grammarRuleset, bool debugOutput) {
        for (int i = 0; i < grammarRuleset.grammarActions.Count; i++) {
            if (grammarRuleset.grammarActions[i].characters.Contains (_char)) {
                for (int j = 0; j < grammarRuleset.grammarActions[i].actions.Count; j++) {
                    GrammarActions action = grammarRuleset.grammarActions[i].actions[j];

                    switch (action) {
                        case GrammarActions.Translate:
                            if (debugOutput) Debug.Log ($"Action: Forward");
                            drawPoint.Translate (Vector3.up * lineLength);
                            break;
                        case GrammarActions.RotateCW:
                            if (debugOutput) Debug.Log ($"Action: + rotationAngle");
                            drawPoint.localEulerAngles += new Vector3 (0, 0, grammarRuleset.rotationAngle);
                            break;
                        case GrammarActions.RotateCCW:
                            if (debugOutput) Debug.Log ($"Action: - rotationAngle");
                            drawPoint.localEulerAngles -= new Vector3 (0, 0, grammarRuleset.rotationAngle);
                            break;
                        case GrammarActions.Save:
                            if (debugOutput) Debug.Log ($"Action: Save Point");
                            posRotMemory.Push (new PosRot (drawPoint.localPosition, drawPoint.rotation));
                            break;
                        case GrammarActions.Restore:
                            if (debugOutput) Debug.Log ($"Action: Restore Point");
                            PosRot posRot = posRotMemory.Pop ();
                            drawPoint.localPosition = posRot.position;
                            drawPoint.rotation = posRot.rotation;

                            lines.Add (GameObject.Instantiate (lineRendererPrefab, lineRendererParent).GetComponent<LineRenderer> ());
                            currentRenderer = lines[lines.Count - 1];
                            break;
                        default:
                        case GrammarActions.None:
                            if (debugOutput) Debug.Log ($"Action NOT FOUND {_char}");
                            return false;
                    }

                }
            }
        }

        return true;
    }
}