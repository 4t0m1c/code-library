﻿using UnityEditor;
using UnityEngine;

public class GrammarConstructorEditorWindow : EditorWindow {

    GrammarObject grammarObject;
    SerializedObject grammarObjectSerialized;
    Vector2 scrollPos;

    [MenuItem ("L-Systems/Grammar Constructor")]
    static void ShowWindow () {
        var window = GetWindow<GrammarConstructorEditorWindow> ();
        window.titleContent = new GUIContent ("Grammar Constructor");
        window.Show ();
    }

    void OnGUI () {
        LoadOrCreate ();
        GrammarEdit ();
    }

    void LoadOrCreate () {
        //Show Object Field to load/create new GrammarObject
        EditorGUILayout.Space (30);
        EditorGUILayout.BeginHorizontal ();

        EditorGUILayout.Space (30);
        EditorGUILayout.BeginVertical ();
        GUILayout.Label ("Select existing Grammar");
        grammarObject = EditorGUILayout.ObjectField (grammarObject, typeof (GrammarObject), false) as GrammarObject;
        EditorGUILayout.EndVertical ();

        EditorGUILayout.Space (30);
        EditorGUILayout.BeginVertical ();
        GUILayout.Label ("Create new Grammar");
        if (GUILayout.Button ("New Grammar")) {

            grammarObject = ScriptableObject.CreateInstance<GrammarObject> ();

            string path = EditorUtility.SaveFilePanelInProject ("Save Grammar", "Grammar", "asset",
                "Please enter a file name to save the file to");
            if (path.Length != 0) {
                AssetDatabase.CreateAsset (grammarObject, path);
                AssetDatabase.SaveAssets ();

                // As we are saving to the asset folder, tell Unity to scan for modified or new assets
                AssetDatabase.Refresh ();
            }
        }
        EditorGUILayout.EndVertical ();
        EditorGUILayout.Space (30);

        EditorGUILayout.EndHorizontal ();
    }

    void GrammarEdit () {
        EditorGUILayout.Space (30);
        scrollPos = EditorGUILayout.BeginScrollView (scrollPos, GUILayout.Width (position.width), GUILayout.Height (position.height - 120));
        if (grammarObject != null) {
            grammarObjectSerialized = new SerializedObject (grammarObject);
            grammarObjectSerialized.Update ();

            SerializedProperty grammarRulesetProperty = grammarObjectSerialized.FindProperty ("grammarRuleset");

            GUILayout.Label ("Parameters");

            EditorGUILayout.BeginVertical (EditorStyles.helpBox);
            EditorGUILayout.Space ();
            GrammarParameters (grammarRulesetProperty);
            EditorGUILayout.Space ();
            EditorGUILayout.EndVertical ();

            GUILayout.Label ("Rules");
            EditorGUILayout.BeginVertical (EditorStyles.helpBox);
            Grammars (grammarRulesetProperty);
            EditorGUILayout.EndVertical ();

            GUILayout.Label ("Actions");
            EditorGUILayout.BeginVertical (EditorStyles.helpBox);
            Actions (grammarRulesetProperty);
            EditorGUILayout.EndVertical ();

            grammarObjectSerialized.ApplyModifiedProperties ();
        }
        EditorGUILayout.EndScrollView ();
    }

    void GrammarParameters (SerializedProperty grammarRulesetProperty) {
        EditorGUILayout.BeginHorizontal ();

        EditorGUILayout.BeginVertical ();
        SerializedProperty _name = grammarRulesetProperty.FindPropertyRelative ("name");
        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
        GUILayout.Label ("Name", EditorStyles.miniLabel);
        EditorGUILayout.PropertyField (_name, GUIContent.none);
        EditorGUILayout.EndVertical ();

        EditorGUILayout.BeginVertical ();
        SerializedProperty _axiom = grammarRulesetProperty.FindPropertyRelative ("axiom");
        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
        GUILayout.Label ("Axiom", EditorStyles.miniLabel);
        EditorGUILayout.PropertyField (_axiom, GUIContent.none);
        EditorGUILayout.EndVertical ();

        EditorGUILayout.BeginVertical ();
        SerializedProperty _angle = grammarRulesetProperty.FindPropertyRelative ("rotationAngle");
        //DO NOT USE EditorGUILayout.PrefixLabel - it messes up the auto layout
        GUILayout.Label ("Angle", EditorStyles.miniLabel);
        EditorGUILayout.PropertyField (_angle, GUIContent.none);
        EditorGUILayout.EndVertical ();

        EditorGUILayout.EndHorizontal ();
    }

    void Grammars (SerializedProperty grammarRulesetProperty) {
        SerializedProperty grammarListProperty = grammarRulesetProperty.FindPropertyRelative ("grammars");

        if (grammarListProperty.arraySize == 0) {
            EditorGUILayout.Space ();
            EditorGUILayout.BeginHorizontal ();
            GUILayout.FlexibleSpace ();
            if (GUILayout.Button ("Add Rule", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.InsertArrayElementAtIndex (0);
                GrammarList (grammarListProperty.GetArrayElementAtIndex (0), true);
            }
            GUILayout.FlexibleSpace ();
            EditorGUILayout.EndHorizontal ();
        }

        for (int i = 0; i < grammarListProperty.arraySize; i++) {

            SerializedProperty item = grammarListProperty.GetArrayElementAtIndex (i);

            EditorGUILayout.BeginVertical ();
            EditorGUILayout.Space ();

            // EditorGUILayout.PropertyField (item, GUIContent.none);
            GrammarList (item);

            EditorGUILayout.BeginHorizontal ();
            GUILayout.FlexibleSpace ();
            if (GUILayout.Button (grammarListProperty.arraySize > 0 ? "Duplicate Rule" : "Add Rule", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.InsertArrayElementAtIndex (i);
            }
            if (GUILayout.Button ("Remove Rule", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.DeleteArrayElementAtIndex (i);
                i--;
            }
            GUILayout.FlexibleSpace ();
            EditorGUILayout.EndHorizontal ();

            EditorGUILayout.EndVertical ();
            // EditorGUILayout.Space ();
        }
        EditorGUILayout.Space ();
    }

    void GrammarList (SerializedProperty property, bool newList = false) {
        SerializedProperty grammarListProperty = property.FindPropertyRelative ("grammarList");

        if (newList) grammarListProperty.InsertArrayElementAtIndex (0);

        EditorGUILayout.BeginVertical (EditorStyles.helpBox);

        EditorGUILayout.BeginHorizontal ();

        GUILayout.Label ("Var", EditorStyles.miniLabel, GUILayout.Width (30));
        GUILayout.Label ("Rule", EditorStyles.miniLabel);
        GUILayout.Label ("Probability", EditorStyles.miniLabel, GUILayout.Width (80));

        EditorGUILayout.EndHorizontal ();

        for (int i = 0; i < grammarListProperty.arraySize; i++) {

            SerializedProperty item = grammarListProperty.GetArrayElementAtIndex (i);

            EditorGUILayout.BeginHorizontal ();

            EditorGUILayout.PropertyField (item, GUIContent.none);

            if (GUILayout.Button ("+", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.InsertArrayElementAtIndex (grammarListProperty.arraySize);
            }
            if (grammarListProperty.arraySize > 1 && GUILayout.Button ("-", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.DeleteArrayElementAtIndex (i);
                i--;
            }

            EditorGUILayout.EndHorizontal ();
            // EditorGUILayout.Space ();
        }
        EditorGUILayout.EndVertical ();
    }

    void Actions (SerializedProperty grammarRulesetProperty) {
        SerializedProperty grammarListProperty = grammarRulesetProperty.FindPropertyRelative ("grammarActions");

        if (grammarListProperty.arraySize == 0) {
            EditorGUILayout.Space ();
            EditorGUILayout.BeginHorizontal ();
            GUILayout.FlexibleSpace ();
            if (GUILayout.Button ("Add Action", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.InsertArrayElementAtIndex (0);
                ActionList (grammarListProperty.GetArrayElementAtIndex (0), true);
            }
            GUILayout.FlexibleSpace ();
            EditorGUILayout.EndHorizontal ();
        }

        EditorGUILayout.Space ();
        for (int i = 0; i < grammarListProperty.arraySize; i++) {

            SerializedProperty item = grammarListProperty.GetArrayElementAtIndex (i);

            EditorGUILayout.BeginVertical ();
            EditorGUILayout.Space ();

            // EditorGUILayout.PropertyField (item, GUIContent.none);
            EditorGUILayout.BeginVertical (EditorStyles.helpBox);
            ActionList (item);
            EditorGUILayout.EndVertical ();

            EditorGUILayout.Space ();
            EditorGUILayout.BeginHorizontal ();
            GUILayout.FlexibleSpace ();
            if (GUILayout.Button (grammarListProperty.arraySize > 0 ? "Duplicate Action" : "Add Action", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.InsertArrayElementAtIndex (i);
            }
            if (GUILayout.Button ("Remove Action", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                grammarListProperty.DeleteArrayElementAtIndex (i);
                i--;
            }
            GUILayout.FlexibleSpace ();
            EditorGUILayout.EndHorizontal ();

            EditorGUILayout.EndVertical ();
        }
        EditorGUILayout.Space ();
    }

    void ActionList (SerializedProperty property, bool newList = false) {
        EditorGUILayout.BeginHorizontal ();

        //Characters

        SerializedProperty charactersProp = property.FindPropertyRelative ("characters");
        if (newList) charactersProp.InsertArrayElementAtIndex (0);

        EditorGUILayout.BeginVertical ();
        for (int i = 0; i < charactersProp.arraySize; i++) {

            SerializedProperty item = charactersProp.GetArrayElementAtIndex (i);

            EditorGUILayout.BeginHorizontal ();

            EditorGUILayout.PropertyField (item, GUIContent.none);

            if (GUILayout.Button ("-", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                charactersProp.DeleteArrayElementAtIndex (i);
                i--;
            }

            EditorGUILayout.EndHorizontal ();
            // EditorGUILayout.Space ();
        }
        EditorGUILayout.EndVertical ();

        if (GUILayout.Button ("+", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
            charactersProp.InsertArrayElementAtIndex (charactersProp.arraySize);
        }

        //Actions

        SerializedProperty actionsProp = property.FindPropertyRelative ("actions");
        if (newList) actionsProp.InsertArrayElementAtIndex (0);

        EditorGUILayout.BeginVertical ();
        for (int i = 0; i < actionsProp.arraySize; i++) {

            SerializedProperty item = actionsProp.GetArrayElementAtIndex (i);

            EditorGUILayout.BeginHorizontal ();

            EditorGUILayout.PropertyField (item, GUIContent.none);

            if (GUILayout.Button ("-", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
                actionsProp.DeleteArrayElementAtIndex (i);
                i--;
            }

            EditorGUILayout.EndHorizontal ();
            // EditorGUILayout.Space ();
        }
        EditorGUILayout.EndVertical ();

        if (GUILayout.Button ("+", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) {
            actionsProp.InsertArrayElementAtIndex (actionsProp.arraySize);
        }

        EditorGUILayout.EndHorizontal ();
    }

    void GrammarActions () {

    }
}