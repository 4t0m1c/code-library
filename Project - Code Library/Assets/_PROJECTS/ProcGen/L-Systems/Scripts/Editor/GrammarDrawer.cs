﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof (Grammar))]
public class GrammarDrawer : PropertyDrawer {

    //https://docs.unity3d.com/ScriptReference/PropertyDrawer.html

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty (position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var variableRect = new Rect (position.x, position.y, 30, position.height);
        var ruleRect = new Rect (variableRect.x + variableRect.width + 5, position.y, (position.width) - 70, position.height);
        var probabilityRect = new Rect (ruleRect.x + ruleRect.width + 5, position.y, 30, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField (variableRect, property.FindPropertyRelative ("variable"), GUIContent.none);
        EditorGUI.PropertyField (ruleRect, property.FindPropertyRelative ("rule"), GUIContent.none);
        EditorGUI.PropertyField (probabilityRect, property.FindPropertyRelative ("probability"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty ();
    }
}