﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "GrammarObject", menuName = "L-Systems/Grammar Object", order = 0)]
public class GrammarObject : ScriptableObject {
    public GrammarRuleset grammarRuleset;
}