﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.MazeGen {

    public class MazeNav : MonoBehaviour {

        [Range (0.01f, 1f)]
        [SerializeField] float stepWait = 0.1f;
        [SerializeField] bool giveMeResults = false;
        [SerializeField] int maxStepsPerFrame = 1000;
        [SerializeField] TrailRenderer trail;

        [Header ("Diagnostics")]
        [SerializeField] bool navigating = false;
        [SerializeField] int numberOfSteps = 0;
        [SerializeField] List<Vector3Int> directionBuffer = new List<Vector3Int> ();
        Vector3 startPos, endPos;

        void OnEnable () {
            MazeGen.OnMazeGenerated += StartNav;
        }

        void OnDisable () {
            MazeGen.OnMazeGenerated -= StartNav;
        }

        [ContextMenu ("Start Navigation")]
        public void StartNav (Vector3 startPos, Vector3 endPos) {
            this.startPos = startPos;
            this.endPos = endPos;

            navigating = true;
            StartCoroutine (Navigate (startPos, endPos));
        }

        public void RunAgain () {
            StartNav (startPos, endPos);
        }

        bool CheckRightWall () {
            return (Physics.Raycast (transform.position + (transform.up * 0.5f), transform.right, 1));
        }
        bool CheckForwardWall () {
            return (Physics.Raycast (transform.position + (transform.up * 0.5f), transform.forward, 1));
        }

        IEnumerator Navigate (Vector3 startPos, Vector3 endPos) {
            transform.position = startPos;
            numberOfSteps = 0;
            trail.Clear ();

            while (navigating) {

                if (CheckRightWall ()) {
                    if (CheckForwardWall ()) {
                        // Debug.Log ($"Left Turn");
                        transform.Rotate (Vector3.up * -90);
                        // yield return new WaitForSeconds (stepWait);
                    } else {
                        // Debug.Log ($"Move Forward");
                        transform.Translate (Vector3.forward);
                    }
                } else {
                    // Debug.Log ($"Right Turn");
                    transform.Rotate (Vector3.up * 90);
                    // yield return new WaitForSeconds (stepWait);

                    if (!CheckForwardWall ()) {
                        // Debug.Log ($"Move Forward");
                        transform.Translate (Vector3.forward);
                        // yield return new WaitForSeconds (stepWait);
                    }
                }

                numberOfSteps++;

                if ((transform.position - endPos).sqrMagnitude < 0.5f) {
                    navigating = false;
                    Debug.Log ($"Made it in {numberOfSteps} steps!");
                }

                if (stepWait > 0 && !giveMeResults) {
                    yield return new WaitForSeconds (stepWait);
                } else if (giveMeResults && numberOfSteps % maxStepsPerFrame == 0) {
                    yield return null;
                }
            }
        }

    }
}