﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.MazeGen {
    public class MazeGen : MonoBehaviour {

        public delegate void MazeGenEvents (Vector3 startPos, Vector3 endPos);
        public static event MazeGenEvents OnMazeGenerated;

        [Header ("Settings")]
        [SerializeField] GameObject cellPrefab;
        [SerializeField] Transform cellParent;
        [SerializeField] Transform currentCellMarker;
        [SerializeField] Vector2 mazeSize;
        [SerializeField] bool stepByStep = true;
        [SerializeField] bool logOutput = true;
        [SerializeField] float generateWait;
        [SerializeField] bool continuous = false;

        [Header ("Output")]
        public List<MazeCell> cells = new List<MazeCell> ();
        public List<MazeCell> cellsVisited = new List<MazeCell> ();
        [SerializeField] MazeCell startingCell;

        [ContextMenu ("Generate Maze")]
        public void GenerateMaze () {

            ClearMaze ();

            GenerateGrid ();

            StartCoroutine (BreakWalls ());

        }

        void GenerateGrid () {
            //Generate Grid

            for (int col = 0; col < mazeSize.x; col++) {
                for (int row = 0; row < mazeSize.y; row++) {
                    GameObject newCell = Instantiate (cellPrefab, new Vector3 (col, 0, row), Quaternion.identity);
                    newCell.transform.SetParent (cellParent, false);
                    MazeCell mazeCell = newCell.GetComponent<MazeCell> ();
                    mazeCell.Init (new Vector2 (col, row), mazeSize, this);
                    cells.Add (mazeCell);
                }
            }

            transform.position = new Vector3 (-((mazeSize.x - 1) / 2), 0, -((mazeSize.y - 1) / 2));

            startingCell = cells[0]; //Can be randomly selected
        }

        IEnumerator BreakWalls () {
            MazeCell currentCell = startingCell;
            cellsVisited.Add (startingCell);

            bool mazeComplete = false;

            while (!mazeComplete) {
                //Get random neighbour within walls
                MazeCell nextCell = currentCell.FindNextCell ();

                if (nextCell == null) {
                    if (currentCell.allWallsBroken) {
                        if (cellsVisited.IndexOf (currentCell) > 0) {
                            if (logOutput) Debug.Log ($"Complete cell, moving backwards.");
                            currentCell = cellsVisited[cellsVisited.IndexOf (currentCell) - 1];
                        } else {
                            if (logOutput) Debug.Log ($"End of the road");
                            mazeComplete = true;
                            break;
                        }
                    } else {
                        if (logOutput) Debug.Log ($"Other walls to check", currentCell.gameObject);
                    }
                } else {
                    Vector2 direction = nextCell.coordinate - currentCell.coordinate;
                    if (logOutput) Debug.Log ($"Got next cell {nextCell.coordinate} | Direction {direction}", nextCell.gameObject);
                    currentCell.BreakWall (direction, false);
                    currentCell = nextCell;
                    currentCell.BreakWall (direction, true);
                    cellsVisited.Add (currentCell);
                    currentCellMarker.position = currentCell.transform.position;

                    if (stepByStep) yield return new WaitForSeconds (generateWait);
                }

            }

            yield return null;

            Vector3 startPos = new Vector3 (-(mazeSize.x / 2 - 0.5f), 0, 0);
            Vector3 endPos = new Vector3 ((mazeSize.x / 2 - 0.5f), 0, 0);
            Debug.Log ($"StartPos: {startPos}, EndPos: {endPos}");
            if (OnMazeGenerated != null) OnMazeGenerated (startPos, endPos);
            currentCellMarker.position = endPos;

            if (continuous) {
                yield return new WaitForSeconds (1);
                GenerateMaze ();
            }
        }

        public MazeCell GetCellAtCoordinate (Vector2 coordinate) {
            for (int i = 0; i < cells.Count; i++) {
                if (cells[i].coordinate == coordinate) {
                    return cells[i];
                }
            }
            return null;
        }

        void ClearMaze () {
            for (int i = cells.Count - 1; i >= 0; i--) {
                DestroyImmediate (cells[i].gameObject);
            }
            cells.Clear ();
            cellsVisited.Clear ();
        }

    }
}