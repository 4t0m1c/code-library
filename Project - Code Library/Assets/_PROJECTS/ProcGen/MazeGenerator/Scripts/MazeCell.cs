﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.MazeGen {

    [System.Serializable]
    public enum Directions {
        North,
        East,
        South,
        West
    }

    public class MazeCell : MonoBehaviour {

        public Vector2 coordinate;
        public bool allWallsBroken {
            get {
                return brokenWalls == 3 || wallsLeft.Count == 0; //At least 2 walls should always be active
            }
        }

        public List<Directions> wallsLeft = new List<Directions> (); //NESW

        [SerializeField] GameObject wallN;
        [SerializeField] GameObject wallE;
        [SerializeField] GameObject wallS;
        [SerializeField] GameObject wallW;

        int brokenWalls = 0;

        MazeGen mazeGen;

        public void Init (Vector2 coordinates, Vector2 gridSize, MazeGen mazeGen) {
            this.mazeGen = mazeGen;
            coordinate = coordinates;

            wallsLeft.Add (Directions.North);
            wallsLeft.Add (Directions.East);
            wallsLeft.Add (Directions.South);
            wallsLeft.Add (Directions.West);

            if (coordinates.x == gridSize.x - 1) wallsLeft.Remove (Directions.East);
            if (coordinates.x == 0) wallsLeft.Remove (Directions.West);
            if (coordinates.y == gridSize.y - 1) wallsLeft.Remove (Directions.North);
            if (coordinates.y == 0) wallsLeft.Remove (Directions.South);
        }

        public MazeCell FindNextCell () {
            if (allWallsBroken) return null;

            Directions randomDirection = wallsLeft[Random.Range (0, wallsLeft.Count)];
            Vector2 coordinateToFind = coordinate;

            if (randomDirection == Directions.North) coordinateToFind.y += 1;
            if (randomDirection == Directions.East) coordinateToFind.x += 1;
            if (randomDirection == Directions.South) coordinateToFind.y += -1;
            if (randomDirection == Directions.West) coordinateToFind.x += -1;

            wallsLeft.Remove (randomDirection);

            MazeCell nextCell = mazeGen.GetCellAtCoordinate (coordinateToFind);
            if (mazeGen.cellsVisited.Contains (nextCell)) {
                return null;
            } else {
                if (randomDirection == Directions.North) nextCell.wallsLeft.Remove (Directions.South);
                if (randomDirection == Directions.East) nextCell.wallsLeft.Remove (Directions.West);
                if (randomDirection == Directions.South) nextCell.wallsLeft.Remove (Directions.North);
                if (randomDirection == Directions.West) nextCell.wallsLeft.Remove (Directions.East);

                return nextCell;
            }
        }

        public bool BreakWall (Vector2 direction, bool entry) {
            /* 
            |------|    |------|
            |      | -> |      | x = 1 -> wallW
            |------|    |------|
            */
            bool wallBroken = false;
            if ((direction.x == 1 && entry) || (direction.x == -1 && !entry) && wallW.activeSelf) {
                wallW.SetActive (false);
                wallBroken = true;
                brokenWalls++;
            }
            if ((direction.x == -1 && entry) || (direction.x == 1 && !entry) && wallE.activeSelf) {
                wallE.SetActive (false);
                wallBroken = true;
                brokenWalls++;
            }
            if ((direction.y == -1 && entry) || (direction.y == 1 && !entry) && wallN.activeSelf) {
                wallN.SetActive (false);
                wallBroken = true;
                brokenWalls++;
            }
            if ((direction.y == 1 && entry) || (direction.y == -1 && !entry) && wallS.activeSelf) {
                wallS.SetActive (false);
                wallBroken = true;
                brokenWalls++;
            }

            return wallBroken;
        }

    }
}