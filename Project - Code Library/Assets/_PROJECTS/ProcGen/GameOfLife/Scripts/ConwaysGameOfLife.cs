﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.GameOfLife {

    public class ConwaysGameOfLife : MonoBehaviour {
        [SerializeField] PopulateGrid grid;
        [SerializeField] float waitTime = 0.1f;

        new Camera camera;
        bool changeToDead = false;
        bool simulating = false;

        void Awake () {
            camera = Camera.main;
        }

        void Update () {
            if (Input.GetMouseButtonDown (0)) {
                SelectingCells (true);
            } else if (Input.GetMouseButton (0)) {
                SelectingCells (false);
            }
        }

        void SelectingCells (bool initial) {
            Vector3 worldPoint = camera.ScreenToWorldPoint (Input.mousePosition);

            worldPoint = grid.transform.InverseTransformPoint (worldPoint);
            worldPoint = new Vector3 (Mathf.Round (worldPoint.x), Mathf.Round (worldPoint.y), 0);

            if (worldPoint.x > grid.gridSize.x - 1 || worldPoint.x < 0) return;
            if (worldPoint.y > grid.gridSize.y - 1 || worldPoint.y < 0) return;

            int childIndex = (int) worldPoint.x + ((int) worldPoint.y * grid.gridSize.y);

            if (initial) {
                changeToDead = grid.cells[childIndex].alive;
            }

            grid.cells[childIndex].Set (!changeToDead);
        }

        public void ResetCells () {
            for (int i = 0; i < grid.cells.Length; i++) {
                grid.cells[i].Set (false);
            }
        }

        public void StopSimulate () {
            simulating = false;
        }

        public void StartSimulate () {
            simulating = true;
            StartCoroutine (GameOfLife ());
        }

        IEnumerator GameOfLife () {
            while (simulating) {
                for (int i = 0; i < grid.cells.Length; i++) {

                    Cell cell = grid.cells[i];

                    int alive = 0;
                    for (int j = 0; j < cell.neighbourhood.Length; j++) {
                        if (cell.neighbourhood[j].alive) alive++;
                    }

                    if (cell.alive && (alive < 2)) {
                        //Rule 1: Any live cell with fewer than two live neighbours dies, as if by underpopulation.
                        cell.NextGeneration (false);
                    }
                    if (cell.alive && (alive == 2 || alive == 3)) {
                        //Rule 2: Any live cell with two or three live neighbours lives on to the next generation.
                        cell.NextGeneration (true);
                    }
                    if (cell.alive && (alive > 3)) {
                        //Rule 3: Any live cell with more than three live neighbours dies, as if by overpopulation.
                        cell.NextGeneration (false);
                    }
                    if (!cell.alive && alive == 3) {
                        //Rule 4: Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                        cell.NextGeneration (true);
                    }
                }

                yield return new WaitForSeconds (waitTime);
            }
        }

        public void SetWait(float wait) {
            waitTime = wait;
        }

    }

}