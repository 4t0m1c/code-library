﻿using UnityEditor;
using UnityEngine;

namespace At0m1c.GameOfLife {

    [CustomEditor (typeof (PopulateGrid))]
    public class PopulateGridEditor : Editor {
        public override void OnInspectorGUI () {
            base.OnInspectorGUI ();

            if (GUILayout.Button ("Generate Grid")) {
                ((PopulateGrid) target).GenerateGrid ();
            }

        }
    }
    
}