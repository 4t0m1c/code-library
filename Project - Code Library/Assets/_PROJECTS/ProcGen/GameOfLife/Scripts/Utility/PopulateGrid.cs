﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace At0m1c.GameOfLife {
    public class PopulateGrid : MonoBehaviour {

        [SerializeField] GameObject cellPrefab;
        public Vector2Int gridSize;

        public Cell[] cells;

        public void GenerateGrid () {
            //Remove or add desired cells
            int desiredCells = gridSize.x * gridSize.y;
            if (desiredCells < transform.childCount) {
                int difference = transform.childCount - desiredCells;
                for (int i = 0; i < difference; i++) {
                    DestroyImmediate (transform.GetChild (transform.childCount - 1).gameObject);
                }
            } else if (desiredCells > transform.childCount) {
                int difference = desiredCells - transform.childCount;
                for (int i = 0; i < difference; i++) {
#if UNITY_EDITOR
                    PrefabUtility.InstantiatePrefab (cellPrefab, transform);
#else 
                    Instantiate (cellPrefab, transform);
#endif
                }
            }

            cells = new Cell[transform.childCount];

            //Arrange cells
            Vector2Int coordinate = Vector2Int.zero;
            for (int i = 0; i < transform.childCount; i++) {
                GameObject cell = transform.GetChild (i).gameObject;
                cell.name = "Cell " + coordinate;

                cell.transform.localPosition = new Vector3 (coordinate.x, coordinate.y);
                cells[i] = cell.GetComponent<Cell> ();
                cells[i].coordinate = coordinate;

                coordinate.x++;
                if (coordinate.x >= gridSize.x) {
                    coordinate.y++;
                    coordinate.x = 0;
                }
            }

            transform.position = new Vector3 (-gridSize.x / 2, -gridSize.y / 2, 0); //Offset parent container by half the grid size to be camera centred
            transform.position += new Vector3 (gridSize.x % 2 == 0 ? 0.5f : 0, gridSize.y % 2 == 0 ? 0.5f : 0, 0); //Account for center pivot of each cell

            Camera.main.orthographicSize = (gridSize.x > gridSize.y ? gridSize.x : gridSize.y) / 1.8f;
            Camera.main.orthographicSize = Camera.main.orthographicSize < 3 ? 3 : Camera.main.orthographicSize;

            for (int i = 0; i < desiredCells; i++) {
                cells[i].neighbourhood = new Cell[] {
                    //N
                    GetCell (new Vector2Int (cells[i].coordinate.x, cells[i].coordinate.y + 1)),
                    //NE
                    GetCell (new Vector2Int (cells[i].coordinate.x + 1, cells[i].coordinate.y + 1)),
                    //E
                    GetCell (new Vector2Int (cells[i].coordinate.x + 1, cells[i].coordinate.y)),
                    //SE
                    GetCell (new Vector2Int (cells[i].coordinate.x + 1, cells[i].coordinate.y - 1)),
                    //S
                    GetCell (new Vector2Int (cells[i].coordinate.x, cells[i].coordinate.y - 1)),
                    //SW
                    GetCell (new Vector2Int (cells[i].coordinate.x - 1, cells[i].coordinate.y - 1)),
                    //W
                    GetCell (new Vector2Int (cells[i].coordinate.x - 1, cells[i].coordinate.y)),
                    //NW
                    GetCell (new Vector2Int (cells[i].coordinate.x - 1, cells[i].coordinate.y + 1)),
                };
            }
        }

        Cell GetCell (Vector2Int coordinate) {
            if (coordinate.x > gridSize.x - 1) coordinate.x = 0;
            if (coordinate.x < 0) coordinate.x = gridSize.x - 1;

            if (coordinate.y > gridSize.y - 1) coordinate.y = 0;
            if (coordinate.y < 0) coordinate.y = gridSize.y - 1;

            return cells[(int) coordinate.x + ((int) coordinate.y * gridSize.y)];
        }

    }
}