﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.GameOfLife {
    public class Cell : MonoBehaviour {

        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] Color colourLive;
        [SerializeField] Color colourDead;

        public bool alive = false;

        public Vector2Int coordinate;

        public Cell[] neighbourhood;

        public void Flip () {
            alive = !alive;

            spriteRenderer.color = alive ? colourLive : colourDead;
        }

        public void Set (bool _alive) {
            if (alive != _alive) Flip ();
        }

        public void NextGeneration (bool _alive) {
            StartCoroutine (NextGenerationApply (_alive));
        }

        IEnumerator NextGenerationApply (bool _alive) {
            yield return null;
            Set (_alive);
        }

    }
}