﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.ProcGen {

    public class DunGen : MonoBehaviour {

        bool generating = false;

        [SerializeField] RawImage genTexture;
        [SerializeField] int scaleFactor;

        [Header ("Fill")]
        [SerializeField] int pixelsPerSecond = 1920 * 1080;

        [Header ("DrunkenWalk")]
        [SerializeField] int padding = 3;
        [SerializeField] int stepSpeed = 10;

        void Start () {
            generating = false;
        }

        public void BeginGenerate () {
            generating = !generating;

            if (generating) {
                // StartCoroutine (FillOverTime ());
                StartCoroutine (DrunkenWalkOverTime ());
            }
        }

        IEnumerator FillOverTime () {
            generating = true;
            Texture2D tex = new Texture2D (Screen.width / scaleFactor, Screen.height / scaleFactor, TextureFormat.ARGB32, false);
            float target = pixelsPerSecond * Time.deltaTime;
            yield return null;

            for (int h = 0; h < Screen.height; h++) {
                for (int w = 0; w < Screen.width; w++) {
                    if (target < 0) {
                        yield return null;
                        tex.Apply ();
                        genTexture.texture = tex;
                        target = pixelsPerSecond * Time.deltaTime;
                    }
                    target--;
                    tex.SetPixel (w, h, Color.black);
                }
            }

            tex.Apply ();
            genTexture.texture = tex;

            generating = false;
        }

        IEnumerator DrunkenWalkOverTime () {
            Texture2D tex = new Texture2D (Screen.width / scaleFactor, Screen.height / scaleFactor, TextureFormat.ARGB32, false);

            //Starting point
            Vector2Int point = new Vector2Int (Screen.width / 2 / scaleFactor, Screen.height / 2 / scaleFactor);

            /* 
                REALTIME GENERATION
            */

            while (generating) {
                int steps = stepSpeed;

                while (steps > 0) {
                    // Step
                    bool sideStep = Random.Range (0, 2) == 0 ? false : true;
                    if (sideStep) { //X Movement
                        if (point.x == 0) point.x++;
                        else if (point.x == Screen.width / scaleFactor) point.x--;
                        else {
                            point.x += Random.Range (0, 2) == 0 ? -1 : 1;
                        }
                    } else { //Y Movement
                        if (point.y == 0) point.y++;
                        else if (point.y == Screen.height / scaleFactor) point.y--;
                        else {
                            point.y += Random.Range (0, 2) == 0 ? -1 : 1;
                        }
                    }

                    //Set pixel colour
                    tex.SetPixel (point.x, point.y, Color.black);

                    steps--;
                }

                tex.Apply ();
                genTexture.texture = tex;

                yield return null;
            }

            /* 
                CLEANUP
            */

            Texture2D finalisedTex = new Texture2D (tex.width, tex.height, TextureFormat.ARGB32, false);

            //Add some padding to existing pixels
            for (int h = 0; h < Screen.height / scaleFactor; h++) {
                for (int w = 0; w < Screen.width / scaleFactor; w++) {
                    if (tex.GetPixel (w, h) == Color.black) {
                        // Fill area around point
                        //Center
                        finalisedTex.SetPixel (w, h, Color.black);
                        //Up
                        for (int i = 1; i < padding; i++) { finalisedTex.SetPixel (w, h + i, Color.black); }
                        //Down
                        for (int i = 1; i < padding; i++) { finalisedTex.SetPixel (w, h - i, Color.black); }
                        //Left
                        for (int i = 1; i < padding; i++) { finalisedTex.SetPixel (w - i, h, Color.black); }
                        //Right
                        for (int i = 1; i < padding; i++) { finalisedTex.SetPixel (w + i, h, Color.black); }
                        //Upper Right
                        for (int i = 1; i < padding; i++) {
                            for (int j = 1; j < padding; j++) {
                                finalisedTex.SetPixel (w + j, h + i, Color.black);
                            }
                        }
                        //Upper Left
                        for (int i = 1; i < padding; i++) {
                            for (int j = 1; j < padding; j++) {
                                finalisedTex.SetPixel (w - j, h + i, Color.black);
                            }
                        }
                        //Lower Left
                        for (int i = 1; i < padding; i++) {
                            for (int j = 1; j < padding; j++) {
                                finalisedTex.SetPixel (w - j, h - i, Color.black);
                            }
                        }
                        //Lower Right
                        for (int i = 1; i < padding; i++) {
                            for (int j = 1; j < padding; j++) {
                                finalisedTex.SetPixel (w + j, h - i, Color.black);
                            }
                        }
                    }
                }
            }

            finalisedTex.Apply ();
            genTexture.texture = finalisedTex;
        }

    }

}