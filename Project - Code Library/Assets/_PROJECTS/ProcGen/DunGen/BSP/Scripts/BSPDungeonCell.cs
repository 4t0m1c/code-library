﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.ProcGen {

    public class BSPDungeonCell : MonoBehaviour {

        [SerializeField] UnityEvent disableVisuals;
        [SerializeField] Node node;

        public void DisableVisuals () {
            disableVisuals.Invoke ();
        }

        public void SetNode (Node node) {
            this.node = node;
        }

    }
}