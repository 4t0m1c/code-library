﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.ProcGen {

    public class BSPDungeonGenerator : MonoBehaviour {

        [Header ("Parameters")]
        [SerializeField] Vector2 gridSize;
        [SerializeField] GameObject cellPrefab;
        [SerializeField] Transform cellParent;
        [SerializeField] int stepCount = 4;

        [Header ("Output")]
        [SerializeField] Node rootNode;

        public void Generate () {
            Clear ();

            Debug.Log ($"Generating...");

            GameObject rootCell = Instantiate (cellPrefab, cellParent);
            rootNode = new Node (gridSize / 2, gridSize, null, rootCell);

            rootNode.Subdivide (cellPrefab, cellParent, stepCount, Random.Range (0, 2) == 0 ? true : false);
            rootNode.CreateLeafRoom (cellPrefab, cellParent);
            rootNode.CreatePath (cellPrefab);
        }

        void Clear () {
            if (rootNode != null) {
                rootNode.Clear ();
                rootNode = null;
            }
            if (transform.childCount > 0) {
                for (int i = transform.childCount - 1; i >= 0; i--) {
                    Destroy (transform.GetChild (i).gameObject);
                }
            }
        }

    }

    [System.Serializable]
    public class Node {

        public Vector2 origin;
        public Vector2 size;
        public Node parent;
        public GameObject cellObject;
        public List<Node> children = new List<Node> ();
        public List<Node> connections = new List<Node> ();

        public Node (Vector2 origin, Vector2 size, Node parent, GameObject cellObject) {
            this.origin = origin;
            this.size = size;
            this.parent = parent;
            this.cellObject = cellObject;

            cellObject.transform.localScale = new Vector3 (size.x, 1, size.y);
            cellObject.GetComponent<BSPDungeonCell> ().SetNode (this);
        }

        public void Clear () {
            if (children.Count > 0) {
                for (int i = children.Count - 1; i >= 0; i--) {
                    children[i].Clear ();
                }
            }

            BSPDungeonGenerator.Destroy (cellObject);
        }

        public void Subdivide (GameObject cellPrefab, Transform cellParent, int stepCount, bool vertical) {

            /* 
                16x16
                8x16 8x16
                4    12
             */

            if (stepCount == 0) return;
            stepCount--;

            cellObject.GetComponent<BSPDungeonCell> ().DisableVisuals ();

            //Right/Up ++
            GameObject childCell = BSPDungeonGenerator.Instantiate (cellPrefab, cellParent);

            Vector2 newSize = new Vector2 (vertical ? size.x / 2 : size.x, vertical ? size.y : size.y / 2);
            Vector2 newOrigin = new Vector2 (vertical ? 0.25f : 0, vertical ? 0 : 0.25f);

            Node childNode = new Node (newOrigin, newSize, this, childCell);
            childCell.transform.SetParent (cellObject.transform, true);
            childCell.transform.localPosition = new Vector3 (newOrigin.x, 0, newOrigin.y);

            childNode.Subdivide (cellPrefab, cellParent, stepCount, !vertical);
            children.Add (childNode);

            //Left/Down --
            childCell = BSPDungeonGenerator.Instantiate (cellPrefab, cellParent);

            newSize = new Vector2 (vertical ? size.x / 2 : size.x, vertical ? size.y : size.y / 2);
            newOrigin = new Vector2 (vertical ? -0.25f : 0, vertical ? 0 : -0.25f);

            childNode = new Node (newOrigin, newSize, this, childCell);
            childCell.transform.SetParent (cellObject.transform, true);
            childCell.transform.localPosition = new Vector3 (newOrigin.x, 0, newOrigin.y);

            childNode.Subdivide (cellPrefab, cellParent, stepCount, !vertical);
            children.Add (childNode);
        }

        public void CreateLeafRoom (GameObject cellPrefab, Transform cellParent) {
            if (children.Count > 0) {
                for (int i = children.Count - 1; i >= 0; i--) {
                    children[i].CreateLeafRoom (cellPrefab, cellParent);
                }
            } else {
                //Found leaf, making room
                cellObject.GetComponent<BSPDungeonCell> ().DisableVisuals ();

                if (Random.Range (0, 100) < 45) {
                    parent.children.Remove (this);
                    Clear ();

                    Node parentNode = parent;
                    while (parentNode != null) {
                        if (parentNode.children.Count == 0) {
                            parentNode.parent.children.Remove (parentNode);
                            parentNode.Clear ();

                            parentNode = parentNode.parent;
                        } else {
                            parentNode = null;
                        }
                    }
                    
                    return; //Invalidate room
                }

                GameObject childCell = BSPDungeonGenerator.Instantiate (cellPrefab, cellParent);

                Vector2 newSize = new Vector2 (size.x * Random.Range (0.30f, 0.80f), size.y * Random.Range (0.30f, 0.80f));
                Vector2 possibleRandomOffsets = new Vector2 (1 - newSize.x, 1 - newSize.y);
                if (Random.Range (0, 2) == 0) possibleRandomOffsets.x *= -1;
                if (Random.Range (0, 2) == 0) possibleRandomOffsets.y *= -1;
                Vector2 newOrigin = new Vector2 ((Random.Range (0.20f, 0.40f) * possibleRandomOffsets.x), (Random.Range (0.20f, 0.40f) * possibleRandomOffsets.y));

                Node childNode = new Node (newOrigin, newSize, this, childCell);
                childCell.transform.SetParent (cellObject.transform, true);
                childCell.transform.localPosition = new Vector3 (newOrigin.x, 0, newOrigin.y);
            }
        }

        public void CreatePath (GameObject cellPrefab) {
            if (children.Count > 0) {
                for (int i = children.Count - 1; i >= 0; i--) {
                    children[i].CreatePath (cellPrefab);
                }
            } else {

                Node targetRoom = null;
                
                //Find siblings
                for (int i = 0; i < parent.children.Count; i++) {
                    if (parent.children[i] != this) {
                        if (!parent.children[i].connections.Contains (this) && !connections.Contains (parent.children[i])) {
                            targetRoom = parent.children[i];
                        }
                    }
                }

                if (targetRoom != null) {
                    targetRoom.connections.Add (this);
                    connections.Add (targetRoom);

                    GameObject childPath = BSPDungeonGenerator.Instantiate (cellPrefab, cellObject.transform.root);
                    Vector3 distance = cellObject.transform.position - targetRoom.cellObject.transform.position;
                    childPath.transform.position = (cellObject.transform.position + targetRoom.cellObject.transform.position) / 2; //average position
                    distance = new Vector3 (Mathf.Abs (distance.x), 1, Mathf.Abs (distance.z));
                    bool vertical = distance.x > distance.z;
                    if (!vertical) childPath.transform.Rotate (Vector3.up * 90);
                    childPath.transform.localScale = new Vector3 (vertical ? distance.x : distance.z, 1, 0.2f);
                }

            }
        }

    }

}