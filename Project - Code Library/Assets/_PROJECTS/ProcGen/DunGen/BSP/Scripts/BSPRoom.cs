﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BSPRoom : MonoBehaviour {

    [SerializeField] UnityEvent OnDisableVisual = new UnityEvent ();

    public void DisableVisual () {
        OnDisableVisual.Invoke ();
    }
}