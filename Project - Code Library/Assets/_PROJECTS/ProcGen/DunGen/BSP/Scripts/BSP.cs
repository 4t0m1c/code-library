﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSP : MonoBehaviour {

    [SerializeField] GameObject roomPrefab;
    [SerializeField] BSPRoom roomParent;
    [SerializeField] Vector2Int gridSize;
    [SerializeField] Vector2 roomMinSize;
    [SerializeField] int generationDepth = 3;

    [SerializeField] Partition rootPartition;

    [SerializeField] List<BSPRoom> rooms = new List<BSPRoom> ();
    [SerializeField] List<BSPRoom> finalRooms = new List<BSPRoom> ();

    [ContextMenu ("Generate")]
    void Generate () {
        ClearRooms ();

        transform.position = new Vector3 (-gridSize.x / 2, 0, -gridSize.y / 2);

        rootPartition = new Partition (new Rect (0, 0, gridSize.x, gridSize.y));
        rootPartition.room = roomParent;
        Partition currentPartition = rootPartition;

        List<Partition> childPartitions = new List<Partition> ();
        List<Partition> nextPartitions = new List<Partition> ();
        finalRooms = new List<BSPRoom> ();

        childPartitions.Add (currentPartition);
        bool splitVertical = Random.Range (0, 2) == 0 ? true : false;

        for (var i = 0; i < generationDepth; i++) {
            for (var x = 0; x < childPartitions.Count; x++) {
                currentPartition = childPartitions[x];
                Rect a_ = new Rect ();
                Rect b_ = new Rect ();
                float splitPosition = 0;

                if (splitVertical) {
                    splitPosition = (float) currentPartition.partitionSize.width / 2 + Random.Range (-(float) currentPartition.partitionSize.width / 4, (float) currentPartition.partitionSize.width / 4);
                    SplitVertically (currentPartition, splitPosition, out a_, out b_);
                } else {
                    splitPosition = (float) currentPartition.partitionSize.height / 2 + Random.Range (-(float) currentPartition.partitionSize.height / 4, (float) currentPartition.partitionSize.height / 4);
                    SplitHorizontally (currentPartition, splitPosition, out a_, out b_);
                }

                Partition a_Partition = new Partition (a_);
                a_Partition.parentPartition = currentPartition;
                currentPartition.childPartitions.Add (a_Partition);
                a_Partition.room = SpawnRoom (a_, currentPartition.room);

                Partition b_Partition = new Partition (b_);
                b_Partition.parentPartition = currentPartition;
                currentPartition.childPartitions.Add (b_Partition);
                b_Partition.room = SpawnRoom (b_, currentPartition.room);

                //Add removes for further subdivision
                if (a_Partition.partitionSize.width >= roomMinSize.x && a_Partition.partitionSize.height >= roomMinSize.y && i < generationDepth - 1) {
                    nextPartitions.Add (a_Partition);
                } else {
                    finalRooms.Add (a_Partition.room);
                }

                if (b_Partition.partitionSize.width >= roomMinSize.x && b_Partition.partitionSize.height >= roomMinSize.y && i < generationDepth - 1) {
                    nextPartitions.Add (b_Partition);
                } else {
                    finalRooms.Add (b_Partition.room);
                }

                currentPartition.room.DisableVisual ();
                splitVertical = !splitVertical;
            }

            childPartitions = nextPartitions;
            nextPartitions = new List<Partition> ();
        }
    }

    void ClearRooms () {
        if (rooms.Count > 0) {
            for (var i = rooms.Count - 1; i >= 0; i--) {
                DestroyImmediate (rooms[i]?.gameObject);
            }
        }
        rooms.Clear ();
    }

    BSPRoom SpawnRoom (Rect rect, BSPRoom room) {
        BSPRoom newRoom = Instantiate (roomPrefab).GetComponent<BSPRoom> ();
        newRoom.transform.localScale = new Vector3 (rect.width, 1, rect.height);
        newRoom.transform.position = new Vector3 (rect.x, 0, rect.y) + roomParent.transform.position;
        newRoom.transform.parent = room.transform;
        rooms.Add (newRoom);

        return newRoom;
    }

    void SplitVertically (Partition currentPartition, float splitPosition, out Rect left, out Rect right) {
        left = new Rect (currentPartition.partitionSize.x,
            currentPartition.partitionSize.y,
            splitPosition,
            currentPartition.partitionSize.height);

        right = new Rect (currentPartition.partitionSize.x + splitPosition,
            currentPartition.partitionSize.y,
            currentPartition.partitionSize.width - splitPosition,
            currentPartition.partitionSize.height);
    }

    void SplitHorizontally (Partition currentPartition, float splitPosition, out Rect bottom, out Rect top) {
        bottom = new Rect (currentPartition.partitionSize.x,
            currentPartition.partitionSize.y,
            currentPartition.partitionSize.width,
            splitPosition);

        top = new Rect (currentPartition.partitionSize.x,
            currentPartition.partitionSize.y + splitPosition,
            currentPartition.partitionSize.width,
            currentPartition.partitionSize.height - splitPosition);
    }

}

[System.Serializable]
public class Partition {
    public Rect partitionSize;
    public Partition parentPartition;
    public BSPRoom room;
    public List<Partition> childPartitions = new List<Partition> ();

    public Partition (Rect partitionSize) {
        this.partitionSize = partitionSize;
    }
}