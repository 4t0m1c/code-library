﻿using System.Collections;
using System.Collections.Generic;
using Noise;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class NoiseGen : MonoBehaviour {

	enum NoiseType {
		Random,
		Perlin,
		Simplex,
		Cellular
	}

	[Header ("References")]
	[SerializeField] RawImage genRen;
	[SerializeField] Transform blockGrassParent, blockDirtParent;
	[SerializeField] ObjectPoolVoxelBlock blockGrassPool, blockDirtPool;

	List<VoxelBlock> grassBlocks = new List<VoxelBlock> ();
	List<VoxelBlock> dirtBlocks = new List<VoxelBlock> ();

	[Header ("Settings")]
	[SerializeField] NoiseType noiseType;
	[SerializeField] int seed;
	[SerializeField] Vector2Int textureSize;
	[SerializeField] Vector2Int terrainSize;
	[SerializeField] float noiseScale = 50;
	[SerializeField] float maxHeight = 5;
	[SerializeField] bool snapHeights = false;
	[SerializeField] bool overTime = true;

	OpenSimplexNoise simplexNoise;

	public void Generate () {
		GenerateTexture ();
	}

	public void GenerateRandom () {
		//Set Seed
		seed = UnityEngine.Random.Range (0, 999);

		Generate ();
	}

	void GenerateTexture () {

		Texture2D tex = new Texture2D (textureSize.x, textureSize.y);

		System.Random rng = new System.Random (seed);
		float offset = (float) rng.NextDouble () * rng.Next (100);

		simplexNoise = new OpenSimplexNoise (seed);

		for (int row = 0; row < textureSize.y; row++) {
			for (int col = 0; col < textureSize.x; col++) {
				float pixelValue = 0;
				float xCoord = offset + ((float) col / (float) textureSize.x) * noiseScale;
				float yCoord = offset + ((float) row / (float) textureSize.y) * noiseScale;

				if (noiseType == NoiseType.Random) {
					pixelValue = (float) rng.NextDouble ();
				} else if (noiseType == NoiseType.Perlin) {
					pixelValue = Mathf.PerlinNoise (xCoord, yCoord);
				} else if (noiseType == NoiseType.Simplex) {
					pixelValue = (float) simplexNoise.Evaluate (System.Convert.ToDouble (xCoord), System.Convert.ToDouble (yCoord));
				} else if (noiseType == NoiseType.Cellular) {
					pixelValue = (float) noise.snoise (new float2 (xCoord, yCoord));
				}

				tex.SetPixel (col, row, new Color (pixelValue, pixelValue, pixelValue));
			}
		}

		tex.Apply ();
		genRen.texture = tex;

		StartCoroutine (GenerateVoxels (terrainSize, textureSize, tex));
	}

	IEnumerator GenerateVoxels (Vector2Int terrainSize, Vector2Int textureSize, Texture2D tex) {
		ReturnAllExistingBlocks ();
		CenterVoxels ();

		for (int row = 0; row < terrainSize.y; row++) {
			for (int col = 0; col < terrainSize.x; col++) {

				GenerateVoxel (row, col, tex);

				if (overTime) yield return new WaitForSeconds (terrainSize.x / (1000 * 10));
			}
		}
	}

	void CenterVoxels () {
		transform.position = new Vector3 (-terrainSize.x / 2, 0, -terrainSize.y / 2);
	}

	void GenerateVoxel (int row, int col, Texture2D tex) {
		int xCoord = Mathf.FloorToInt (((float) col * (float) textureSize.x) / (float) terrainSize.x);
		int yCoord = Mathf.FloorToInt (((float) row * (float) textureSize.y) / (float) terrainSize.y);

		float pixelValue = tex.GetPixel (xCoord, yCoord).r;

		VoxelBlock newBlock = blockGrassPool.GetFromObjectPool ();
		newBlock.transform.SetParent (blockGrassParent);
		grassBlocks.Add (newBlock);

		float blockHeight = pixelValue * maxHeight;
		if (snapHeights) {
			blockHeight = Mathf.Round (blockHeight);

			//Spawn dirt blocks
			for (int i = 0; i < Mathf.Round (pixelValue * maxHeight); i++) {
				VoxelBlock newDirt = blockDirtPool.GetFromObjectPool ();
				newDirt.transform.SetParent (blockDirtParent);
				dirtBlocks.Add (newDirt);

				newDirt.transform.localPosition = new Vector3 (col, i, row);
			}
		}

		newBlock.transform.localPosition = new Vector3 (col, blockHeight, row);
	}

	void ReturnAllExistingBlocks () {
		//Return grass children
		if (grassBlocks.Count > 0) {
			for (int i = grassBlocks.Count - 1; i >= 0; i--) {
				blockGrassPool.PutIntoObjectPool (grassBlocks[i]);
				grassBlocks.RemoveAt (i);
			}
		}

		//Return dirt children
		if (dirtBlocks.Count > 0) {
			for (int i = dirtBlocks.Count - 1; i >= 0; i--) {
				blockDirtPool.PutIntoObjectPool (dirtBlocks[i]);
				dirtBlocks.RemoveAt (i);
			}
		}
	}

}