using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockType : MonoBehaviour {

    public void SetBlockType (BlockTypeObject blockTypeObject) {
        GameObject blockPrefab = Instantiate (blockTypeObject.blockPrefab, transform);
        
        if (blockTypeObject.blockType == BlockTypes.Block) {
            //Set colour
            MeshRenderer meshRenderer = blockPrefab.GetComponent<MeshRenderer> ();
            Material mat = meshRenderer.material;
            mat.color = blockTypeObject.blockColour;
        } else {
            //Probably a tree
        }
    }

}