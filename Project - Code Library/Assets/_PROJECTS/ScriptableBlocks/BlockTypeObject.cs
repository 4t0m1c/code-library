using UnityEngine;

public enum BlockTypes {
    Block, Custom
}

[CreateAssetMenu(fileName = "BlockTypeObject", menuName = "Project - Code Library/BlockTypeObject", order = 0)]
public class BlockTypeObject : ScriptableObject {
    
    public BlockTypes blockType;
    public GameObject blockPrefab;
    public Color blockColour;

}