using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour {

    [SerializeField] BlockType block;
    [SerializeField] GameObject blockPrefab;
    [SerializeField] List<BlockTypeObject> blockTypes = new List<BlockTypeObject> ();

    [EasyButtons.Button]
    void RandomiseBlock () {
        block = Instantiate (blockPrefab, new Vector3 (Random.Range (-5, 5), 0, Random.Range (-5, 5)), Quaternion.identity).GetComponent<BlockType> ();
        block.SetBlockType (blockTypes[Random.Range (0, blockTypes.Count)]);
    }

}