﻿using System.Collections;
using System.Collections.Generic;
using CodingConcepts;
using UnityEngine;

[CreateAssetMenu (fileName = "ActionJump", menuName = "Project - Code Library/Action - Jump", order = 0)]
public class ActionJump : Action {

    public float jumprForce = 3;

    public override bool PerformAction () {
        gameObject.GetComponent<Rigidbody> ().AddForce (Vector3.up * jumprForce, ForceMode.Impulse);
        return true;
    }

}