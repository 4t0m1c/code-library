﻿using System.Collections;
using System.Collections.Generic;
using CodingConcepts;
using UnityEngine;

[CreateAssetMenu (fileName = "ActionMove", menuName = "Project - Code Library/Action - Move", order = 0)]
public class ActionMove : Action {
    public Vector3 moveDirection;

    public override bool PerformAction () {
        Debug.Log ("Action - Move");
        return true;
    }

    public void MoveFast () {
        Debug.Log ("Action - Move Fast");
        gameObject.transform.Translate(moveDirection);
    }
}