﻿using System.Collections;
using System.Collections.Generic;
using CodingConcepts;
using UnityEngine;

public class MyActions : MonoBehaviour {
    [SerializeField] List<Action> actions = new List<Action> ();

    void Start () {
        foreach (var _action in actions) {
            _action.gameObject = gameObject;
            _action.PerformAction ();
        }
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.Space)) {
            actions.GetAction<ActionJump> ().PerformAction ();
        }
        if (Input.GetKeyDown (KeyCode.W)) {
            actions.GetAction<ActionMove> ().MoveFast ();
        }
    }
}