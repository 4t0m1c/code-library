﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CodingConcepts {

    public class Action : ScriptableObject {
        [HideInInspector] public GameObject gameObject;

        //Marked as virtual so that it can be overridden by child classes for individual implementation
        public virtual bool PerformAction () {
            Debug.Log ("Action not implemented");
            return false;
        }

    }

    //Extension class
    public static class ActionExtensions {

        //Targets a particular signature - List<Action>
        public static T GetAction<T> (this List<Action> actions) {
            foreach (var action in actions) {
                //Find the action with a matching type
                if (action is T) {
                    //Return the action in the childclass type and not Action
                    return (T) Convert.ChangeType (action, typeof (T));;
                }
            }
            //Nothing was found
            return default (T);
        }

    }
}