﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

namespace At0m1c.JobSystem {
    public class PerlinJob : MonoBehaviour {

        [Header ("Settings")]
        [SerializeField] float gridSize = 64;
        [SerializeField] float waveSpeed = 1;
        [SerializeField] float resolution = 10;
        [SerializeField] float height = 5;
        [SerializeField] GameObject gridBlockPrefab;

        List<Transform> gridBlocks = new List<Transform> ();
        TransformAccessArray gridBlocksArray = new TransformAccessArray ();

        bool playing = false;
        bool useJobs = false;

        void Awake () {
            //Unlock the frame rate from 60
            //VSync should also be disabled in the Quality settings to get an uncapped FPS
            Application.targetFrameRate = 0;
        }

        void Update () {
            if (playing) {
                if (useJobs) {
                    MoveAllBlocksJob ();
                } else {
                    MoveAllBlocks ();
                }
            }
        }

        //Standard approach to manipulating all block positions
        void MoveAllBlocks () {
            for (int i = 0; i < gridBlocks.Count; i++) {
                Vector3 pos = gridBlocks[i].position;
                pos.y = GetPerlinY (pos.x, pos.z, waveSpeed, gridSize, resolution, height, Time.timeSinceLevelLoad);
                gridBlocks[i].position = pos;
            }
        }

        //Job system approach to manipulating all block positions over multiple threads
        void MoveAllBlocksJob () {
            MoveBlocksPerlin job = new MoveBlocksPerlin () {
                waveSpeed = waveSpeed,
                gridSize = gridSize,
                resolution = resolution,
                height = height,
                time = Time.timeSinceLevelLoad
            };

            JobHandle handle = job.Schedule (gridBlocksArray);
            handle.Complete ();
        }

        void OnDestroy () {
            gridBlocksArray.Dispose ();
        }

        // ==============
        // UI Functions
        // ==============

        public void GenerateAllBlocks () {
            for (int x = 0; x < gridSize; x++) {
                for (int y = 0; y < gridSize; y++) {
                    gridBlocks.Add (Instantiate (gridBlockPrefab, new Vector3 (x, 0, y), Quaternion.identity).transform);
                }
            }

            gridBlocksArray = new TransformAccessArray (gridBlocks.ToArray ());
        }

        public void Play () {
            playing = true;
        }

        public void Stop () {
            playing = false;
        }

        public void UseJobs (bool use) {
            useJobs = use;
        }

        // ==============
        // Static Methods
        // ==============

        public static float GetPerlinY (float x, float y, float waveSpeed, float gridSize, float resolution, float height, float time) {
            //Primary waves sampled by position and varied by time to offset the waves
            float pX = ((x / gridSize) * resolution) + (time * waveSpeed);
            float pZ = ((y / gridSize) * resolution) + (time * waveSpeed);
            float p1 = Mathf.PerlinNoise (pX, pZ) * height;

            //Secondary waves for realism/variation
            float pX2 = ((x / gridSize) * resolution / 2) + (3 * waveSpeed);
            float pZ2 = ((y / gridSize) * resolution / 2) + (3 * waveSpeed);
            float p2 = Mathf.PerlinNoise (pX2, pZ2) * height;

            return p1 + p2;
        }

    }

    public struct MoveBlocksPerlin : IJobParallelForTransform {
        [ReadOnly]
        public float waveSpeed;
        [ReadOnly]
        public float gridSize;
        [ReadOnly]
        public float resolution;
        [ReadOnly]
        public float height;
        [ReadOnly]
        public float time;

        public void Execute (int i, TransformAccess transform) {
            Vector3 pos = transform.position;
            pos.y = PerlinJob.GetPerlinY (transform.position.x, transform.position.z, waveSpeed, gridSize, resolution, height, time);
            transform.position = pos;
        }
    }
}