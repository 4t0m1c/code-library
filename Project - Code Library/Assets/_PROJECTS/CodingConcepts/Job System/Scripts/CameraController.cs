﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour {

    [SerializeField] InputActionMap actionMap;
    [SerializeField] float moveSpeed = 10;
    [SerializeField] float lookSpeed = 2;
    [SerializeField] float scrollSpeed = 10;

    Vector2 moveVector;
    Vector2 rotateVector;
    bool rotating = false;
    Transform rotateRoot;

    void Awake () {
        rotateRoot = transform.GetChild (0);

        actionMap.Enable ();
        actionMap.FindAction ("Scroll").performed += x => Scroll (x.ReadValue<float> ());
        actionMap.FindAction ("Move").performed += x => moveVector = x.ReadValue<Vector2> ();
        actionMap.FindAction ("RotateDelta").performed += x => rotateVector = x.ReadValue<Vector2> ();
        actionMap.FindAction ("RotateActivate").performed += x => rotating = true;
        actionMap.FindAction ("RotateActivate").canceled += x => rotating = false;
    }

    void Update () {
        transform.Translate (new Vector3 (moveVector.x * transform.localScale.x, 0, moveVector.y * transform.localScale.x) * Time.deltaTime * moveSpeed, Space.Self);
        if (rotating) {
            transform.Rotate (new Vector3 (0, rotateVector.x, 0) * Time.deltaTime * lookSpeed, Space.Self);
            rotateRoot.Rotate (new Vector3 (-rotateVector.y, 0, 0) * Time.deltaTime * lookSpeed, Space.Self);
        }
    }

    void Scroll (float _scrollValue) {
        _scrollValue = Mathf.Clamp (_scrollValue, -1f, 1f) * Time.deltaTime * scrollSpeed;
        Vector3 scale = transform.localScale;
        float scaleClamped = Mathf.Clamp (scale.x - _scrollValue, 1f, 100f);
        scale = new Vector3 (scaleClamped, scaleClamped, scaleClamped);
        transform.localScale = scale;
    }
}