﻿using UnityEngine;

namespace CodingConcepts {
    
    [System.Serializable]
    public struct Vector3X {

        //Constructor
        //Used to create a new instance of the type with the parameters given
        public Vector3X (float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        //Properties
        public float x;
        public float y;
        public float z;

        //Static Properties

        public static Vector3X forward () {
            return new Vector3X (0, 0, 1);
        }
        public static Vector3X right () {
            return new Vector3X (1, 0, 0);
        }
        public static Vector3X up () {
            return new Vector3X (0, 1, 0);
        }

        //Operators
        //+ - * / != ==
        public static bool operator == (Vector3X lhs, Vector3X rhs) {
            if (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z) {
                return true;
            }
            return false;
        }

        public static bool operator != (Vector3X lhs, Vector3X rhs) {
            if (lhs.x != rhs.x || lhs.y != rhs.y || lhs.z != rhs.z) {
                return true;
            }
            return false;
        }

    }

    //Extension Methods
    public static class Vector3XExtensions {

        /// <summary>
        /// Rounds x,y,z of the Vector3X to the nearest number.
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector3X Round (this Vector3X vector) {
            return new Vector3X (Mathf.Round (vector.x), Mathf.Round (vector.y), Mathf.Round (vector.z));
        }

    }

}