﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Collectables {
    public class CollectableEnergy : MonoBehaviour, ICollectable {
        public void Collect () {
            Debug.Log ("Energy collected.. Adding to stats..");
            //Implement logic for adding energy to player
        }

    }
}