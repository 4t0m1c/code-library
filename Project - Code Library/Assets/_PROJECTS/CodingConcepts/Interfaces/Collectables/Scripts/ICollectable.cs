﻿namespace At0m1c.Collectables {
    public interface ICollectable {
        void Collect ();
    }
}