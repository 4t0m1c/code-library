﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace At0m1c.Collectables {
    public class CharacterController : MonoBehaviour {
        [SerializeField] InputActionMap characterActions;
        [SerializeField] LayerMask collectMask;

        void Start () {
            characterActions.Enable ();

            characterActions.FindAction ("Move").performed += x => Move (x.ReadValue<Vector2> ());
            characterActions.FindAction ("Collect").performed += x => Collect ();
        }

        void Move (Vector2 moveVector) {
            Debug.Log ("Moving");
            transform.Translate (new Vector3 (moveVector.x, 0, moveVector.y));
        }

        void Collect () {
            Debug.Log ("Collecting..");
            Collider[] cols = Physics.OverlapSphere (transform.position, 2, collectMask);

            foreach (var col in cols) {
                ICollectable _collectable = col.GetComponent<ICollectable> ();
                if (_collectable != null) {
                    _collectable.Collect ();
                }
            }
        }
    }
}