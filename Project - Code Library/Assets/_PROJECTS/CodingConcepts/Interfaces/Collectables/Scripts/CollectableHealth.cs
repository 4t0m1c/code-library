﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Collectables {
    public class CollectableHealth : MonoBehaviour, ICollectable {
        public void Collect () {
            Debug.Log ("Health collected.. Adding to stats..");
            //Implement logic for adding health to player
        }
    }
}