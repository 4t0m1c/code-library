﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Collectables {

    public class CollectableItem : MonoBehaviour, ICollectable {
        public void Collect () {
            Debug.Log ("Item collected.. Adding to inventory..");
            //Implement logic for adding item to inventory
        }
    }
}