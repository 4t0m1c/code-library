﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootFinder : MonoBehaviour {
    List<ILootable> lootables = new List<ILootable> ();
    int gold = 0;

    void OnTriggerEnter (Collider other) {
        ILootable lootable = other.GetComponent<ILootable> ();
        if (lootable != null) {
            lootables.Add (lootable);
        }
    }

    void OnTriggerExit (Collider other) {
        ILootable lootable = other.GetComponent<ILootable> ();
        if (lootable != null) {
            lootables.Remove (lootable);
        }
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.E)) {
            lootables.ForEach (x => gold += x.Loot ());
            lootables.Clear ();
        }
    }
}