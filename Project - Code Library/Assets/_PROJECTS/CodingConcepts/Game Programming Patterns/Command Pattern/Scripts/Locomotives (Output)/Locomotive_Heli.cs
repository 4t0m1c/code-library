﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace At0m1c.CommandPattern {
    public class Locomotive_Heli : MonoBehaviour, ILocomotive {

        Rigidbody rb;
        [SerializeField] Transform helirotars;
        bool inVehichle = false;

        void Awake () {
            rb = GetComponent<Rigidbody> ();
        }

        public LocomotionMode GetLocomotionMode () {
            return new Locomotion_Fly (rb);
        }

        void Update () {
            if (inVehichle)
                helirotars.Rotate (Vector3.up * 10);
        }

        public void EnterLocomotive (Rigidbody playerRigidbody) {
            rb.useGravity = false;
            inVehichle = true;
            rb.MovePosition (rb.position + (Vector3.up * 5));
            playerRigidbody.gameObject.GetComponentsInChildren<Collider> ().ToList ().ForEach (x => x.enabled = false);
            playerRigidbody.isKinematic = true;
            playerRigidbody.transform.position = transform.position;
            playerRigidbody.transform.parent = transform;
        }

        public void ExitLocomotive (Rigidbody playerRigidbody) {
            rb.useGravity = true;
            inVehichle = false;
            playerRigidbody.transform.parent = null;
            playerRigidbody.transform.position = transform.position + (transform.right * 1.5f);
            playerRigidbody.transform.localEulerAngles = Vector3.zero;
            playerRigidbody.gameObject.GetComponentsInChildren<Collider> ().ToList ().ForEach (x => x.enabled = true);
            playerRigidbody.isKinematic = false;
        }
    }
}