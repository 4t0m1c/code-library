﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace At0m1c.CommandPattern {
    public class Locomotive_Car : MonoBehaviour, ILocomotive {

        Rigidbody rb;

        void Awake () {
            rb = GetComponent<Rigidbody> ();
        }

        public LocomotionMode GetLocomotionMode () {
            return new Locomotion_Drive (rb);
        }

        public void EnterLocomotive (Rigidbody playerRigidbody) {
            playerRigidbody.gameObject.GetComponentsInChildren<Collider> ().ToList ().ForEach (x => x.enabled = false);
            playerRigidbody.isKinematic = true;
            playerRigidbody.transform.position = transform.position;
            playerRigidbody.transform.parent = transform;
        }

        public void ExitLocomotive (Rigidbody playerRigidbody) {
            playerRigidbody.transform.parent = null;
            playerRigidbody.transform.position = transform.position + (transform.right * 1.5f);
            playerRigidbody.transform.localEulerAngles = Vector3.zero;
            playerRigidbody.gameObject.GetComponentsInChildren<Collider> ().ToList ().ForEach (x => x.enabled = true);
            playerRigidbody.isKinematic = false;
        }
    }
}