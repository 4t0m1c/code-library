﻿using UnityEngine;

namespace At0m1c.CommandPattern {
    public interface ILocomotive {
        LocomotionMode GetLocomotionMode ();
        void EnterLocomotive (Rigidbody playerRigidbody);
        void ExitLocomotive (Rigidbody playerRigidbody);
    }
}