﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.CommandPattern {
    public abstract class LocomotionMode {
        //Drive | Fly | Walk

        protected float moveSpeed = 10;
        public Vector2 moveVector;
        public Rigidbody rb;

        protected LocomotionMode (Rigidbody targetRB) {
            this.rb = targetRB;
        }

        public abstract void Move (Vector2 moveVector);
        public virtual void Start () { }
        public virtual void FixedUpdate () { }
        public virtual void Update () { }
    }
}