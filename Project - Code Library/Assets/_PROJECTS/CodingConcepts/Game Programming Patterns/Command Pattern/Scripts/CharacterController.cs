﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace At0m1c.CommandPattern {
    public class CharacterController : MonoBehaviour {
        [SerializeField] InputActionMap characterActions;
        [SerializeField] LayerMask interactMask;

        LocomotionMode activeLocomotionMode;
        ILocomotive activeLocomotive;
        
        Rigidbody rb;

        void Awake () {
            rb = GetComponent<Rigidbody> ();
            activeLocomotionMode = new Locomotion_Walk (rb);
        }

        void Start () {
            characterActions.Enable ();

            characterActions.FindAction ("Move").performed += x => activeLocomotionMode.moveVector = x.ReadValue<Vector2> ();
            characterActions.FindAction ("Move").canceled += x => activeLocomotionMode.moveVector = Vector2.zero;

            characterActions.FindAction ("Interact").performed += x => {
                if (activeLocomotive == null)
                    Interact ();
                else
                    ExitLocomotive ();
            };
        }

        [ContextMenu("Test")]
        void FixedUpdate () {
            Debug.Log("Test");
            activeLocomotionMode.FixedUpdate ();
        }

        void Interact () {
            Debug.Log ("Interacting..");
            Collider[] cols = Physics.OverlapSphere (transform.position, 2, interactMask);

            ILocomotive closestLocomotive = null;
            float closestDist = 999;
            foreach (var col in cols) {
                ILocomotive loco = col.attachedRigidbody.GetComponent<ILocomotive> ();
                if (loco != null) {
                    if (Vector3.Distance (transform.position, col.attachedRigidbody.transform.position) < closestDist) {
                        closestLocomotive = loco;
                    }
                }
            }

            if (closestLocomotive != null) {
                activeLocomotionMode = closestLocomotive.GetLocomotionMode ();
                closestLocomotive.EnterLocomotive (rb);
                activeLocomotive = closestLocomotive;
            }
        }

        void ExitLocomotive () {
            activeLocomotive.ExitLocomotive (rb);
            activeLocomotionMode = new Locomotion_Walk (rb);
            activeLocomotive = null;
        }
    }
}