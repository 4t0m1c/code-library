﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.CommandPattern {
    public class Locomotion_Roll : LocomotionMode {
        public Locomotion_Roll (Rigidbody targetRB) : base (targetRB) {
            moveSpeed = 15;
        }

        public override void Move (Vector2 moveVector) {
            this.moveVector = moveVector;
        }

        public override void FixedUpdate () {
            rb.angularVelocity = new Vector3 (moveVector.y * moveSpeed, 0, -moveVector.x * moveSpeed);
        }
    }
}