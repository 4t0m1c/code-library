﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.CommandPattern {
    public class Locomotion_Drive : LocomotionMode {
        public Locomotion_Drive (Rigidbody targetRB) : base (targetRB) {
            moveSpeed = 10;
        }

        public override void Move (Vector2 moveVector) {
            this.moveVector = moveVector;
        }

        public override void FixedUpdate () {
            Vector3 _velocity = rb.transform.forward * moveVector.y * moveSpeed;
            _velocity.y = rb.velocity.y;
            rb.velocity = _velocity;
            rb.angularVelocity = rb.transform.InverseTransformDirection (new Vector3 (0, moveVector.x * moveSpeed, 0));
        }
    }
}