﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.CommandPattern {
    public class Locomotion_Walk : LocomotionMode {
        public Locomotion_Walk (Rigidbody targetRB) : base (targetRB) {
            moveSpeed = 5;
        }

        public override void Move (Vector2 moveVector) {
            this.moveVector = moveVector;
        }

        public override void FixedUpdate () {
            rb.velocity = new Vector3 (moveVector.x * moveSpeed, rb.velocity.y, moveVector.y * moveSpeed);
        }
    }
}