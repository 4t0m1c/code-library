using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace At0m1c.Abstract {
    public class InputManager : MonoBehaviour {

        public static UnityEvent<bool> OnCycleToolNext = new UnityEvent<bool> ();
        public static UnityEvent OnUseToolStarted = new UnityEvent ();
        public static UnityEvent OnUseToolCanceled = new UnityEvent ();

        PlayerActions playerActions;

        void Start () {
            playerActions = new PlayerActions ();
            playerActions.Enable ();

            playerActions.Game.CycleTool.performed += CycleTool;

            playerActions.Game.UseTool.started += x => OnUseToolStarted.Invoke ();
            playerActions.Game.UseTool.canceled += x => OnUseToolCanceled.Invoke ();
        }

        void OnDestroy () {
            playerActions.Dispose ();
        }

        void CycleTool (InputAction.CallbackContext ctx) {
            float value = ctx.ReadValue<float> ();
            if (value != 0) {
                if (value > 0) {
                    OnCycleToolNext.Invoke (false);
                } else {
                    OnCycleToolNext.Invoke (true);
                }
            }
        }
    }
}