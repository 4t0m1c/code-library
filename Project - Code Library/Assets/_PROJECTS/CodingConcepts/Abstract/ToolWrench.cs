using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Abstract {
    public class ToolWrench : ToolBase {

        public override void EquipTool () {
            base.EquipTool ();
            //Equip the tool feedback
        }

        public override void UseTool () {
            //Use the tool
            Debug.Log($"Using {this.name}");
        }

    }
}