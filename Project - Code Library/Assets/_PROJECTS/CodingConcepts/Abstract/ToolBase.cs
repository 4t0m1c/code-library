using UnityEngine;

namespace At0m1c.Abstract {
    public abstract class ToolBase : MonoBehaviour {
        public bool equipped = false;
        public float useTime = 1;

        public virtual void EquipTool () {
            equipped = true;
            Debug.Log ($"Equipping {this.name}");
        }

        public virtual void UnEquipTool () {
            equipped = true;
            Debug.Log ($"Unequipping {this.name}");
        }

        public abstract void UseTool ();
    }
}