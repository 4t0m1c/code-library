using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.Abstract {
    public class ToolBelt : MonoBehaviour {
        [SerializeField] List<ToolBase> tools = new List<ToolBase> ();

        [SerializeField] ToolBase currentTool;

        [SerializeField] bool usingTool = false;

        void Start () {
            if (tools.Count > 0) {
                currentTool = tools[0];
                currentTool.EquipTool ();
            }
        }

        void OnEnable () {
            InputManager.OnCycleToolNext.AddListener (CycleNextWeapon);

            InputManager.OnUseToolStarted.AddListener (UseToolStarted);
            InputManager.OnUseToolCanceled.AddListener (UseToolCanceled);
        }

        void CycleNextWeapon (bool next) {
            if (usingTool) return;

            int currentIndex = tools.IndexOf (currentTool);

            if (next) currentIndex++;
            else currentIndex--;

            if (currentIndex > tools.Count - 1) {
                currentIndex = 0;
            } else if (currentIndex < 0) {
                currentIndex = tools.Count - 1;
            }

            currentTool.UnEquipTool ();
            currentTool = tools[currentIndex];
            currentTool.EquipTool ();
        }

        void UseToolStarted () {
            usingTool = true;
            StartCoroutine (UsingTool ());
        }

        void UseToolCanceled () {
            usingTool = false;
        }

        IEnumerator UsingTool () {
            float waitTime = currentTool.useTime;

            while (usingTool) {
                if (waitTime > 0) {
                    waitTime -= Time.deltaTime;
                } else {
                    waitTime = currentTool.useTime;
                    currentTool.UseTool ();
                }
                yield return null;
            }
        }

    }
}