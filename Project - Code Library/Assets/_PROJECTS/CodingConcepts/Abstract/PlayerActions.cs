// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/CodingConcepts/Abstract/PlayerActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace At0m1c.Abstract
{
    public class @PlayerActions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @PlayerActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerActions"",
    ""maps"": [
        {
            ""name"": ""Game"",
            ""id"": ""5a727f9a-3bdc-4106-a702-c7939c6fd2a1"",
            ""actions"": [
                {
                    ""name"": ""CycleTool"",
                    ""type"": ""PassThrough"",
                    ""id"": ""4e64e46e-6676-4bb0-b9bc-969c345bbc6e"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UseTool"",
                    ""type"": ""Value"",
                    ""id"": ""c8392163-1c26-476d-a045-4092a12832d4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4c72bbb5-4f11-428d-9f36-80e98e77e4f6"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CycleTool"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4de61f33-84de-419d-8e34-fb8400aff5b3"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseTool"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Game
            m_Game = asset.FindActionMap("Game", throwIfNotFound: true);
            m_Game_CycleTool = m_Game.FindAction("CycleTool", throwIfNotFound: true);
            m_Game_UseTool = m_Game.FindAction("UseTool", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Game
        private readonly InputActionMap m_Game;
        private IGameActions m_GameActionsCallbackInterface;
        private readonly InputAction m_Game_CycleTool;
        private readonly InputAction m_Game_UseTool;
        public struct GameActions
        {
            private @PlayerActions m_Wrapper;
            public GameActions(@PlayerActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @CycleTool => m_Wrapper.m_Game_CycleTool;
            public InputAction @UseTool => m_Wrapper.m_Game_UseTool;
            public InputActionMap Get() { return m_Wrapper.m_Game; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(GameActions set) { return set.Get(); }
            public void SetCallbacks(IGameActions instance)
            {
                if (m_Wrapper.m_GameActionsCallbackInterface != null)
                {
                    @CycleTool.started -= m_Wrapper.m_GameActionsCallbackInterface.OnCycleTool;
                    @CycleTool.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnCycleTool;
                    @CycleTool.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnCycleTool;
                    @UseTool.started -= m_Wrapper.m_GameActionsCallbackInterface.OnUseTool;
                    @UseTool.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnUseTool;
                    @UseTool.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnUseTool;
                }
                m_Wrapper.m_GameActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @CycleTool.started += instance.OnCycleTool;
                    @CycleTool.performed += instance.OnCycleTool;
                    @CycleTool.canceled += instance.OnCycleTool;
                    @UseTool.started += instance.OnUseTool;
                    @UseTool.performed += instance.OnUseTool;
                    @UseTool.canceled += instance.OnUseTool;
                }
            }
        }
        public GameActions @Game => new GameActions(this);
        public interface IGameActions
        {
            void OnCycleTool(InputAction.CallbackContext context);
            void OnUseTool(InputAction.CallbackContext context);
        }
    }
}
