﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SaveLoadJSON {

    public class JSONSaveLoad : MonoBehaviour {
        public static JSONSaveLoad instance; //Singleton instance

        //Save Data Structure - You can add any data needed in this class
        [System.Serializable] //Must be serializable to be saved and loaded - Converts C# object to bytes and back
        public class SavedData {
            public List<string> _saveText = new List<string> (); //This is set from other scripts - UITextInput.cs
            //Add more data to be saved - can be any type (color, Vector3, etc.)
        }

        public SavedData saveDataObject; //The instance of our save data. This is the actual object containing the data and conforms to the data structure above.

        void Awake () {
            instance = this;
        }

        public void SaveData () {
            string _path = Path.Combine (Application.persistentDataPath, "saveData.json"); //Path to the file and the filename
            string _data = JsonUtility.ToJson (saveDataObject); //Convert all our data to a JSON formatted string
            File.WriteAllText (_path, _data); //Write the data to file - Limit the number of times this is called to avoid writing delays. You should save all data at particular times or on application quit.

            Debug.Log ("Saved Text: " + _data);
        }

        public void LoadData () {

            string _path = Path.Combine (Application.persistentDataPath, "saveData.json"); //Path to the file and the filename
            string _data = File.ReadAllText (_path); //Read the text from the file
            saveDataObject = JsonUtility.FromJson<SavedData> (_data); //Convert the text from JSON to our SavedData object type

            Debug.Log ("Loaded Text: " + _data);
        }

    }
}