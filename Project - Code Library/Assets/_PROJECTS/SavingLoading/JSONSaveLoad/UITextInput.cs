﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SaveLoadJSON {
    public class UITextInput : MonoBehaviour {

        public InputField textInput;
        public Text textOutput;

        public void AddEntry () {
            JSONSaveLoad.instance.saveDataObject._saveText.Add (textInput.text);
        }

        public void RemoveEntry () {
            JSONSaveLoad.instance.saveDataObject._saveText.RemoveAt (JSONSaveLoad.instance.saveDataObject._saveText.Count - 1);
        }

        public void SaveText () { //Called with UI Button
            JSONSaveLoad.instance.SaveData ();
        }

        public void LoadText () { //Called with UI Button
            JSONSaveLoad.instance.LoadData ();
            textOutput.text = string.Empty;
            foreach (var item in JSONSaveLoad.instance.saveDataObject._saveText) {
                textOutput.text += item + '\n';
            }
        }

    }
}