﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveLoadPlayerPrefs {

    public class PlayerPrefsSaveLoad : MonoBehaviour {
        public static PlayerPrefsSaveLoad instance; //Singleton instance

        //Save Data
        public string _saveText; //This is set from other scripts - UITextInput.cs

        void Awake () {
            instance = this;
        }

        public void SaveData () {
            PlayerPrefs.SetString ("_saveText", _saveText);
            Debug.Log ("Saved Text: " + _saveText);
        }

        public void LoadData () {
            _saveText = PlayerPrefs.GetString ("_saveText");
            Debug.Log ("Loaded Text: " + _saveText);
        }

    }
}