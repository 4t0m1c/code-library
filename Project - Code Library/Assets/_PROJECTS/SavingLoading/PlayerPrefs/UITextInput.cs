﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SaveLoadPlayerPrefs {
    public class UITextInput : MonoBehaviour {

        public InputField textInput;
        public Text textOutput;

        public void SaveText () { //Called with UI Button
            PlayerPrefsSaveLoad.instance._saveText = textInput.text;
            PlayerPrefsSaveLoad.instance.SaveData ();
        }

        public void LoadText () { //Called with UI Button
            PlayerPrefsSaveLoad.instance.LoadData ();
            textOutput.text = PlayerPrefsSaveLoad.instance._saveText;
        }

    }
}