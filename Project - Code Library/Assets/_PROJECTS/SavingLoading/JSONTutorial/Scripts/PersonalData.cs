namespace At0m1c.SaveLoad {
	public enum Pizza {
		Pineapple, //0
		Mushroom, //1
		Cheese, //2
		Salami //3
	}

	[System.Serializable]
	public class PersonalData {
		public string _name = "Test";
		public int _age = 2;
		public string _occupation = "Work";
		public Pizza _pizza = Pizza.Pineapple;
	}
}