﻿using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.SaveLoad {
    public class UISaveLoad : MonoBehaviour {

        [SerializeField] Text nameText;
        [SerializeField] Text ageText;
        [SerializeField] Text occupationText;
        [SerializeField] Text pizzaText;

        [SerializeField] PersonalData currentPersonalData = new PersonalData ();

        public void SaveData () {
            SaveLoadPersonalData.SaveData (currentPersonalData);
            Debug.Log ("Saving");
        }

        public void LoadData () {
            currentPersonalData = SaveLoadPersonalData.LoadData ();
            Debug.Log ("Loading");

            nameText.text = currentPersonalData._name;
            ageText.text = currentPersonalData._age.ToString ();
            occupationText.text = currentPersonalData._occupation;
            pizzaText.text = currentPersonalData._pizza.ToString ();
        }

        public void SetName (string name) {
            currentPersonalData._name = name;
        }
        public void SetAge (string age) {
            currentPersonalData._age = int.Parse (age);
        }
        public void SetOccupation (string occupation) {
            currentPersonalData._occupation = occupation;
        }
        public void SetFavouritePizza (int pizza) {
            currentPersonalData._pizza = (Pizza) pizza;
        }

    }
}