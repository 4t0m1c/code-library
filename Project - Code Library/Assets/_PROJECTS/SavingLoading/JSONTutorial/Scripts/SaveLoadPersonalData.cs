﻿using System.IO;
using UnityEngine;

namespace At0m1c.SaveLoad {

    public class SaveLoadPersonalData {

        public static void SaveData (PersonalData personalDataSave) {
            string _path = Path.Combine (Application.persistentDataPath, "personalDataSave.json");
            string _data = JsonUtility.ToJson (personalDataSave);
            File.WriteAllText (_path, _data);

            Debug.Log ("Saving Data");
        }

        public static PersonalData LoadData () {
            string _path = Path.Combine (Application.persistentDataPath, "personalDataSave.json");
            string _data = File.ReadAllText (_path);
            Debug.Log ("Loading Data");

            return JsonUtility.FromJson<PersonalData> (_data);
        }

    }
}