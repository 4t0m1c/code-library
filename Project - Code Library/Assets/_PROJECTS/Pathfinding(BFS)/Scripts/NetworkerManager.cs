﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace At0m1c.PathfindingBFS.Class {

    [System.Serializable]
    public class Network {
        public List<Node> nodes = new List<Node> ();
    }

    [System.Serializable]
    public class Node {
        public Vector3 position;
        public List<Node> connectedNodes = new List<Node> ();
    }

    public class NetworkerManager : MonoBehaviour {

        [SerializeField] Network _network;

        Dictionary<Node, Node> nodeParents = new Dictionary<Node, Node> ();

        [SerializeField] Transform startQuery, endQuery, resultQuery;

        [ContextMenu ("GetPath")]
        void GetPath () {
            nodeParents.Clear ();
            resultQuery.position = FindShortestPathBFS (GetNodeAtPosition (startQuery.position), GetNodeAtPosition (endQuery.position)).position;
        }

        Node GetNodeAtPosition (Vector3 position) {
            foreach (var node in _network.nodes) {
                if (node.position == position) {
                    return node;
                }
            }
            return default;
        }

        //Breadth first search of graph.
        //Populates List<Node> path with a valid solution to the goalPosition.
        //Returns the goalPosition if a solution is found.
        //Returns the startPosition if no solution is found.
        Node FindShortestPathBFS (Node startPosition, Node goalPosition) {

            uint nodeVisitCount = 0;
            float timeNow = Time.realtimeSinceStartup;

            Queue<Node> queue = new Queue<Node> ();
            HashSet<Node> exploredNodes = new HashSet<Node> ();
            queue.Enqueue (startPosition);

            while (queue.Count != 0) {
                Node currentNode = queue.Dequeue ();
                nodeVisitCount++;

                if (currentNode == goalPosition) {
                    Debug.Log ("BFS time: " + (Time.realtimeSinceStartup - timeNow).ToString ());
                    Debug.Log (string.Format ("BFS visits: {0} ({1:F2}%)", nodeVisitCount, (nodeVisitCount / (double) _network.nodes.Count) * 100));

                    return currentNode;
                }

                List<Node> nodes = GetWalkableNodes (currentNode);

                for (int i = 0; i < nodes.Count; i++) {

                    nodes[i] = GetNodeAtPosition (nodes[i].position);
                    if (!exploredNodes.Contains (nodes[i])) {
                        //Mark the node as explored
                        exploredNodes.Add (nodes[i]);

                        //Store a reference to the previous node
                        nodeParents.Add (nodes[i], currentNode);

                        //Add this to the queue of nodes to examine
                        queue.Enqueue (nodes[i]);
                    }
                }
            }

            return startPosition;
        }

        List<Node> GetWalkableNodes (Node curr) {

            List<Node> walkableNodes = new List<Node> ();

            List<Node> possibleNodes = new List<Node> ();

            curr.connectedNodes.ForEach (x => possibleNodes.Add (x));

            foreach (Node node in possibleNodes) {
                walkableNodes.Add (node);
            }

            Debug.Log ("Found" + walkableNodes.Count + "walkable nodes at " + curr.position);

            return walkableNodes;
        }

        void OnDrawGizmos () {
            foreach (var node in _network.nodes) {
                Gizmos.DrawWireCube (node.position, new Vector3 (0.6f, 0.6f, 0.6f));
                foreach (var connectedNode in node.connectedNodes) {
                    Gizmos.DrawLine (node.position, connectedNode.position);
                }
            }

            Gizmos.color = Color.green;
            for (int i = 0; i < nodeParents.Count - 1; i++) {
                Gizmos.DrawLine (nodeParents.ElementAt (i).Value.position, nodeParents.ElementAt (i + 1).Value.position);
            }
        }
    }
}