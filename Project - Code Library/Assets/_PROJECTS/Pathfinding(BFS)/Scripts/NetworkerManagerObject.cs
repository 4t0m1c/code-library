﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace At0m1c.PathfindingBFS.Object {

    [System.Serializable]
    public class Network {
        public List<Node> nodes = new List<Node> ();
    }

    public class NetworkerManagerObject : MonoBehaviour {

        [SerializeField] Network _network;

        Dictionary<Node, Node> nodeParents = new Dictionary<Node, Node> ();

        [SerializeField] Node startQuery, endQuery, resultQuery;

        [ContextMenu ("GetPath")]
        void GetPath () {
            nodeParents.Clear ();
            resultQuery = FindShortestPathBFS (startQuery, endQuery);
        }

        //Breadth first search of graph.
        //Populates List<Node> path with a valid solution to the goalPosition.
        //Returns the goalPosition if a solution is found.
        //Returns the startPosition if no solution is found.
        Node FindShortestPathBFS (Node startPosition, Node goalPosition) {

            uint nodeVisitCount = 0;
            float timeNow = Time.realtimeSinceStartup;

            Queue<Node> queue = new Queue<Node> ();

            List<Node> _nodesLeft = new List<Node> ();
            _nodesLeft.AddRange (_network.nodes);

            HashSet<Node> exploredNodes = new HashSet<Node> ();
            queue.Enqueue (startPosition);

            while (queue.Count != 0) {
                Node currentNode = queue.Dequeue ();
                nodeVisitCount++;

                if (currentNode == goalPosition) {
                    Debug.Log ("BFS time: " + (Time.realtimeSinceStartup - timeNow).ToString ());
                    Debug.Log (string.Format ("BFS visits: {0} ({1:F2}%)", nodeVisitCount, (nodeVisitCount / (double) _network.nodes.Count) * 100));

                    return currentNode;
                }

                List<Node> nodes = GetWalkableNodes (currentNode);

                for (int i = 0; i < nodes.Count; i++) {

                    if (!exploredNodes.Contains (nodes[i])) {
                        //Remove when explored to leave orphan data
                        _nodesLeft.Remove (nodes[i]);

                        //Mark the node as explored
                        exploredNodes.Add (nodes[i]);

                        //Store a reference to the previous node
                        nodeParents.Add (nodes[i], currentNode);

                        //Add this to the queue of nodes to examine
                        queue.Enqueue (nodes[i]);
                    }
                }
            }

            Debug.Log ("Explored nodes: " + exploredNodes.Count + " | Orphan Nodes: " + _nodesLeft.Count);

            return startPosition;
        }

        List<Node> GetWalkableNodes (Node curr) {

            List<Node> walkableNodes = new List<Node> ();

            curr.connectedNodes.ForEach (x => walkableNodes.Add (x));

            Debug.Log ("Found " + walkableNodes.Count + " walkable nodes at " + curr.transform.position);

            return walkableNodes;
        }

        void OnDrawGizmos () {
            foreach (var node in _network.nodes) {
                Gizmos.DrawWireCube (node.transform.position, new Vector3 (0.6f, 0.6f, 0.6f));
                foreach (var connectedNode in node.connectedNodes) {
                    Gizmos.DrawLine (node.transform.position, connectedNode.transform.position);
                }
            }

            Gizmos.color = Color.green;
            for (int i = 0; i < nodeParents.Count - 1; i++) {
                Gizmos.DrawLine (nodeParents.ElementAt (i).Value.transform.position, nodeParents.ElementAt (i + 1).Value.transform.position);
            }

            if (startQuery != null) {
                Gizmos.color = Color.green;
                Gizmos.DrawWireCube (startQuery.transform.position, new Vector3 (0.65f, 0.65f, 0.65f));
            }
            if (endQuery != null) {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube (endQuery.transform.position, new Vector3 (0.65f, 0.65f, 0.65f));
            }

            if (resultQuery != null) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube (resultQuery.transform.position, new Vector3 (0.6f, 0.6f, 0.6f));
            }
        }
    }
}