﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.PathfindingBFS.Object {
    public class Node : MonoBehaviour {
        public List<Node> connectedNodes = new List<Node> ();
    }
}