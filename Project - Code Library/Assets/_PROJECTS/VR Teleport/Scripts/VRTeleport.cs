﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRTeleport {

    public class VRTeleport : MonoBehaviour {

        [SerializeField] GameObject teleportEffect;
        [SerializeField] Transform playerTransform;
        [SerializeField] Transform playerHand;
        [SerializeField] LayerMask layerMask;

        Vector3 teleportLocation;

        bool teleporting = false;

        public void TeleportPress () {
            //Display decal
            teleportEffect.SetActive (true);
            StartCoroutine (UpdateTeleportPosition ());
        }

        public void TeleportEnd () {
            teleporting = false;
            teleportEffect.SetActive (false);
            playerTransform.position = teleportLocation;
        }

        IEnumerator UpdateTeleportPosition () {
            teleporting = true;
            while (teleporting) {
                // Raycast from controller
                if (Physics.Raycast (playerHand.position, playerHand.forward, out RaycastHit hit, 50, layerMask, QueryTriggerInteraction.Ignore)) {
                    // if (Vector3.Angle (hit.normal, Vector3.up) < 5f) {
                    teleportLocation = hit.point;
                    teleportEffect.transform.position = teleportLocation;
                    // }
                }
                yield return null;
            }
        }

    }
}