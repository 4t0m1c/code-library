﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.DialogueSystem {
    public class UIInteract : MonoBehaviour {
        [SerializeField] Canvas canvas;
        [SerializeField] UnityEvent OnInteract;

        void Awake () {
            canvas.enabled = false;
        }

        void Update () {
            canvas.transform.LookAt (Camera.main.transform, Vector3.back);
            canvas.transform.Rotate (Vector3.right * 180f);
        }

        void OnEnable () {
            BasicCharacterController.OnInteract += Interact;
            BasicCharacterController.OnEnterInteractable += ShowUI;
            BasicCharacterController.OnExitInteractable += HideUI;
        }

        void OnDisable () {
            BasicCharacterController.OnInteract -= Interact;
            BasicCharacterController.OnEnterInteractable -= ShowUI;
            BasicCharacterController.OnExitInteractable -= HideUI;
        }

        void Interact () {
            Debug.Log("Interacting");
            OnInteract.Invoke ();
        }

        void ShowUI () {
            canvas.enabled = true;
        }

        void HideUI () {
            canvas.enabled = false;
        }
    }
}