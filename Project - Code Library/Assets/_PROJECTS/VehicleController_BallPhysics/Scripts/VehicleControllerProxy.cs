﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.VehicleBall {

    public class VehicleControllerProxy : MonoBehaviour {

        [SerializeField] float moveSpeed = 50;
        [SerializeField] Transform actualTransform;
        public Vector3 relativeVelocity;
        [SerializeField] float decaySpeed = 10;

        Vector2 moveVector;
        public Rigidbody rigidbody;
        float fowardDecay = 0;

        void Awake () {
            rigidbody = GetComponent<Rigidbody> ();
        }

        void OnEnable () {
            VehicleInput.OnMoveVector += SetMove;
        }
        
        void OnDisable () {
            VehicleInput.OnMoveVector -= SetMove;
        }

        void Update () {
            DoMove (moveVector);
            fowardDecay = Mathf.Lerp (fowardDecay, moveVector.y, Time.deltaTime * decaySpeed);
        }

        void SetMove (Vector2 _moveVector) {
            moveVector = _moveVector;
        }

        void DoMove (Vector2 _moveVector) {
            relativeVelocity = actualTransform.InverseTransformVector (rigidbody.velocity);
            float dotProduct = Vector3.Dot (rigidbody.velocity.normalized, actualTransform.forward);

            Debug.Log ($"Dot Product {dotProduct} | Relative Velocity {relativeVelocity}");
            Vector3 torqueVector = new Vector3 (_moveVector.y, 0, 0);
            torqueVector = actualTransform.TransformDirection (torqueVector);
            rigidbody.AddTorque (torqueVector * Time.deltaTime * moveSpeed, ForceMode.VelocityChange);
            rigidbody.rotation = Quaternion.Lerp (rigidbody.rotation, actualTransform.rotation, Time.deltaTime);

            Vector3 desiredVelocity = new Vector3 (rigidbody.velocity.x * Mathf.Abs (dotProduct), rigidbody.velocity.y, rigidbody.velocity.z * Mathf.Abs (dotProduct));
            rigidbody.velocity = Vector3.Lerp (rigidbody.velocity, desiredVelocity * fowardDecay, Time.deltaTime * 10);
        }

    }

}