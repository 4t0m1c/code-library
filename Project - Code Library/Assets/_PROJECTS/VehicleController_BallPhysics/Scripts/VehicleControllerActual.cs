﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.VehicleBall {
    public class VehicleControllerActual : MonoBehaviour {

        [SerializeField] VehicleControllerProxy proxyTransform;
        [SerializeField] float turnSpeed = 10;
        [SerializeField] float orbitSpeed = 10;
        [SerializeField] Transform cameraTransform;
        Vector2 moveVector;
        Vector2 orbitVector;
        bool orbit = false;

        Quaternion origRot;

        void Awake () {
            origRot = cameraTransform.localRotation;
        }

        void OnEnable () {
            VehicleInput.OnMoveVector += SetMove;
            VehicleInput.OnOrbitBegin += BeginOrbit;
            VehicleInput.OnOrbitEnd += EndOrbit;
            VehicleInput.OnMousePosition += SetOrbit;
        }
        void OnDisable () {
            VehicleInput.OnMoveVector -= SetMove;
        }

        void FixedUpdate () {
            DoMove (moveVector);
        }

        void Update () {
            DoOrbit (orbitVector);

            if (!orbit) {
                cameraTransform.localRotation = Quaternion.Lerp (cameraTransform.localRotation, origRot, Time.deltaTime);
                cameraTransform.GetChild(0).localRotation = Quaternion.Lerp (cameraTransform.GetChild(0).localRotation, origRot, Time.deltaTime);
            }
        }

        void SetMove (Vector2 _moveVector) {
            moveVector = _moveVector;
        }

        void BeginOrbit () {
            orbit = true;
        }

        void EndOrbit () {
            orbit = false;
        }

        void SetOrbit (Vector2 _orbitVector) {
            orbitVector = _orbitVector;
        }

        void DoMove (Vector2 _moveVector) {
            transform.position = new Vector3 (proxyTransform.transform.position.x, transform.position.y, proxyTransform.transform.position.z);

            transform.Rotate (Vector3.up * _moveVector.x * Time.fixedDeltaTime * turnSpeed * Mathf.Abs (proxyTransform.relativeVelocity.z));
        }

        void DoOrbit (Vector2 _orbitVector) {
            if (orbit) {
                cameraTransform.Rotate (Vector3.up * _orbitVector.x * Time.deltaTime * orbitSpeed);
                cameraTransform.GetChild (0).Rotate (-Vector3.right * _orbitVector.y * Time.deltaTime * orbitSpeed);
            }
        }

    }
}