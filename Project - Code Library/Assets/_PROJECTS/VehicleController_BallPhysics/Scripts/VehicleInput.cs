﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.VehicleBall {
    public class VehicleInput : MonoBehaviour {

        public delegate void V2InputEvents (Vector2 vector);
        public static event V2InputEvents OnMoveVector;
        public static event V2InputEvents OnMousePosition;
        public delegate void InputEvents ();
        public static event InputEvents OnOrbitBegin;
        public static event InputEvents OnOrbitEnd;

        VehicleControls _vehicleControls;

        void Awake () {
            _vehicleControls = new VehicleControls ();
            _vehicleControls.Enable ();

            _vehicleControls.Vehicle.Move.performed += x => {
                if (OnMoveVector != null) OnMoveVector (x.ReadValue<Vector2> ());
            };

            _vehicleControls.Vehicle.MousePosition.performed += x => {
                if (OnMousePosition != null) OnMousePosition (x.ReadValue<Vector2> ());
            };

            _vehicleControls.Vehicle.OrbitActivate.started += x => BeginOrbit ();
            _vehicleControls.Vehicle.OrbitActivate.canceled += x => EndOrbit ();
        }

        void BeginOrbit () {
            if (OnOrbitBegin != null) OnOrbitBegin ();
        }
        void EndOrbit () {
            if (OnOrbitEnd != null) OnOrbitEnd ();
        }

    }
}