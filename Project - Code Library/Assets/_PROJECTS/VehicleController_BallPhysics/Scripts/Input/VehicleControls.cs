// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/VehicleController_BallPhysics/Scripts/Input/VehicleControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @VehicleControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @VehicleControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""VehicleControls"",
    ""maps"": [
        {
            ""name"": ""Vehicle"",
            ""id"": ""f91577a3-fce4-486c-975a-65539846db06"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""d21f5d57-c132-4c31-a66e-66d9d7442901"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""OrbitActivate"",
                    ""type"": ""Button"",
                    ""id"": ""068640b4-446b-45f5-b58d-3eb183a283dd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""Button"",
                    ""id"": ""e3c3c30a-a0ad-4060-8056-6c5f3286cd05"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""b541eaa4-8578-4ce6-8c0f-31941eda1584"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""3396283e-7484-4acc-b676-79271b68c9b9"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c5acc109-8544-4e1a-9564-1d4a0eb9d983"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0d26d248-7174-49df-9cd4-14be1570032a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""fc9bdd83-a656-4910-9ac1-7bc690f4f94a"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""c6c0abcb-87b8-4b79-b12b-4aabebdab09c"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OrbitActivate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a32bdeb9-6c58-4906-b4c9-52b0ec7352ca"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Vehicle
        m_Vehicle = asset.FindActionMap("Vehicle", throwIfNotFound: true);
        m_Vehicle_Move = m_Vehicle.FindAction("Move", throwIfNotFound: true);
        m_Vehicle_OrbitActivate = m_Vehicle.FindAction("OrbitActivate", throwIfNotFound: true);
        m_Vehicle_MousePosition = m_Vehicle.FindAction("MousePosition", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Vehicle
    private readonly InputActionMap m_Vehicle;
    private IVehicleActions m_VehicleActionsCallbackInterface;
    private readonly InputAction m_Vehicle_Move;
    private readonly InputAction m_Vehicle_OrbitActivate;
    private readonly InputAction m_Vehicle_MousePosition;
    public struct VehicleActions
    {
        private @VehicleControls m_Wrapper;
        public VehicleActions(@VehicleControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Vehicle_Move;
        public InputAction @OrbitActivate => m_Wrapper.m_Vehicle_OrbitActivate;
        public InputAction @MousePosition => m_Wrapper.m_Vehicle_MousePosition;
        public InputActionMap Get() { return m_Wrapper.m_Vehicle; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(VehicleActions set) { return set.Get(); }
        public void SetCallbacks(IVehicleActions instance)
        {
            if (m_Wrapper.m_VehicleActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_VehicleActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_VehicleActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_VehicleActionsCallbackInterface.OnMove;
                @OrbitActivate.started -= m_Wrapper.m_VehicleActionsCallbackInterface.OnOrbitActivate;
                @OrbitActivate.performed -= m_Wrapper.m_VehicleActionsCallbackInterface.OnOrbitActivate;
                @OrbitActivate.canceled -= m_Wrapper.m_VehicleActionsCallbackInterface.OnOrbitActivate;
                @MousePosition.started -= m_Wrapper.m_VehicleActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_VehicleActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_VehicleActionsCallbackInterface.OnMousePosition;
            }
            m_Wrapper.m_VehicleActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @OrbitActivate.started += instance.OnOrbitActivate;
                @OrbitActivate.performed += instance.OnOrbitActivate;
                @OrbitActivate.canceled += instance.OnOrbitActivate;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
            }
        }
    }
    public VehicleActions @Vehicle => new VehicleActions(this);
    public interface IVehicleActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnOrbitActivate(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
    }
}
