﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portals {
    public class PortalTestThrow : MonoBehaviour {

        [SerializeField] GameObject projectilePrefab;
        [SerializeField] float projectileForce = 10;

        void Update () {
            if (Input.GetMouseButtonDown (0)) {
                ThrowBall ();
            }
        }

        [ContextMenu("Test Throw")]
        void ThrowBall () {
            //Cast ray from mouse point
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
            Debug.Log (ray.direction);

            //Fire projectile forward
            GameObject newProjectile = Instantiate (projectilePrefab);
            newProjectile.transform.position = ray.origin;
            newProjectile.transform.LookAt (newProjectile.transform.position + ray.direction);
            newProjectile.GetComponent<Rigidbody> ().AddRelativeForce (newProjectile.transform.forward * projectileForce, ForceMode.Impulse);
        }
    }
}