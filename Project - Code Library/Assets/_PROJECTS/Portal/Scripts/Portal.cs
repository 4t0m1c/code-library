﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portals {
	public class Portal : MonoBehaviour {

		/* 
			Enter portal
			Scale to 0
			Move to targetPortal
			Scale to 1
			Add force in forward direction
		*/

		[Header ("Portal Target")]
		[SerializeField] Portal targetPortal;

		[Header ("Setup")]
		[SerializeField] string portalableTag;
		[SerializeField] float portalForce = 10;
		[SerializeField] float portalScaleSpeed = 10;
		[SerializeField] float portalDelay = 0.5f;

		GameObject receivePortalable;

		void OnTriggerEnter (Collider other) {
			Debug.Log (other.attachedRigidbody.transform.gameObject.name);

			if (other.attachedRigidbody.transform.tag == portalableTag && other.attachedRigidbody.gameObject != receivePortalable) {
				StartCoroutine (ScalePlayer (other.attachedRigidbody));
			}
		}

		void OnTriggerExit (Collider other) {
			if (other.attachedRigidbody.transform.tag == portalableTag && other.attachedRigidbody.gameObject == receivePortalable) {
				receivePortalable = null;
			}
		}

		public void ReceivePortalObject (GameObject portalable) {
			//Prevent reentring trigger if portal is a destination
			receivePortalable = portalable;
		}

		IEnumerator ScalePlayer (Rigidbody rBody) {
			Vector3 _scale = rBody.transform.localScale;

			//Scale down to 0
			rBody.isKinematic = true;
			while (rBody.transform.localScale.x > 0) {
				float modScale = Mathf.Clamp01 (rBody.transform.localScale.x - (Time.deltaTime * portalScaleSpeed));
				rBody.transform.localScale = new Vector3 (modScale, modScale, modScale);
				yield return null;
			}

			targetPortal.ReceivePortalObject(rBody.gameObject);
			rBody.transform.position = targetPortal.transform.position + (targetPortal.transform.forward * 0.3f);

			//Wait then fire from target portal
			yield return new WaitForSeconds (portalDelay);

			//Scale up to 1
			while (rBody.transform.localScale.x < _scale.x) {
				float modScale = Mathf.Clamp01 (rBody.transform.localScale.x + (Time.deltaTime * portalScaleSpeed));
				rBody.transform.localScale = new Vector3 (modScale, modScale, modScale);
				yield return null;
			}

			rBody.transform.localScale = _scale;
			rBody.isKinematic = false;
			rBody.AddForce (targetPortal.transform.forward * portalForce, ForceMode.Impulse);
		}

		private void OnDrawGizmos () {
			Gizmos.color = Color.blue;
			Gizmos.DrawRay (transform.position, transform.forward * 35);
		}
	}
}