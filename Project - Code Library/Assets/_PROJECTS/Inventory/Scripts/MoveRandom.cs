﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRandom : MonoBehaviour {

    void Start () {
        StartCoroutine (MoveToRandomPosition ());

        UIIndicators.instance.SpawnEnemyIndicator (transform);
    }

    IEnumerator MoveToRandomPosition () {
        while (true) {
            Vector3 randomPos = transform.position + new Vector3 (Random.Range (-10f, 10f), 0, Random.Range (-10f, 10f));
            while ((transform.position - randomPos).sqrMagnitude > 0.1f) {
                // float t = Time.deltaTime * 10;
                // t = t * t * t * (t * (6f * t - 15f) + 10f);
                transform.position = Vector3.MoveTowards (transform.position, randomPos, Time.deltaTime);
                yield return null;
            }
            yield return null;
        }
    }

}