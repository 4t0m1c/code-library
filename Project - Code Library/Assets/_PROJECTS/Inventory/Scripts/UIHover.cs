﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHover : MonoBehaviour {

    Camera camera;
    Canvas canvas;
    Transform target;

    bool on = false;

    void Awake () {
        camera = Camera.main;
        canvas = GetComponent<Canvas> ();
    }

    public void SetTarget (Hoverable hoverable) {
        target = hoverable.transform;
    }

    public void HoverOn () {
        if (!on) {
            on = true;
            canvas.enabled = true;
        }
    }

    public void HoverOff () {
        if (on) {
            on = false;
            canvas.enabled = false;
        }
    }

    void FixedUpdate () {
        if (target != null) {
            float angleToCam = Vector3.Dot (camera.transform.forward, (target.position - camera.transform.position).normalized);
            Debug.Log ($"angleToCam: {angleToCam}");

            Vector3 screenPos = camera.WorldToScreenPoint (target.position);
            if (angleToCam <= 0 || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.y < 0 || screenPos.y > Screen.height) {
                canvas.enabled = false;
            } else {
                canvas.enabled = true;
                transform.position = screenPos;
            }
        }
    }

}