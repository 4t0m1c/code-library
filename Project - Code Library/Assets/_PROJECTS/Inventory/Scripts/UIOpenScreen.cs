﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOpenScreen : MonoBehaviour {

    [SerializeField] Canvas canvas;
    bool open = false;

    void Update () {
        if (Input.GetKeyDown (KeyCode.Tab)) {
            open = !open;
            canvas.enabled = open;
        }
    }
}