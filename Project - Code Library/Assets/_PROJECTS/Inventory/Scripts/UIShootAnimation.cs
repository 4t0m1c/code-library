﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShootAnimation : MonoBehaviour {

    [SerializeField] Animator animator;
    [SerializeField] string animatorTrigger;

    void Update () {
        if (Input.GetMouseButtonDown (0)) {
            animator.SetTrigger (animatorTrigger);
        }
    }
}