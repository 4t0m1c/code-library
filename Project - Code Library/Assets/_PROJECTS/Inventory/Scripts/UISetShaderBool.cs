﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISetShaderBool : MonoBehaviour {
    bool selected = false;
    [SerializeField] Image renderer;

    Material mat;

    void Awake () {
        mat = Instantiate (renderer.material);
        mat.EnableKeyword ("_SheenEnabled");
    }

    public void ToggleSelect () {
        selected = !selected;
        mat.SetInt ("_SheenEnabled", selected ? 1 : 0);
        renderer.material = mat;
    }

}