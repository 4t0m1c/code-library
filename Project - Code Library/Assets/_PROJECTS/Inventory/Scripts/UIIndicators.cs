﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIIndicators : MonoBehaviour {

    public static UIIndicators instance;

    [SerializeField] GameObject uiEnemyIndicatorPrefab;
    [SerializeField] Transform uiEnemyIndicatorParent;

    void Awake () {
        instance = this;
    }

    public void SpawnEnemyIndicator (Transform enemyTransform) {
        GameObject newIndicator = Instantiate (uiEnemyIndicatorPrefab, uiEnemyIndicatorParent);
        newIndicator.GetComponent<UIIndicator> ().SetTarget (enemyTransform);
    }

}