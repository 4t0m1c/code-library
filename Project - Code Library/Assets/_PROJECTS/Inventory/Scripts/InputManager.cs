﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    Camera camera;
    [SerializeField] LayerMask hoverLayerMask;
    [SerializeField] UIHover uiHover;

    void Awake () {
        camera = Camera.main;
    }

    void Update () {
        Ray ray = camera.ScreenPointToRay (Input.mousePosition);
        if (Physics.Raycast (ray, out RaycastHit hit, 250, hoverLayerMask)) {
            Debug.Log ($"Hit: {hit.transform} | {hit.collider.attachedRigidbody.transform}");
            if (hit.collider.attachedRigidbody.transform.TryGetComponent<Hoverable> (out Hoverable hoverable)) {
                uiHover.SetTarget (hoverable);
                uiHover.HoverOn ();
                //Get hoverable information from hoverable
            } else {
                if (uiHover != null) uiHover.HoverOff ();
            }
        } else {
            if (uiHover != null) uiHover.HoverOff ();
        }
    }
}