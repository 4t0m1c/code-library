﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayOneShot : MonoBehaviour {

    [SerializeField] AudioClip clip;
    [SerializeField] AudioSource audioSource;

    public void PlayAudioClip () {
        audioSource.PlayOneShot (clip);
    }

}