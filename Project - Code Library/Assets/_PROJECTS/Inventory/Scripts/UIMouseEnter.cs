﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMouseEnter : MonoBehaviour {

    [SerializeField] Animator animator;
    [SerializeField] string animatorBool;

    public void MouseEnter () {
        animator.SetBool (animatorBool, true);
    }

    public void MouseExit () {
        animator.SetBool (animatorBool, false);
    }

}