﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvasGroup : MonoBehaviour {

    [SerializeField] CanvasGroup canvasGroup;

    public static UICanvasGroup selectedGroup;
    bool selected = false;

    public void HoverOn () {
        if (!selected) {
            canvasGroup.alpha = 0.5f;
        }
    }

    public void HoverOff () {
        if (!selected) {
            canvasGroup.alpha = 0f;
        }
    }

    public void Select () {
        if (selectedGroup != null) selectedGroup.Deselect ();

        selected = true;
        if (selected) {
            canvasGroup.alpha = 1;
            selectedGroup = this;
        }
    }

    public void Deselect () {
        selected = false;
        if (!selected) {
            canvasGroup.alpha = 0;
        }
    }

}