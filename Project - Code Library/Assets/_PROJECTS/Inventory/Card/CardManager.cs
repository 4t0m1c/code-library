﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.CCG {
    public class CardManager : MonoBehaviour {

        [SerializeField] GameObject cardPrefab;
        [SerializeField] Transform cardSpawnParent;

        [SerializeField] List<CardObject> cards = new List<CardObject> ();

        public List<Card> currentHand = new List<Card> ();

        void Start () {
            Debug.Log ($"Cards: {cards.Count}");
            for (int i = 0; i < cards.Count; i++) {
                SpawnCard (cards[i]);
            }
        }

        void SpawnCard (CardObject cardObject) {
            GameObject newCard = Instantiate (cardPrefab, cardSpawnParent);
            newCard.GetComponent<UICard> ().SetCard (cardObject.card);

            currentHand.Add (cardObject.card);
        }

    }
}