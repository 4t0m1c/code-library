using UnityEngine;

namespace At0m1c.CCG {

	[CreateAssetMenu (fileName = "Card", menuName = "Project - Code Library/Card", order = 0)]
	public class CardObject : ScriptableObject {
		public Card card;
	}

	[System.Serializable]
	public class Card {
		public Sprite cardSprite;
	}
	
}