﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace At0m1c.CCG {

    public class UICard : MonoBehaviour {

        [SerializeField] Image cardImage;
        Card card;

        public void SetCard (Card card) {
            this.card = card;
            cardImage.sprite = this.card.cardSprite;
        }

    }
}