// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/NodeNetwork/Scripts/Input/NodeInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @NodeInputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @NodeInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""NodeInputActions"",
    ""maps"": [
        {
            ""name"": ""Default"",
            ""id"": ""a8e13851-4f3e-4362-a985-c317346bada6"",
            ""actions"": [
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""7164a743-da74-40dc-a435-6dccb41a5b5d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Build"",
                    ""type"": ""Button"",
                    ""id"": ""a4508eb0-b394-4a6d-b579-6fe77f71d8d0"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fa49fee4-c213-43bc-a7b6-3ef66d5f15bb"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a0f13181-da56-4abe-8037-9b121efb6923"",
                    ""path"": ""<Keyboard>/b"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Build"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Build"",
            ""id"": ""881c1797-0442-4160-b82f-adcc5cbd6d9c"",
            ""actions"": [
                {
                    ""name"": ""Confirm"",
                    ""type"": ""Button"",
                    ""id"": ""d743d05f-f708-4c67-9e3c-f034b23118d7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""9be11c12-0001-4424-a7b7-895e27325a4d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""GridAlign"",
                    ""type"": ""Button"",
                    ""id"": ""64cd98c6-f123-462f-9370-c85802d868b2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""GridSnap"",
                    ""type"": ""Button"",
                    ""id"": ""08f091d3-dfe8-4409-9418-9776d37f0651"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""8bcdc445-e1c5-44da-bccd-2c0072fc9c95"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GridAlign"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84adc072-b32b-4e5e-a690-379168e8f79a"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""GridSnap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1ca7ec08-8764-4013-bb59-3dd05b4cc1f9"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Confirm"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6706f1cb-bf37-42e1-b5cc-a0205bd1b469"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Default
        m_Default = asset.FindActionMap("Default", throwIfNotFound: true);
        m_Default_MousePosition = m_Default.FindAction("MousePosition", throwIfNotFound: true);
        m_Default_Build = m_Default.FindAction("Build", throwIfNotFound: true);
        // Build
        m_Build = asset.FindActionMap("Build", throwIfNotFound: true);
        m_Build_Confirm = m_Build.FindAction("Confirm", throwIfNotFound: true);
        m_Build_Cancel = m_Build.FindAction("Cancel", throwIfNotFound: true);
        m_Build_GridAlign = m_Build.FindAction("GridAlign", throwIfNotFound: true);
        m_Build_GridSnap = m_Build.FindAction("GridSnap", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Default
    private readonly InputActionMap m_Default;
    private IDefaultActions m_DefaultActionsCallbackInterface;
    private readonly InputAction m_Default_MousePosition;
    private readonly InputAction m_Default_Build;
    public struct DefaultActions
    {
        private @NodeInputActions m_Wrapper;
        public DefaultActions(@NodeInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @MousePosition => m_Wrapper.m_Default_MousePosition;
        public InputAction @Build => m_Wrapper.m_Default_Build;
        public InputActionMap Get() { return m_Wrapper.m_Default; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DefaultActions set) { return set.Get(); }
        public void SetCallbacks(IDefaultActions instance)
        {
            if (m_Wrapper.m_DefaultActionsCallbackInterface != null)
            {
                @MousePosition.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMousePosition;
                @Build.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnBuild;
                @Build.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnBuild;
                @Build.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnBuild;
            }
            m_Wrapper.m_DefaultActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
                @Build.started += instance.OnBuild;
                @Build.performed += instance.OnBuild;
                @Build.canceled += instance.OnBuild;
            }
        }
    }
    public DefaultActions @Default => new DefaultActions(this);

    // Build
    private readonly InputActionMap m_Build;
    private IBuildActions m_BuildActionsCallbackInterface;
    private readonly InputAction m_Build_Confirm;
    private readonly InputAction m_Build_Cancel;
    private readonly InputAction m_Build_GridAlign;
    private readonly InputAction m_Build_GridSnap;
    public struct BuildActions
    {
        private @NodeInputActions m_Wrapper;
        public BuildActions(@NodeInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Confirm => m_Wrapper.m_Build_Confirm;
        public InputAction @Cancel => m_Wrapper.m_Build_Cancel;
        public InputAction @GridAlign => m_Wrapper.m_Build_GridAlign;
        public InputAction @GridSnap => m_Wrapper.m_Build_GridSnap;
        public InputActionMap Get() { return m_Wrapper.m_Build; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(BuildActions set) { return set.Get(); }
        public void SetCallbacks(IBuildActions instance)
        {
            if (m_Wrapper.m_BuildActionsCallbackInterface != null)
            {
                @Confirm.started -= m_Wrapper.m_BuildActionsCallbackInterface.OnConfirm;
                @Confirm.performed -= m_Wrapper.m_BuildActionsCallbackInterface.OnConfirm;
                @Confirm.canceled -= m_Wrapper.m_BuildActionsCallbackInterface.OnConfirm;
                @Cancel.started -= m_Wrapper.m_BuildActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_BuildActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_BuildActionsCallbackInterface.OnCancel;
                @GridAlign.started -= m_Wrapper.m_BuildActionsCallbackInterface.OnGridAlign;
                @GridAlign.performed -= m_Wrapper.m_BuildActionsCallbackInterface.OnGridAlign;
                @GridAlign.canceled -= m_Wrapper.m_BuildActionsCallbackInterface.OnGridAlign;
                @GridSnap.started -= m_Wrapper.m_BuildActionsCallbackInterface.OnGridSnap;
                @GridSnap.performed -= m_Wrapper.m_BuildActionsCallbackInterface.OnGridSnap;
                @GridSnap.canceled -= m_Wrapper.m_BuildActionsCallbackInterface.OnGridSnap;
            }
            m_Wrapper.m_BuildActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Confirm.started += instance.OnConfirm;
                @Confirm.performed += instance.OnConfirm;
                @Confirm.canceled += instance.OnConfirm;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @GridAlign.started += instance.OnGridAlign;
                @GridAlign.performed += instance.OnGridAlign;
                @GridAlign.canceled += instance.OnGridAlign;
                @GridSnap.started += instance.OnGridSnap;
                @GridSnap.performed += instance.OnGridSnap;
                @GridSnap.canceled += instance.OnGridSnap;
            }
        }
    }
    public BuildActions @Build => new BuildActions(this);
    public interface IDefaultActions
    {
        void OnMousePosition(InputAction.CallbackContext context);
        void OnBuild(InputAction.CallbackContext context);
    }
    public interface IBuildActions
    {
        void OnConfirm(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
        void OnGridAlign(InputAction.CallbackContext context);
        void OnGridSnap(InputAction.CallbackContext context);
    }
}
