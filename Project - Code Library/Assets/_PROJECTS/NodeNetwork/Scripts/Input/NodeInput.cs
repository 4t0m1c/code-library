﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace At0m1c.NodeNetwork {
    public class NodeInput : MonoBehaviour {

        public static NodeInput instance;

        public static Vector2 MousePosition;

        public delegate void NodeInputEvents ();
        public static event NodeInputEvents OnBuild;
        public static event NodeInputEvents OnConfirm;
        public static event NodeInputEvents OnCancel;
        public static event NodeInputEvents OnGridAlign;
        public static event NodeInputEvents OnGridSnap;

        public static event NodeInputEvents OnMapsEnabled;

        public NodeInputActions nodeInputActions;

        void Awake () {
            instance = this;

            nodeInputActions = new NodeInputActions ();
            nodeInputActions.Enable ();
        }

        void OnEnable () {
            //Default
            nodeInputActions.Default.MousePosition.performed += UpdateMousePosition;
            nodeInputActions.Default.Build.performed += ModeBuild;

            //Build
            nodeInputActions.Build.Confirm.performed += BuildConfirm;
            nodeInputActions.Build.Cancel.performed += BuildCancel;
            nodeInputActions.Build.GridAlign.performed += BuildGridAlign;
            nodeInputActions.Build.GridSnap.performed += BuildGridSnap;
        }

        void OnDisable () {
            //Default
            nodeInputActions.Default.MousePosition.performed -= UpdateMousePosition;
            nodeInputActions.Default.Build.performed -= ModeBuild;

            //Build
            nodeInputActions.Build.Confirm.performed -= BuildConfirm;
            nodeInputActions.Build.Cancel.performed -= BuildCancel;
            nodeInputActions.Build.GridAlign.performed -= BuildGridAlign;
            nodeInputActions.Build.GridSnap.performed -= BuildGridSnap;
        }

        public void EnableMaps (InputActionMap map, bool disableOthers = true) {
            if (disableOthers) {
                nodeInputActions.Disable ();
            }

            map.Enable ();
            nodeInputActions.Default.Enable ();

            if (OnMapsEnabled != null) OnMapsEnabled ();
        }

        void UpdateMousePosition (InputAction.CallbackContext ctx) {
            MousePosition = ctx.ReadValue<Vector2> ();
        }

        void ModeBuild (InputAction.CallbackContext ctx) {
            if (OnBuild != null) OnBuild ();
        }

        void BuildConfirm (InputAction.CallbackContext ctx) {
            if (OnConfirm != null) OnConfirm ();
        }

        void BuildCancel (InputAction.CallbackContext ctx) {
            if (OnCancel != null) OnCancel ();
        }

        void BuildGridAlign (InputAction.CallbackContext ctx) {
            if (OnGridAlign != null) OnGridAlign ();
        }

        void BuildGridSnap (InputAction.CallbackContext ctx) {
            if (OnGridSnap != null) OnGridSnap ();
        }

    }
}