﻿using System.Collections;
using System.Collections.Generic;
using At0m1c.NodeNetwork;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonActive : MonoBehaviour {

    [Header ("Active")]
    [SerializeField] Color activePrimaryColour;
    [SerializeField] Color activeSecondaryColour;

    [Header ("Inactive")]
    [SerializeField] Color inactivePrimaryColour;
    [SerializeField] Color inactiveSecondaryColour;

    [Header ("Color Items")]
    [SerializeField] List<Image> colImagesPrimary = new List<Image> ();
    [SerializeField] List<Image> colImagesSecondary = new List<Image> ();
    [SerializeField] List<Text> colTextsPrimary = new List<Text> ();
    [SerializeField] List<Text> colTextsSecondary = new List<Text> ();

    bool active = false;

    void Awake () {
        active = false;
    }

    void OnEnable () {
        NodeInput.OnBuild += ToggleActive;
        NodeInput.OnMapsEnabled += CheckActive;
    }

    void DisEnable () {
        NodeInput.OnBuild -= ToggleActive;
        NodeInput.OnMapsEnabled -= CheckActive;
    }

    void CheckActive () {
        if ((NodeInput.instance.nodeInputActions.Build.enabled && !active) || (!NodeInput.instance.nodeInputActions.Build.enabled && active)) {
            ToggleActive ();
        }
    }

    void ToggleActive () {
        active = !active;

        if (!active) {
            colImagesPrimary.ForEach (x => x.color = activePrimaryColour);
            colImagesSecondary.ForEach (x => x.color = activeSecondaryColour);
            colTextsPrimary.ForEach (x => x.color = activePrimaryColour);
            colTextsSecondary.ForEach (x => x.color = activeSecondaryColour);
        } else {
            colImagesPrimary.ForEach (x => x.color = inactivePrimaryColour);
            colImagesSecondary.ForEach (x => x.color = inactiveSecondaryColour);
            colTextsPrimary.ForEach (x => x.color = inactivePrimaryColour);
            colTextsSecondary.ForEach (x => x.color = inactiveSecondaryColour);
        }
    }
}