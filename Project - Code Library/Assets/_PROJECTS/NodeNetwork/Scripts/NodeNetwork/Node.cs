﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace At0m1c.NodeNetwork {
	public class Node : MonoBehaviour {

		[Header ("References")]
		[SerializeField] Transform fenceWire;
		[SerializeField] List<GameObject> postPrefabs;

		[Header ("Settings")]
		[SerializeField] List<Vector3> connectionOverridePoints = new List<Vector3> ();
		[SerializeField] LayerMask nodesMask;

		[Header ("Output")]
		public List<Node> connectedNodes = new List<Node> ();
		public NodeNetwork connectedNetwork;

		List<SphereCollider> connectionColliders = new List<SphereCollider> ();
		float wireDistance;
		BoxCollider fenceCollider;

		void Awake () {
			fenceCollider = GetComponent<BoxCollider> ();
		}

		public void NodePlaced (Vector3 endPos, bool spawnPost) {
			wireDistance = (endPos - transform.position).magnitude;
			fenceWire.localScale = new Vector3 (wireDistance / 4, 1, 1);
			fenceCollider.size = new Vector3 (fenceCollider.size.x, fenceCollider.size.y, wireDistance);
			fenceCollider.center = new Vector3 (fenceCollider.center.x, fenceCollider.center.y, wireDistance / 2f);

			if (spawnPost) {
				GameObject newPost = Instantiate (postPrefabs[Random.Range (0, postPrefabs.Count)], transform.position, Quaternion.identity);
				newPost.transform.Rotate (Vector3.up * Random.Range (0f, 360f));
				newPost.transform.SetParent (transform);
			}

			Debug.Log ("<color=green>Building complete.. Checking connections" + "</color>");
			connectionColliders.ForEach (x => x.enabled = true);
			CheckConnections ();
		}

		public void EndPlacement (bool spawnPost) {
			if (spawnPost) {
				GameObject newPost = Instantiate (postPrefabs[Random.Range (0, postPrefabs.Count)], transform.position, Quaternion.identity);
				newPost.transform.Rotate (Vector3.up * Random.Range (0f, 360f));
				newPost.transform.SetParent (transform);
				newPost.transform.localPosition += Vector3.forward * wireDistance;
			}
		}

		void CheckConnections () {
			//Find other connections
			List<Collider> cols = new List<Collider> ();
			if (connectionOverridePoints.Count > 0) {
				foreach (var point in connectionOverridePoints) {
					Debug.Log ("<color=green>Checking point: " + transform.TransformPoint (point) + "</color>");
					cols.AddRange (Physics.OverlapSphere (transform.TransformPoint (point), 0.75f, nodesMask, QueryTriggerInteraction.Ignore).ToList ());
				}
			} else {
				foreach (var point in connectionColliders) {
					Debug.Log ("<color=green>Checking point: " + transform.TransformPoint (point.center) + "</color>");
					cols.AddRange (Physics.OverlapSphere (transform.TransformPoint (point.center), 0.75f, nodesMask, QueryTriggerInteraction.Ignore).ToList ());
				}
			}

			if (cols.Count > 0) {
				foreach (var item in cols) {
					if (item.transform != transform) {
						Node _node = item.transform.GetComponent<Node> ();

						if (_node != null) {
							Debug.Log ("<color=green>Found potential connection: " + item.gameObject + " | " + cols.IndexOf (item) + "/" + cols.Count + "</color>");
							Debug.Log ("<color=green>Connector detected: " + item.gameObject.name + "</color>");

							//Add reference to other node
							if (!_node.connectedNodes.Contains (this)) _node.connectedNodes.Add (this);

							//Add reference to this node
							if (!connectedNodes.Contains (_node)) connectedNodes.Add (_node);
						}
					}

				}
			}

			SetNodeNetwork (NodeNetworkManager.instance.networks.FindNetwork (this));
		}

		public bool SetNodeNetwork (NodeNetwork network) {
			Debug.Log ("<color=green>*** START SetNodeNetwork ***" + "</color>");
			bool movedNetworks = false;

			if (network != connectedNetwork) {
				if (connectedNetwork != null) {
					Debug.Log ("<color=green>" + gameObject.name + ": Moving Networks" + "</color>");
					if (connectedNetwork.nodes.Contains (this)) connectedNetwork.nodes.Remove (this);
				}

				connectedNetwork = network;

				if (!connectedNetwork.nodes.Contains (this)) connectedNetwork.nodes.Add (this);
				Debug.Log ("<color=green>" + gameObject.name + ": Transfer complete" + "</color>");

				movedNetworks = true;
			}

			Debug.Log ("<color=green>*** END SetNodeNetwork ***" + "</color>");
			return movedNetworks;
		}

		public void Destroy () {
			if (connectedNetwork != null) {
				Debug.Log ("<color=green>" + gameObject.name + ": Returning resources" + "</color>");

				NodeNetworkManager.instance.networks.RemoveAndRecalculate (connectedNetwork, this);
			}
		}

	}
}