﻿using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.NodeNetwork {

    [System.Serializable]
    public class NodeConnection {
        public bool isProvider;
        // public Resource resourceLoad;
    }

    [System.Serializable]
    public class NodeNetworkResource {
        // public Resource resource;
        public List<NodeConnection> connections = new List<NodeConnection> ();

        public NodeNetworkResource (NodeConnection _connection) {
            // resource = new Resource (_connection.resourceLoad.resourceType, _connection.isProvider ? _connection.resourceLoad.resourceQuantity : -_connection.resourceLoad.resourceQuantity);
            connections.Add (_connection);
        }
    }

    /* public static class NodeNetworkResourceExtensions {

        public static bool HasResource (this List<NodeNetworkResource> resources, Resource resource) {
            foreach (var _res in resources) {
                if (_res.resource.resourceType == resource.resourceType) {
                    // Debug.Log ("<color=orange>" + "Found resource available: " + _res.ToString () + " | need: " + resource.ToString () + "</color>");
                    if (resource.resourceQuantity < 0) {
                        return _res.resource.resourceQuantity + resource.resourceQuantity >= 0;
                    } else {
                        return _res.resource.resourceQuantity >= resource.resourceQuantity;
                    }
                }
            }
            return false;
        }

        public static void AddResources (this List<NodeNetworkResource> resources, List<NodeNetworkResource> nodeResources) {
            foreach (var conn in nodeResources) {
                resources.AddResource (conn);
            }
        }

        public static void AddResource (this List<NodeNetworkResource> resources, NodeNetworkResource nodeResources) {
            foreach (var conn in resources) {
                if (conn.resource.resourceType == nodeResources.resource.resourceType) {
                    // Debug.Log ("<color=orange>" + "Found matching type. Adding resources" + "</color>");

                    conn.connections.AddRange (nodeResources.connections);
                    conn.resource += nodeResources.resource;
                    return;
                }
            }

            // Debug.Log ("<color=orange>" + "No matching type. Adding new NodeNetworkResource" + "</color>");
            resources.Add (nodeResources);
        }

        public static void AddConnections (this List<NodeNetworkResource> resources, List<NodeConnection> nodeConnections) {
            foreach (var conn in nodeConnections) {
                resources.AddConnection (conn);
            }
        }

        public static void AddConnection (this List<NodeNetworkResource> resources, NodeConnection nodeConnection) {
            foreach (var conn in resources) {
                if (conn.resource.resourceType == nodeConnection.resourceLoad.resourceType) {
                    if (!conn.connections.Contains (nodeConnection)) {
                        conn.connections.Add (nodeConnection);

                        // Debug.Log ("<color=orange>" + "Found matching type. Adding resources" + "</color>");

                        if (nodeConnection.isProvider) {
                            conn.resource.resourceQuantity += nodeConnection.resourceLoad.resourceQuantity;
                        } else {
                            conn.resource.resourceQuantity -= nodeConnection.resourceLoad.resourceQuantity;
                        }
                    }
                    return;
                }
            }

            // Debug.Log ("<color=orange>" + "No matching type. Adding new NodeNetworkResource" + "</color>");
            resources.Add (new NodeNetworkResource (nodeConnection));
        }

        public static void RemoveConnections (this List<NodeNetworkResource> resources, List<NodeConnection> nodeConnections) {
            foreach (var conn in nodeConnections) {
                resources.RemoveConnection (conn);
            }
        }

        public static void RemoveConnection (this List<NodeNetworkResource> resources, NodeConnection nodeConnection) {
            for (int i = 0; i < resources.Count; i++) {
                if (resources[i].resource.resourceType == nodeConnection.resourceLoad.resourceType) {
                    if (resources[i].connections.Contains (nodeConnection)) {
                        // Debug.Log ("<color=orange>" + "Found matching type. Removing resources" + "</color>");

                        resources[i].connections.Remove (nodeConnection);
                        if (nodeConnection.isProvider) {
                            resources[i].resource.resourceQuantity -= nodeConnection.resourceLoad.resourceQuantity;
                        } else {
                            resources[i].resource.resourceQuantity += nodeConnection.resourceLoad.resourceQuantity;
                        }

                        if (resources[i].resource.resourceQuantity == 0 || resources[i].connections.Count == 0) {
                            resources.RemoveAt (i);
                        }

                    }
                    return;
                }
            }
            // Debug.Log ("<color=orange>" + "No matching type. The resource was not added previously. " + nodeConnection.resourceLoad.resourceType + "</color>");
        }
    }
 */
}