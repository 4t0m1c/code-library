﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace At0m1c.NodeNetwork {
    public class NodeNetworkManager : MonoBehaviour {
        public static NodeNetworkManager instance;

        enum PlaceProgress {
            Begin,
            Placing,
            Complete
        }

        [Header ("Settings")]
        [SerializeField] LayerMask groundMask;
        [SerializeField] LayerMask invalidMask;
        [SerializeField] LayerMask blockMask;
        [SerializeField] LayerMask snappingMask;
        [SerializeField] bool gridSnapping = true;
        [SerializeField] bool gridAlignment = true;
        [SerializeField] float maxDistance = 10f;
        [SerializeField] bool snapToPosts = true;
        [SerializeField] float snappingRadius = 1f;
        float snappingRadiusSqr;

        [Header ("References")]
        [SerializeField] GameObject pipeStraightPrefab;
        [SerializeField] LineRenderer nodeLineRenderer;
        [SerializeField] Transform placementGizmoStart;
        [SerializeField] Transform placementGizmoEnd;
        [SerializeField] Transform groundRayPlane;
        [SerializeField] Transform nodeParent;

        [Header ("Debug")]
        [SerializeField] PlaceProgress placeProgress;
        public List<NodeNetwork> networks = new List<NodeNetwork> ();
        Node lastNode;

        [SerializeField] Collider[] checkForPosts = new Collider[0];
        [SerializeField] Collider[] colliders = new Collider[0];

        // public delegate void NodeNetworkUpdates (ResourceBundle availableResources, ResourceBundle pendingResources);
        // public static NodeNetworkUpdates OnResourcesUpdated;

        bool drawing = false;
        [SerializeField] bool snappingStart = false;
        [SerializeField] bool snappingEnd = false;

        // ResourceBundle _availableResources;
        // ResourceBundle _pendingResources;

        bool buildConnection = false, cancelConnection = false;

        float startHeight;
        Vector3 lastValidPos;

        void Awake () {
            instance = this;
            snappingRadiusSqr = snappingRadius * snappingRadius;
        }

        void Start () {
            StartCoroutine (NetworkDirtyCheck ());
        }

        void OnEnable () {
            NodeInput.OnBuild += PipeModeEnter;
            NodeInput.OnConfirm += SetInputBuildConnection;
            NodeInput.OnCancel += SetInputCancelConnection;
            NodeInput.OnGridAlign += SetGridAlignment;
            NodeInput.OnGridSnap += SetGridSnap;
        }

        void OnDisable () {
            NodeInput.OnBuild -= PipeModeEnter;
            NodeInput.OnConfirm -= SetInputBuildConnection;
            NodeInput.OnCancel -= SetInputCancelConnection;
            NodeInput.OnGridAlign -= SetGridAlignment;
            NodeInput.OnGridSnap -= SetGridSnap;
        }

        public void SetInputBuildConnection () {
            if (!buildConnection && drawing) buildConnection = true;
        }
        public void SetInputCancelConnection () {
            if (!cancelConnection && drawing) cancelConnection = true;
        }
        public void SetGridAlignment () {
            gridAlignment = !gridAlignment;
        }
        public void SetGridSnap () {
            gridSnapping = !gridSnapping;
        }

        public void SetInputRaiseConnection () {
            Debug.Log ("Raise Node");

            if (placeProgress == PlaceProgress.Begin) {
                groundRayPlane.position = placementGizmoStart.position + new Vector3 (0, 1.25f, 0);
                Ray rayGround = new Ray (placementGizmoStart.position + new Vector3 (0, 1.1f, 0), -placementGizmoStart.up);
                if (Physics.Raycast (rayGround, out RaycastHit groundHit, 5f, groundMask, QueryTriggerInteraction.Ignore)) {
                    Debug.Log ("Valid Raise");
                    startHeight++;
                }
            } else {
                groundRayPlane.position = placementGizmoEnd.position + new Vector3 (0, 1.25f, 0);
                Ray rayGround = new Ray (placementGizmoEnd.position + new Vector3 (0, 1.1f, 0), -placementGizmoEnd.up);
                if (Physics.Raycast (rayGround, out RaycastHit groundHit, 5f, groundMask, QueryTriggerInteraction.Ignore)) {
                    Debug.Log ("Valid Raise");
                    startHeight++;
                }
            }

        }
        public void SetInputLowerConnection () {
            Debug.Log ("Lower Node");

            if (placeProgress == PlaceProgress.Begin) {
                groundRayPlane.position = placementGizmoStart.position + new Vector3 (0, 1.25f, 0);
                Ray rayGround = new Ray (placementGizmoStart.position + new Vector3 (0, -0.9f, 0), -placementGizmoStart.up);
                if (Physics.Raycast (rayGround, out RaycastHit groundHit, 5f, groundMask, QueryTriggerInteraction.Ignore)) {
                    Debug.Log ("Valid Lower");
                    startHeight--;
                }
            } else {
                groundRayPlane.position = placementGizmoEnd.position + new Vector3 (0, 1.25f, 0);
                Ray rayGround = new Ray (placementGizmoEnd.position + new Vector3 (0, -0.9f, 0), -placementGizmoEnd.up);
                if (Physics.Raycast (rayGround, out RaycastHit groundHit, 5f, groundMask, QueryTriggerInteraction.Ignore)) {
                    Debug.Log ("Valid Lower");
                    startHeight--;
                }
            }

        }

        /* 
            PIPE PLACING
        */

        public void PipeModeEnter () {
            if (!drawing) {
                Debug.Log ("=== Pipe Mode Enter ===");
                drawing = true;
                StartCoroutine (DrawingPath ());
                NodeInput.instance.EnableMaps (NodeInput.instance.nodeInputActions.Build, true);
            } else {
                StartCoroutine (CancelAll ());
            }
        }

        IEnumerator CancelAll () {
            SetInputCancelConnection ();
            yield return null;
            SetInputCancelConnection ();

            cancelConnection = false;
            Complete (new RaycastHit (), true);
        }

        IEnumerator DrawingPath () {
            placeProgress = PlaceProgress.Begin;

            while (drawing) {
                bool raySuccess = Physics.Raycast (Camera.main.ScreenPointToRay (NodeInput.MousePosition), out RaycastHit hit, 300, groundMask, QueryTriggerInteraction.Ignore);
                colliders = Physics.OverlapSphere (new Vector3 (Mathf.Round (hit.point.x), hit.point.y, Mathf.Round (hit.point.z)), 0.25f, invalidMask, QueryTriggerInteraction.Ignore);

                if (raySuccess && colliders.Length == 0) {
                    if (placeProgress == PlaceProgress.Begin) Starting (hit);
                    else if (placeProgress == PlaceProgress.Placing) Placing (hit);
                    else if (placeProgress == PlaceProgress.Complete) Complete (hit);
                } else {
                    if (placeProgress == PlaceProgress.Begin) Starting (hit, false);
                    else if (placeProgress == PlaceProgress.Placing) Placing (hit, false);
                    else if (placeProgress == PlaceProgress.Complete) Complete (hit, false);
                }

                if (colliders.Length > 0) {
                    string overlapString = string.Empty;
                    colliders.ToList ().ForEach (x => {
                        overlapString += x.transform.name + ", ";
                    });

                    Debug.Log ("Hit: " + hit.transform + " - " + hit.point + " | Overlap: " + overlapString);
                }

                yield return null;
            }

            placementGizmoStart.gameObject.SetActive (false);
            placementGizmoEnd.gameObject.SetActive (false);
            nodeLineRenderer.gameObject.SetActive (false);
        }

        void Starting (RaycastHit hitPoint, bool success = true) {
            groundRayPlane.gameObject.SetActive (success);
            placementGizmoStart.gameObject.SetActive (success);
            nodeLineRenderer.gameObject.SetActive (false);
            placementGizmoEnd.gameObject.SetActive (false);

            if (cancelConnection) {
                cancelConnection = false;
                placeProgress = PlaceProgress.Complete;
                return;
            }

            if (success) {
                Vector3 roundedPos = new Vector3 (gridSnapping ? Mathf.Round (hitPoint.point.x) : hitPoint.point.x, hitPoint.point.y, gridSnapping ? Mathf.Round (hitPoint.point.z) : hitPoint.point.z);
                /* 
                    TODO: Use terrain sampling here to ensure consistant height above terrain
                */

                if (snapToPosts) {
                    checkForPosts = Physics.OverlapSphere (roundedPos, snappingRadius, snappingMask, QueryTriggerInteraction.Ignore);
                    if (checkForPosts.Length > 0) {
                        float closestPoint = 999f;
                        // Debug.Log ($"Checking posts: {checkForPosts.Length}");
                        for (int i = 0; i < checkForPosts.Length; i++) {
                            float _distSqr = (roundedPos - checkForPosts[i].transform.position).sqrMagnitude;
                            // Debug.Log ($"Checking Dist: {checkForPosts[i]} {_distSqr}");
                            if (_distSqr < closestPoint && _distSqr <= snappingRadiusSqr) {
                                // Debug.Log ($"Closer: {checkForPosts[i]}");
                                closestPoint = _distSqr;
                                roundedPos = checkForPosts[i].transform.position;
                                snappingStart = true;
                            }
                        }
                    } else {
                        snappingStart = false;
                    }
                }

                placementGizmoStart.position = roundedPos;

                if (buildConnection) {
                    buildConnection = false;
                    placeProgress = PlaceProgress.Placing;
                    nodeLineRenderer.gameObject.SetActive (true);
                    placementGizmoEnd.gameObject.SetActive (true);
                    nodeLineRenderer.positionCount = 2;
                    nodeLineRenderer.SetPositions (new Vector3[] { placementGizmoStart.position, placementGizmoStart.position });
                    startHeight = roundedPos.y;
                }

                groundRayPlane.position = new Vector3 (hitPoint.point.x, roundedPos.y, hitPoint.point.z);
            }

        }

        void Placing (RaycastHit hitPoint, bool success = true) {
            // placementGizmoEnd.gameObject.SetActive (success);
            groundRayPlane.position = new Vector3 (hitPoint.point.x, hitPoint.point.y + 0.002f, hitPoint.point.z);

            placementGizmoStart.LookAt (hitPoint.point, Vector3.up);
            float roundY = gridAlignment ? Mathf.Round (placementGizmoStart.eulerAngles.y / 90) * 90 : placementGizmoStart.eulerAngles.y;
            placementGizmoStart.eulerAngles = new Vector3 (0, roundY, 0);

            Vector3 roundedPos = new Vector3 (gridSnapping ? Mathf.Round (hitPoint.point.x) : hitPoint.point.x, hitPoint.point.y, gridSnapping ? Mathf.Round (hitPoint.point.z) : hitPoint.point.z);
            float distance = Vector3.Distance (placementGizmoStart.position, roundedPos);
            distance = Mathf.Clamp (distance, 0, maxDistance);
            roundedPos = placementGizmoStart.position + ((gridAlignment ? placementGizmoStart.forward : (hitPoint.point - placementGizmoStart.position).normalized) * distance);
            roundedPos = new Vector3 (gridSnapping ? Mathf.Round (roundedPos.x) : roundedPos.x, roundedPos.y, gridSnapping ? Mathf.Round (roundedPos.z) : roundedPos.z);
            roundedPos.y = hitPoint.point.y;

            Ray rayGround = new Ray (groundRayPlane.position + new Vector3 (0, -0.001f, 0), Vector3.down);
            bool validGroundhit = true;
            if (!Physics.Raycast (rayGround, out RaycastHit groundHit, 5f, groundMask, QueryTriggerInteraction.Ignore)) {
                Debug.Log ("Missed Ground");
                validGroundhit = false;
            } else {
                // Debug.Log ("Hit ground at: " + groundHit.point.y);
                validGroundhit = true;
            }

            Vector3 fixedDirection = (groundRayPlane.position - placementGizmoStart.position);

            if (gridAlignment) {

                float absX = Mathf.Abs (fixedDirection.x);
                float absY = Mathf.Abs (fixedDirection.y);
                float absZ = Mathf.Abs (fixedDirection.z);

                if (absX > absY && absX > absZ) {
                    fixedDirection.y = 0;
                    fixedDirection.z = 0;
                } else if (absY > absX && absY > absZ) {
                    fixedDirection.x = 0;
                    fixedDirection.z = 0;
                } else if (absZ >= absX && absZ >= absY) {
                    fixedDirection.x = 0;
                    fixedDirection.y = 0;
                }

                fixedDirection.RoundUniform (1);
            }

            Ray ray = new Ray (placementGizmoStart.position + placementGizmoStart.forward * 0.5f, fixedDirection);

            Debug.DrawLine (ray.origin, ray.origin + ray.direction, Color.red);

            RaycastHit[] hits = Physics.RaycastAll (ray, fixedDirection.magnitude, blockMask, QueryTriggerInteraction.Ignore);
            RaycastHit lastHit = default;
            bool invalidHit = false;
            foreach (RaycastHit hit in hits) {
                if (!hit.collider.CompareTag ("RaycastPlane")) {
                    lastHit = hit;
                    invalidHit = true;
                    break;
                }
            }
            if (invalidHit || !validGroundhit) {

                // Debug.Log ("Invalid path: " + lastValidPos + " | " + lastHit.transform);
                roundedPos = lastValidPos;

                roundedPos.y = startHeight;
                nodeLineRenderer.material.SetColor ("_Color", Color.yellow);

                if (roundedPos == placementGizmoStart.position) {
                    success = false;
                    nodeLineRenderer.material.SetColor ("_Color", Color.red);
                }

            } else {
                nodeLineRenderer.material.SetColor ("_Color", Color.cyan);
                lastValidPos = roundedPos;
            }

            if (snapToPosts) {
                checkForPosts = Physics.OverlapSphere (placementGizmoEnd.position, snappingRadius, snappingMask, QueryTriggerInteraction.Ignore);
                if (checkForPosts.Length > 0) {
                    float closestPoint = 999f;
                    // Debug.Log ($"Checking posts: {checkForPosts.Length}");
                    for (int i = 0; i < checkForPosts.Length; i++) {
                        float _distSqr = (roundedPos - checkForPosts[i].transform.position).sqrMagnitude;
                        // Debug.Log ($"Checking Dist: {checkForPosts[i]} {_distSqr}");
                        if (_distSqr < closestPoint && _distSqr <= snappingRadiusSqr) {
                            // Debug.Log ($"Closer: {checkForPosts[i]}");
                            closestPoint = _distSqr;
                            roundedPos = checkForPosts[i].transform.position;
                            snappingEnd = true;
                            placementGizmoEnd.gameObject.SetActive (false);
                        }
                    }
                } else {
                    snappingEnd = false;
                    placementGizmoEnd.gameObject.SetActive (true);
                }
            }

            // Debug.Log ("RoundedPos: " + roundedPos);
            placementGizmoEnd.position = roundedPos;
            placementGizmoEnd.LookAt (placementGizmoStart, Vector3.up);
            placementGizmoStart.LookAt (placementGizmoEnd, Vector3.up);

            if (snappingStart) {
                placementGizmoStart.gameObject.SetActive (false);
            }

            if (buildConnection) {
                buildConnection = false;
                int segments = (int) ((placementGizmoStart.InverseTransformPoint (roundedPos).z));

                Debug.Log ("Straight Segments: " + segments);
                List<NodeNetwork> affectedNetworks = new List<NodeNetwork> ();

                float startingHeight = placementGizmoStart.position.y;
                float endingHeight = placementGizmoEnd.position.y;
                float currentHeight = startingHeight;

                Debug.Log ("Pipe Segments Start");

                Vector3 startPos = placementGizmoStart.position;
                startPos.y = currentHeight;
                Vector3 endPos = placementGizmoEnd.position;
                endPos.y = currentHeight;
                if (endingHeight != currentHeight) {
                    endPos.y += Mathf.Clamp (endingHeight - currentHeight, -1, 1);
                    currentHeight = endPos.y;
                }
                GameObject newPipeSegment = Instantiate (pipeStraightPrefab, startPos, placementGizmoStart.rotation, nodeParent);
                lastNode = newPipeSegment.GetComponent<Node> ();
                lastNode.NodePlaced (endPos, !snappingStart);

                lastValidPos = newPipeSegment.transform.position;

                Debug.Log ("Pipe Segments End");

                placementGizmoStart.position = placementGizmoEnd.position;
                placeProgress = PlaceProgress.Placing;
                snappingStart = false;
                placementGizmoStart.gameObject.SetActive (true);

            }
            // }

            if (cancelConnection) {
                cancelConnection = false;
                //Cancel placement
                placeProgress = PlaceProgress.Begin;
                startHeight = 0;
                lastNode?.EndPlacement (!snappingEnd);
                snappingEnd = false;
            }

            nodeLineRenderer.SetPositions (new Vector3[] { placementGizmoStart.position, placementGizmoEnd.position });
        }

        void Complete (RaycastHit hitPoint, bool success = true) {
            drawing = false;
            groundRayPlane.gameObject.SetActive (false);
            startHeight = 0;

            NodeInput.instance.EnableMaps (NodeInput.instance.nodeInputActions.Default, true);

            Debug.Log ("=== Pipe Mode Exit ===");
        }

        public IEnumerator NetworkDirtyCheck () {
            WaitForEndOfFrame _wait = new WaitForEndOfFrame ();
            while (true) {
                yield return null;
                yield return _wait;

                bool networkChanged = false;

                foreach (var network in networks) {
                    if (network.networkDirty) {
                        if (networkChanged == false) {
                            networkChanged = true;
                        }

                        if (network.OnNetworkUpdated != null) network.OnNetworkUpdated ();
                        network.networkDirty = false;
                    }
                }

                yield return null;
                yield return _wait;
            }
        }

    }

    /*
        NODE NETWORK EXTENSIONS
    */

    public static class NodeNetworkExtensions {
        /* 
            ADD/REMOVE NETWORK
        */

        public static NodeNetwork FindNetwork (this List<NodeNetwork> networks, List<Node> nodes) {
            networks.Add (new NodeNetwork (nodes.ToArray ()));
            return networks[networks.Count - 1];
        }

        public static NodeNetwork FindNetwork (this List<NodeNetwork> networks, Node node) {

            Debug.Log ("AddToNetwork");
            if (node.connectedNodes.Count == 0) {
                //Create a new network and add the new points
                Debug.Log ("Creating a new network for " + node.gameObject.name);
                networks.Add (new NodeNetwork (node));
                NodeNetwork network = networks[networks.Count - 1];

                return network;
            }

            List<NodeNetwork> connectedNetworks = new List<NodeNetwork> ();

            foreach (var _node in node.connectedNodes) {
                //Check if networks contain this node
                if (_node.connectedNetwork != null) {
                    //if the node exists on the network, check its connections are on the same network
                    if (!connectedNetworks.Contains (_node.connectedNetwork)) connectedNetworks.Add (_node.connectedNetwork);
                }
            }

            if (connectedNetworks.Count > 1) {
                Debug.Log ("Joining 2 existing networks");
                //Join 2 networks
                for (int i = 1; i < connectedNetworks.Count; i++) {
                    if (connectedNetworks[0] != connectedNetworks[i]) {
                        connectedNetworks[0].nodes.AddRange (connectedNetworks[i].nodes);

                        for (int k = 0; k < connectedNetworks[i].nodes.Count; k++) {
                            if (connectedNetworks[i].nodes[k] != null && connectedNetworks[i].nodes[k].SetNodeNetwork (connectedNetworks[0])) {
                                k--;
                            } else if (connectedNetworks[i].nodes[k] == null) {
                                connectedNetworks[i].nodes.RemoveAt (k);
                                k--;
                            }
                        }

                        networks.Remove (connectedNetworks[i]);
                        connectedNetworks.RemoveAt (i);
                        i--;
                    }
                }
            } else {
                Debug.Log ("Using network from connected node");
            }

            if (!connectedNetworks[0].nodes.Contains (node)) node.SetNodeNetwork (connectedNetworks[0]);
            return connectedNetworks[0];
        }

        public static void RemoveNetwork (this List<NodeNetwork> networks, NodeNetwork _network) {
            networks.Remove (_network);
        }

        /* 
            SPLIT NETWORK
        */

        public static void RemoveAndRecalculate (this List<NodeNetwork> networks, NodeNetwork network, Node node) {
            for (int j = 0; j < network.nodes.Count; j++) {
                network.nodes[j].connectedNodes.Remove (node);
            }
            List<Node> _connectedNodes = node.connectedNodes;
            network.nodes.Remove (node);

            if (network.nodes.Count == 0) {
                Debug.Log ("Network empty.. Removing..");
                networks.RemoveNetwork (network);
            }

            for (int i = 1; i < node.connectedNodes.Count; i++) {
                List<Node> orphanedNodes = FindOrphanedNetworkNodes (network, _connectedNodes[0], _connectedNodes[i]);
                orphanedNodes?.Remove (_connectedNodes[0]);
                if (orphanedNodes?.Count > 0) {
                    Debug.Log ("Splitting network");
                    NodeNetwork _newNetwork = networks.FindNetwork (orphanedNodes);
                    orphanedNodes.ForEach (x => x.SetNodeNetwork (_newNetwork));
                }
            }
        }

        static List<Node> FindOrphanedNetworkNodes (this NodeNetwork network, Node startPosition, Node goalPosition) {

            uint nodeVisitCount = 0;
            float timeNow = Time.realtimeSinceStartup;

            Queue<Node> queue = new Queue<Node> ();

            List<Node> _nodesLeft = new List<Node> ();
            _nodesLeft.AddRange (network.nodes);

            HashSet<Node> exploredNodes = new HashSet<Node> ();
            queue.Enqueue (startPosition);

            while (queue.Count != 0) {
                Node currentNode = queue.Dequeue ();
                nodeVisitCount++;

                if (currentNode == goalPosition) {
                    Debug.Log ("BFS time: " + (Time.realtimeSinceStartup - timeNow).ToString ());
                    Debug.Log (string.Format ("BFS visits: {0} ({1:F2}%)", nodeVisitCount, (nodeVisitCount / (double) network.nodes.Count) * 100));

                    return default;
                }

                List<Node> _nodes = GetWalkableNodes (currentNode);
                for (int i = 0; i < _nodes.Count; i++) {
                    if (!exploredNodes.Contains (_nodes[i])) {
                        //Remove when explored to leave orphan data
                        _nodesLeft.Remove (_nodes[i]);
                        //Mark the node as explored
                        exploredNodes.Add (_nodes[i]);
                        //Add this to the queue of nodes to examine
                        queue.Enqueue (_nodes[i]);
                    }
                }
            }

            Debug.Log ("Explored nodes: " + exploredNodes.Count + " | Orphan Nodes: " + _nodesLeft.Count);

            return _nodesLeft;
        }

        static List<Node> GetWalkableNodes (Node curr) {
            List<Node> walkableNodes = new List<Node> ();
            curr.connectedNodes.ForEach (x => walkableNodes.Add (x));
            Debug.Log ("Found " + walkableNodes.Count + " walkable nodes at " + curr.transform.position);
            return walkableNodes;
        }

        public static Vector3 RoundUniform (this Vector3 _origin, float _dist) {
            float fracVal = 1 / _dist;
            return new Vector3 (Mathf.Round (_origin.x * fracVal) / fracVal, Mathf.Round (_origin.y * fracVal) / fracVal, Mathf.Round (_origin.z * fracVal) / fracVal);
        }

    }

}