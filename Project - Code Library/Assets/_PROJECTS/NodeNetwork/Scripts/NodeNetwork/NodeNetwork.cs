﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace At0m1c.NodeNetwork {

    [System.Serializable]
    public class NodeNetwork {

        public NodeNetwork (params Node[] _connectionLinks) {
            foreach (var link in _connectionLinks) {
                nodes.Add (link);
            }
        }

        public delegate void NodeNetworkEvent ();
        public NodeNetworkEvent OnNetworkUpdated;
        public List<Node> nodes = new List<Node> ();
        // public List<NodeNetworkResource> resources = new List<NodeNetworkResource> ();
        // public List<NodeNetworkResource> pendingResources = new List<NodeNetworkResource> (); //Add to pending if dependencies are not met. Reevaluate once a new provider has been added.
        public bool networkDirty = false;

        // public bool AddResources (List<NodeConnection> connections) {
        //     bool success = CheckResources (connections);
        //     // Debug.Log ("<color=yellow>Add resources: " + Time.timeSinceLevelLoad + "</color>");
        //     networkDirty = true;
        //     return success;
        // }

        // public void RemoveResources (List<NodeConnection> connections) {
        //     bool success = CheckResources (connections);
        //     // resources.RemoveConnections (connections);
        //     // pendingResources.RemoveConnections (connections);
        //     // Debug.Log ("<color=yellow>Remove resources: " + Time.timeSinceLevelLoad + "</color>");
        //     networkDirty = true;
        // }

        // public bool CheckResources (List<NodeConnection> connections) {
        //     List<NodeNetworkResource> dependents = new List<NodeNetworkResource> ();

        //     foreach (var conn in connections) {
        //         if (!conn.isProvider) dependents.AddConnection (conn);
        //     }

        //     // Debug.Log ("<color=yellow>Checking " + dependents.Count + " dependants for resources</color>");

        //     bool dependenciesMet = true;
        //     List<NodeNetworkResource> existingResources = new List<NodeNetworkResource> ();
        //     existingResources.AddResources (resources);
        //     foreach (var dep in dependents) {
        //         // Debug.Log ("<color=yellow>Dependant needs: " + (-dep.resource).ToString () + "</color>");
        //         if (!existingResources.HasResource (-dep.resource)) {
        //             // Debug.Log ("<color=yellow>Dependencies not met.</color>");
        //             dependenciesMet = false;
        //             break;
        //         } else {
        //             // Debug.Log ("<color=yellow>Dependencies met.</color>");
        //             existingResources.Remove (dep);
        //         }
        //     }

        //     if (dependenciesMet) {
        //         // Debug.Log ("<color=yellow>Dependencies met. Adding resources.</color>");
        //         pendingResources.RemoveConnections (connections);
        //         resources.AddConnections (connections);
        //         return true;
        //     } else {
        //         // Debug.Log ("<color=yellow>Dependencies not met. Resources pending.</color>");
        //         resources.RemoveConnections (connections);
        //         pendingResources.AddConnections (connections);
        //         return false;
        //     }
        // }

    }
}