﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace At0m1c.ActionQueue {
    public class Block2 : MonoBehaviour, IActionable {

        public void PerformAction (UnityAction callback) {
            /* Action can be used in any way. It can be instant or over time. */
            StartCoroutine (Action (callback));
        }

        IEnumerator Action (UnityAction callback) {
            float timeToComplete = 3;
            while (timeToComplete > 0) {
                timeToComplete -= Time.deltaTime;
                Debug.Log ($"Action on {this.name} completing in {timeToComplete} seconds");
                yield return null;
            }

            callback.Invoke ();
        }

    }
}