﻿using UnityEngine.Events;

namespace At0m1c.ActionQueue {

    public interface IActionable {

        void PerformAction (UnityAction callback);

    }

}