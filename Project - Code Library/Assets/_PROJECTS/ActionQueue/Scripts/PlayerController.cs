﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

namespace At0m1c.ActionQueue {
    public class PlayerController : MonoBehaviour {

        enum ActionStatus {
            Waiting,
            Navigating,
            Interacting,
            Complete
        }

        [SerializeField] LayerMask clickMask;

        NavMeshAgent agent;
        Camera camera;

        Vector2 mousePosition;

        Queue<IActionable> actionQueue = new Queue<IActionable> ();
        IActionable currentAction;

        ActionQueueInput actionQueueInput;

        [SerializeField] ActionStatus actionStatus;

        bool destinationOverride = false;

        void Awake () {
            agent = GetComponent<NavMeshAgent> ();
            camera = Camera.main;
        }

        void Start () {
            actionQueueInput = new ActionQueueInput ();

            StartCoroutine (ProcessQueue ());

            actionQueueInput.Enable ();

            actionQueueInput.Play.MousePosition.performed += x => mousePosition = x.ReadValue<Vector2> ();
            actionQueueInput.Play.Click.performed += x => Click ();
        }

        void OnDestroy () {
            actionQueueInput.Dispose ();
        }

        void Click () {
            //Set destination or set target actionable as destination

            Ray ray = camera.ScreenPointToRay (mousePosition);
            if (Physics.Raycast (ray, out RaycastHit hit, 50, clickMask, QueryTriggerInteraction.Ignore)) {
                if (hit.collider.gameObject.TryGetComponent<IActionable> (out IActionable actionable)) {
                    Debug.Log ($"Hit actionable");
                    actionQueue.Enqueue (actionable);
                    destinationOverride = false;
                } else {
                    if (actionStatus == ActionStatus.Navigating || actionStatus == ActionStatus.Waiting) {
                        // Can only interrupt if walking or waiting
                        Debug.Log ($"Hit floor");
                        destinationOverride = true;
                        agent.SetDestination (hit.point);
                        actionStatus = ActionStatus.Waiting;
                    }
                }
            }
        }

        IEnumerator ProcessQueue () {
            Vector3 targetDestination = Vector3.zero;

            while (true) {
                if (actionStatus == ActionStatus.Complete) {
                    //Resetting
                    yield return new WaitForSeconds (1);
                    actionStatus = ActionStatus.Waiting;
                }

                if (!destinationOverride) {
                    if (currentAction == null) {
                        //No action currently set. See if one is available.
                        if (actionQueue.Count > 0) {
                            //Action is available. Assign it.
                            currentAction = actionQueue.Dequeue ();
                            targetDestination = ((MonoBehaviour) currentAction).transform.position;
                        } else {
                            Debug.Log ($"No actions available");
                        }
                    }

                    if (currentAction != null) {
                        //Has action, processing
                        Debug.Log ($"Processing action");

                        if (actionStatus == ActionStatus.Waiting) {
                            //Set the destination
                            agent.SetDestination (targetDestination);
                            actionStatus = ActionStatus.Navigating;
                        } else if (agent.hasPath && agent.isActiveAndEnabled) {
                            //Already set. Navigating.
                            //Check for distance to actionable
                            if (agent.remainingDistance > agent.radius) {
                                Debug.Log ($"Navigating");
                                actionStatus = ActionStatus.Navigating;
                            } else {
                                Debug.Log ($"Arrived");
                                currentAction.PerformAction (ActionComplete);
                                actionStatus = ActionStatus.Interacting;
                            }
                        }
                    }
                } else {
                    if (agent.hasPath && agent.isActiveAndEnabled) {
                        //Still getting to override destination
                    } else {
                        if (agent.remainingDistance <= agent.radius || agent.isStopped) {
                            //Reached override destination
                            actionStatus = ActionStatus.Complete; //Will trigger a wait
                            destinationOverride = false;
                        }
                    }
                }
                yield return null;
            }
        }

        void ActionComplete () {
            actionStatus = ActionStatus.Complete;
            currentAction = null;
        }

    }
}