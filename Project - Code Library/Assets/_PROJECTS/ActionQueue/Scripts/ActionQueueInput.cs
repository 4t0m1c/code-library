// GENERATED AUTOMATICALLY FROM 'Assets/_PROJECTS/ActionQueue/Scripts/ActionQueueInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace At0m1c.ActionQueue
{
    public class @ActionQueueInput : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @ActionQueueInput()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""ActionQueueInput"",
    ""maps"": [
        {
            ""name"": ""Play"",
            ""id"": ""2c6f8090-221b-4e8d-a190-016f3d559aa5"",
            ""actions"": [
                {
                    ""name"": ""Click"",
                    ""type"": ""Button"",
                    ""id"": ""1377489e-6b06-42ea-839f-79dde9b74572"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""118b1ec5-5fc7-4977-a8b0-af2475074ee9"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""ec7bfc26-ee7f-4180-afee-2393f22f829c"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0485382f-fc66-428d-a341-3f632d8756b6"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Play
            m_Play = asset.FindActionMap("Play", throwIfNotFound: true);
            m_Play_Click = m_Play.FindAction("Click", throwIfNotFound: true);
            m_Play_MousePosition = m_Play.FindAction("MousePosition", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Play
        private readonly InputActionMap m_Play;
        private IPlayActions m_PlayActionsCallbackInterface;
        private readonly InputAction m_Play_Click;
        private readonly InputAction m_Play_MousePosition;
        public struct PlayActions
        {
            private @ActionQueueInput m_Wrapper;
            public PlayActions(@ActionQueueInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Click => m_Wrapper.m_Play_Click;
            public InputAction @MousePosition => m_Wrapper.m_Play_MousePosition;
            public InputActionMap Get() { return m_Wrapper.m_Play; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayActions set) { return set.Get(); }
            public void SetCallbacks(IPlayActions instance)
            {
                if (m_Wrapper.m_PlayActionsCallbackInterface != null)
                {
                    @Click.started -= m_Wrapper.m_PlayActionsCallbackInterface.OnClick;
                    @Click.performed -= m_Wrapper.m_PlayActionsCallbackInterface.OnClick;
                    @Click.canceled -= m_Wrapper.m_PlayActionsCallbackInterface.OnClick;
                    @MousePosition.started -= m_Wrapper.m_PlayActionsCallbackInterface.OnMousePosition;
                    @MousePosition.performed -= m_Wrapper.m_PlayActionsCallbackInterface.OnMousePosition;
                    @MousePosition.canceled -= m_Wrapper.m_PlayActionsCallbackInterface.OnMousePosition;
                }
                m_Wrapper.m_PlayActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Click.started += instance.OnClick;
                    @Click.performed += instance.OnClick;
                    @Click.canceled += instance.OnClick;
                    @MousePosition.started += instance.OnMousePosition;
                    @MousePosition.performed += instance.OnMousePosition;
                    @MousePosition.canceled += instance.OnMousePosition;
                }
            }
        }
        public PlayActions @Play => new PlayActions(this);
        public interface IPlayActions
        {
            void OnClick(InputAction.CallbackContext context);
            void OnMousePosition(InputAction.CallbackContext context);
        }
    }
}
